app.controller('settingCityCtrl', function($scope) {
	$scope.init = function() {

	}

	$scope.init();
});

// app.controller('settingCityIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
// 	$scope.init = function() {
// 		$scope.cities = [];

// 	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
// 	        var q = $q.defer();
// 	        var data = {};

// 	        if ($scope.status != 99) {
// 		        data.status = $scope.status;
// 	        }

// 	        RequestService.getall('city').then(function(data) {
// 				q.resolve(data);
// 			});

// 	        return q.promise;
// 	    }).withOption('createdRow', createdRow);

// 	    $scope.dtColumns = [
// 	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
// 	        DTColumnBuilder.newColumn('name').withTitle('Nama'),
// 	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
// 	    ];

// 	    $scope.dtInstance = {};

// 	    function createdRow(row, data, dataIndex) {
// 	    	$compile(angular.element(row).contents())($scope);
// 	    }

// 	    function numberHtml(data, type, full, meta) {
// 	    	return meta.row + 1;
// 	    }

// 		// function detailHtml(data, type, full, meta) {
// 		//     return '<a href="" ng-click="navigateTo(\'app.settingCity.detail\', cities[' + meta.row + '])">' + data.name + '</a>';
// 		// }

// 		// function statusHtml(data, type, full, meta) {
// 		// 	var status;

// 		// 	if (data.status) {
// 		// 		status = '<span class="status-active">Aktif</span>';
// 		// 	} else {
// 		// 		status = '<span class="status-nonactive">Non-Aktif</span>';
// 		// 	}

// 		//     return status;
// 		// }

// 		//    function imageHtml(data, type, full, meta) {
// 		//        return '<img src="data:image;base64,' + data.img + '" width="55px">';
// 		// }

// 		// function timeHtml(data, type, full, meta) {
// 		// 	var date = new Date(data.time);

// 		// 	return TimeService.days[date.getDay()] + ', ' 
// 		// 		+ ('0' + date.getDate()).slice(-2) + ' '
// 		// 		+ TimeService.months[date.getMonth()] + ' '
// 		// 		+ date.getFullYear() + ' '
// 		// 		+ ('0' + date.getHours()).slice(-2) + ':'
// 		// 		+ ('0' + date.getMinutes()).slice(-2) + ':'
// 		// 		+ ('0' + date.getMinutes()).slice(-2);
// 		// }

// 		function actionHtml(data, type, full, meta) {
// 			$scope.cities[meta.row] = data;

// 			return '\
// 				<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.settingCity.edit\', cities[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
// 				<button class="btn btn-danger btn-xs btn-block" ng-click="destroyCity(cities[' + meta.row + '].id)"><i class="fa fa-trash-o"></i> Delete</button>\
// 			';
// 		}
// 	}

// 	$scope.reloadData = function() {
// 		$scope.dtInstance.reloadData();
// 	}

// 	$scope.navigateTo = function(targetPage, objectData) {
// 		$state.go(targetPage, {
// 			city: objectData
// 		});
// 	}

// 	$scope.destroyCity = function(id) {
// 		$rootScope.confirmHeader  = 'Konfirmasi';
// 		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
// 		$rootScope.confirmButton  = 'Hapus';

// 		$rootScope.modalAction = function() {
// 			var data = { 'id' : id };

// 			$('#indexConfirm').modal('hide');
			
// 			RequestService.delete(data, 'city').then(function(response) {
// 				$scope.reloadData();
// 			});
// 		}

// 		$('#indexConfirm').modal('show');
// 	}

// 	$scope.init();
// });

app.controller('settingCityIndexCtrl', function($scope, $rootScope, $state, RequestService) {
	$scope.cities = [];
	$scope.pages = [];
	$scope.last;
	$scope.limit = 10;
	$scope.currentPage = 1;
	var data = {};

	$scope.init = function() {
        // if ($scope.status != 99) {
	       //  data.status = $scope.status;
        // }

		RequestService.getbypaging(data, 'city', $scope.currentPage, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.cities.push(data[ii]);
			};
		});

		RequestService.gettotal(data, 'city').then(function(data) {
			console.log(data);
			for (var ii = 1; ii <= Math.ceil(data / $scope.limit); ii++) {
				$scope.pages.push(ii);
			};

			$scope.last = $scope.pages.length;
		});
	};

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			city: objectData
		});
	}

	$scope.gotoPage = function(page) {
		$scope.cities = [];
		$scope.currentPage = page;

		RequestService.getbypaging(data, 'city', page, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.cities.push(data[ii]);
			};
		});
	}

	$scope.reloadData = function() {
		$scope.cities = [];
		$scope.pages = [];

		if ($scope.status != 99) {
	        data.status = $scope.status;
        } else {
        	data = {};
        }

		RequestService.getbypaging(data, 'city', $scope.currentPage, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.cities.push(data[ii]);
			};
		});

		RequestService.gettotal(data, 'city').then(function(data) {
			for (var ii = 1; ii <= Math.ceil(data / $scope.limit); ii++) {
				$scope.pages.push(ii);
			};

			$scope.last = $scope.pages.length;
		});
	}

	$scope.destroyCity = function(id) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'city').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('settingCityCreateCtrl', function($scope, $state, $rootScope, RequestService) {
	$scope.init = function() {
		$scope.errorName = false;
	}

	$scope.insertCity = function() {
		var check = true;

		$scope.errorName = false;

		if (!$scope.name) {
			$scope.errorName = true;
			check = false;
		}

		if (check) {
			var date = new Date();

			var data = {
				'name': $scope.name
			}

			RequestService.save(data, 'city').then(function(response) {
				$state.go('app.settingCity.index');

				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = response.data.message;

				$('#indexModal').modal('show');
			});
		}
	}

	$scope.init();
});

app.controller('settingCityEditCtrl', function($scope, $stateParams, $state, $rootScope, RequestService) {
	$scope.init = function() {
		if ($stateParams.city) { 
			$scope.city = $stateParams.city;

			$scope.errorName = false;
		} else $state.go('app.settingCity.index');
	}

	$scope.updateCity = function() {
		var check = true;

		$scope.errorName = false;

		if (!$scope.city.name) {
			$scope.errorName = true;
			check = false;
		}

		if (check) {
			var data = {
				'id': $scope.city.id,
				'name': $scope.city.name
			}

			RequestService.save(data, 'city').then(function(response) {
				$state.go('app.settingCity.index');

				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = response.data.message;

				$('#indexModal').modal('show');
			});
		}
	}

	$scope.init();
});