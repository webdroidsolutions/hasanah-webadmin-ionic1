app.controller('RolesCtrl', function($scope,RequestService,$rootScope, $uibModal) {
	$scope.roles=[];
	$scope.newRole=[];
	$scope.init = function() {
		$scope.newRole=[];
		RequestService.getRoles('role').then(function(response) {
			console.log(response);
			$scope.roles=response;
			$rootScope.modalHeader  = 'Pesan';
			$rootScope.modalMessage = 'Roles fetched';

			$('#indexModal').modal('show');
		}); 
	}

	$scope.save = function() {
		if (!$scope.newRole[0].name) 
		{
			alert("Please enter name for new role..");
			return;
		};
		$scope.newRole[0].name=$scope.newRole[0].name.toUpperCase();
		RequestService.save($scope.newRole[0],'role').then(function(response) {
			$scope.init();
			$scope.roles=response;
			$rootScope.modalHeader  = 'Pesan';
			$rootScope.modalMessage = 'Role created';

			$('#indexModal').modal('show');
		}); 
	}

	$scope.update = function(role) {
		if (!role.name) 
		{
			alert("Please enter name for new role..");
			return;
		};
		role.name=role.name.toUpperCase();
		role.id=role.id;
		RequestService.save(role,'role').then(function(response) {
			$scope.init();
			$rootScope.modalHeader  = 'Pesan';
			$rootScope.modalMessage = response.data.message;
			$('#indexModal').modal('show');
		}); 
	}

	$scope.delete = function(role) {
		RequestService.delete(role,'role').then(function(response) {
			$scope.init();
			$rootScope.modalHeader  = 'Pesan';
			$rootScope.modalMessage = response.data.message;
			$('#indexModal').modal('show');
		}); 
	};

	$scope.manageRole = function(role)
	{
		var modalInstance = $uibModal.open({
			templateUrl: 'templates/role/html/manage-role-model.html',
			controller: 'ManageRoleCtrl',
			size: 'lg',
			animation: true,
			resolve: {
				item: function () {
					return role;
				}
			}
		});
	}
	$scope.init();
});

app.controller('ManageRoleCtrl', function($scope, $state, RequestService,$uibModalInstance,item,$rootScope) {
	var role;
	$scope.menus;
	$scope.allSelected=false;
	$scope.init = function() {
		$scope.$on('$viewContentLoaded', function(){
			$.AdminLTE.layout.activate();
		});
		role=item;
		$scope.title=role.name;
        //console.log(item);
        $scope.user=localStorage.user;
        $scope.displayName = localStorage.name;
        $scope.displayType = localStorage.user_type;
        RequestService.getMenus('role').then(function(response) {
        	$scope.menus=response[0].menus;
        	for (var i = 0; i < role.menus.length; i++) 
        	{
        		var j = 0;
        		for (var m in $scope.menus) {
        			if (role.menus[i].id==$scope.menus[j].id) 
        			{
        				$scope.menus[j].checked=true;
        			}
        			j++;
        		}
        	}
        	$rootScope.modalHeader  = 'Pesan';
        	$rootScope.modalMessage = 'Menus fetched';

        	$('#indexModal').modal('show');
        }); 
    };

    $scope.selectAll=function(){
    	if ($scope.allSelected) 
    	{
    		var i = 0;
    		for (var m in $scope.menus) {
    			$scope.menus[i].checked=true;
    			i++;
    		}
    	} 
    	else 
    	{
    		var i = 0;
    		for (var m in $scope.menus) {
    			$scope.menus[i].checked=false;
    			i++;
    		}
    	}
    };

    $scope.update = function() {
    	role.menus=[];
    	var i = 0;
    	for (var m in $scope.menus) {
    		if ($scope.menus[i].checked) 
    		{
    			role.menus.push($scope.menus[i]);
    		} 
    		i++;
    	}
    	console.log(role);
    	RequestService.update(role,'role').then(function(response) {
    		$scope.init();
    		$scope.cancel();
    		$rootScope.modalHeader  = 'Pesan';
    		$rootScope.modalMessage = response.data.message;
    		$('#indexModal').modal('show');
    	}); 
    };

    $scope.cancel=function()
    {
    	console.log("role updated");
    	$uibModalInstance.close();
    };

    $scope.init();
});