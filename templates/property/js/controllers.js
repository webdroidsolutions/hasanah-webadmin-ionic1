app.controller('propertyCtrl', function($scope) {
	$scope.init = function() {

	}

	$scope.init();
});

// app.controller('propertyIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
// 	$scope.init = function() {
// 		$scope.properties = [];

// 	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
// 	        var q = $q.defer();
// 	        var data = {};

// 	        if ($scope.status != 99) {
// 		        data.status = $scope.status;
// 	        }

// 	        RequestService.getbystatus(data, 'property').then(function(data) {
// 				q.resolve(data);
// 			});

// 	  //       return q.promise;
// 	  //   }).withOption('createdRow', createdRow);

// 	  //   $scope.dtOptions = DTOptionsBuilder.newOptions()
// 	  //   	.withOption('ajax', {
// 	  //   		dataSrc: function(json) {
// 			// 		json['recordsTotal'] = 16
// 			// 		json['recordsFiltered'] = 10
// 			// 		json['draw']=2
// 			// 		return json;
// 			// 	},
// 			// 	url: 'app-data/test.json',
// 			// 	type: 'GET'
// 			// })
// 			// .withOption('processing', true)
// 			// .withOption('serverSide', true)
// 			// .withPaginationType('full_numbers');

// 		// $scope.dtOptions = DTOptionsBuilder.newOptions()
//   //           .withOption('ajax', {
//   //               contentType: 'application/json',
//   //               url: 'app-data/test.json',
//   //               type: 'GET',
//   //               dataSrc: function(json) {
// 		// 			json['recordsTotal'] = 10
// 		// 			json['recordsFiltered'] = 10
// 		// 			json['draw']= 1
// 		// 			return json;
// 		// 		},
//   //               // beforeSend: function(xhr){
//   //               //     xhr.setRequestHeader("Authorization",
//   //               //         "Bearer " + AuthenticationService.getAccessToken());
//   //               // },
//   //               // data: function(data, dtInstance) {

//   //               //     // The returned object has 'email' as property, but the server entity has 'emailAddress'
//   //               //     // We need to override what we ask to the server here otherwise search will not work
//   //               //     data.columns[1].data = "emailAddress";

//   //               //     // Any values you set on the data object will be passed along as parameters to the server
//   //               //     //data.access_token = AuthenticationService.getAccessToken();
//   //               //     return JSON.stringify(data);
//   //               // }
//   //           })
//   //           .withDataProp('data') // This is the name of the value in the returned recordset which contains the actual data
//   //           .withOption('serverSide', true)
//   //       // .withBootstrap()
//   //       .withPaginationType('simple_numbers')
//   //       .withDisplayLength(20)
//   //       .withOption('createdRow', function (row) {
//   //           // Recompiling so we can bind Angular directive to the DT
//   //           $compile(angular.element(row).contents())($scope);
//         })
//   //       .withOption('saveState', true)
//   //       .withOption('order', [0, 'asc']);

// 	    $scope.dtColumns = [
// 	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
// 	        DTColumnBuilder.newColumn(null).withTitle('Nama').renderWith(detailHtml),
// 	        DTColumnBuilder.newColumn(null).withTitle('Gambar').notSortable().renderWith(imageHtml),
// 	        DTColumnBuilder.newColumn('address').withTitle('Alamat'),
// 	        DTColumnBuilder.newColumn(null).withTitle('Status').renderWith(statusHtml),
// 	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
// 	    ];

// 	    $scope.dtInstance = {};

// 	    function createdRow(row, data, dataIndex) {
// 	    	$compile(angular.element(row).contents())($scope);
// 	    }

// 	    function numberHtml(data, type, full, meta) {
// 	    	return meta.row + 1;
// 	    }

// 	    function detailHtml(data, type, full, meta) {
// 	        return '<a href="" ng-click="navigateTo(\'app.property.detail\', properties[' + meta.row + '])">' + data.name + '</a>';
// 		}

// 		function statusHtml(data, type, full, meta) {
// 	    	var status;

// 	    	if (data.status) {
// 	    		status = '<span class="status-active">Aktif</span>';
// 	    	} else {
// 	    		status = '<span class="status-nonactive">Non-Aktif</span>';
// 	    	}

// 	        return status;
// 		}

// 	    function imageHtml(data, type, full, meta) {
// 	        return '<img src="data:image;base64,' + data.img[0] + '" width="55px">';
// 		}

// 		// function timeHtml(data, type, full, meta) {
// 		// 	var date = new Date(data.time);

// 		// 	return TimeService.days[date.getDay()] + ', ' 
// 		// 		+ ('0' + date.getDate()).slice(-2) + ' '
// 		// 		+ TimeService.months[date.getMonth()] + ' '
// 		// 		+ date.getFullYear() + ' '
// 		// 		+ ('0' + date.getHours()).slice(-2) + ':'
// 		// 		+ ('0' + date.getMinutes()).slice(-2) + ':'
// 		// 		+ ('0' + date.getMinutes()).slice(-2);
// 		// }

// 		function actionHtml(data, type, full, meta) {
// 			$scope.properties[meta.row] = data;

// 			return '\
// 				<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.property.edit\', properties[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
// 				<button class="btn btn-danger btn-xs btn-block" ng-click="destroyProperty(properties[' + meta.row + '].id)"><i class="fa fa-trash-o"></i> Delete</button>\
// 			';
// 		}
// 	}

// 	$scope.reloadData = function() {
// 		$scope.dtInstance.reloadData();
// 	}

// 	$scope.navigateTo = function(targetPage, objectData) {
// 		$state.go(targetPage, {
// 			property: objectData
// 		});
// 	}

// 	$scope.destroyProperty = function(id) {
// 		$rootScope.confirmHeader  = 'Konfirmasi';
// 		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
// 		$rootScope.confirmButton  = 'Hapus';

// 		$rootScope.modalAction = function() {
// 			var data = { 'id' : id };

// 			$('#indexConfirm').modal('hide');

// 			RequestService.delete(data, 'property').then(function(response) {
// 				$scope.reloadData();
// 			});
// 		}

// 		$('#indexConfirm').modal('show');
// 	}

// 	$scope.init();
// });

app.controller('propertyIndexCtrl', function($scope, $rootScope, $state, RequestService) {
	$scope.properties = [];
	$scope.pages = [];
	$scope.last;
	$scope.limit = 10;
	$scope.currentPage = 1;
	$scope.baseUrl = RequestService.url.replace('adm/', '');
	var data = {};

	$scope.init = function() {
        // if ($scope.status != 99) {
	       //  data.status = $scope.status;
        // }

        RequestService.getbypaging(data, 'property', $scope.currentPage, $scope.limit).then(function(data) {
        	for (var ii = 0; ii < data.length; ii++) {
        		$scope.properties.push(data[ii]);
        	};
        });

        RequestService.gettotal(data, 'property').then(function(data) {
        	for (var ii = 1; ii <= Math.ceil(data / $scope.limit); ii++) {
        		$scope.pages.push(ii);
        	};

        	$scope.last = $scope.pages.length;
        });
    };

    $scope.navigateTo = function(targetPage, objectData) {
    	$state.go(targetPage, {
    		property: objectData
    	});
    }

    $scope.gotoPage = function(page) {
    	$scope.properties = [];
    	$scope.currentPage = page;

    	RequestService.getbypaging(data, 'property', page, $scope.limit).then(function(data) {
    		for (var ii = 0; ii < data.length; ii++) {
    			$scope.properties.push(data[ii]);
    		};
    	});
    }

    $scope.reloadData = function() {
    	$scope.properties = [];
    	$scope.pages = [];

    	if ($scope.status != 99) {
    		data.status = $scope.status;
    	} else {
    		data = {};
    	}

    	RequestService.getbypaging(data, 'property', $scope.currentPage, $scope.limit).then(function(data) {
    		for (var ii = 0; ii < data.length; ii++) {
    			$scope.properties.push(data[ii]);
    		};
    	});

    	RequestService.gettotal(data, 'property').then(function(data) {
    		for (var ii = 1; ii <= Math.ceil(data / $scope.limit); ii++) {
    			$scope.pages.push(ii);
    		};

    		$scope.last = $scope.pages.length;
    	});
    }

    $scope.destroyProperty = function(id, old_img) {
    	$rootScope.confirmHeader  = 'Konfirmasi';
    	$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
    	$rootScope.confirmButton  = 'Hapus';

    	$rootScope.modalAction = function() {
    		var data = { 'id' : id, 'old_img' : old_img };

    		$('#indexConfirm').modal('hide');

    		RequestService.delete(data, 'property').then(function(response) {
    			$scope.reloadData();
    		});
    	}

    	$('#indexConfirm').modal('show');
    }

    $scope.init();
});

app.controller('propertyDetailCtrl', function($scope, $stateParams, $state, RequestService) {
	$scope.baseUrl = RequestService.url.replace('adm/', '');
	$scope.init = function() {
		if ($stateParams.property) { 
			$scope.property = $stateParams.property;

			$scope.imgLen = [];

			var data = { 'id': $stateParams.property.id };

			RequestService.getby(data, 'property', 'id').then(function(property) {
				$scope.property = property;

				for (var ii = 0; ii < $scope.property.img.length; ii++) {
					$scope.imgLen.push(ii);
				}

				if ($scope.property.auction_date) {
					var auction_date = new Date($scope.property.auction_date);
					$scope.auction_date = auction_date.getMonth() + 1 + '/' + auction_date.getDate() + '/' + auction_date.getFullYear();
				} else {
					$scope.auction_date = '';
				}

				if ($scope.property.status) {
					$scope.property.status_ = 'Aktif';
				} else {
					$scope.property.status_ = 'Non-Aktif';
				}
			});

		} else $state.go('app.property.index');
	}

	$scope.init();
});

app.controller('propertyCreateCtrl', function($scope, $state, $rootScope, $compile, RequestService) {
	$scope.init = function() {
		$scope.imgFile = [];
		$scope.price = 0;
		$scope.cities = [];

		$scope.errorName = false;
		$scope.errorImg0 = false;
		$scope.errorAddress = false;
		$scope.errorCity = false;
		$scope.errorPrice = false;
		$scope.errorLocation = false;
		$scope.errorSurfaceArea = false;
		$scope.errorBuildingArea = false;
		// $scope.errorAuctionDate = false;

		$('.datepicker').datepicker({
			autoclose: true
		});

		$scope.status = true;

		RequestService.getall('city').then(function(cities) {
			for (var ii = 0; ii < cities.length; ii++) {
				$scope.cities.push(cities[ii]);
			}
		});
	}

	$scope.insertProperty = function() {
		var check = true;

		$scope.errorName = false;
		$scope.errorImg0 = false;
		$scope.errorAddress = false;
		$scope.errorCity = false;
		$scope.errorPrice = false;
		$scope.errorLocation = false;
		$scope.errorSurfaceArea = false;
		$scope.errorBuildingArea = false;
		// $scope.errorAuctionDate = false;

		// var number = $('#divImg .form-group').length;

		if (!$scope.name) {
			$scope.errorName = true;
			check = false;
		}

		if (!$scope.imgFile[0]) {
			$scope.errorImg0 = true;
			check = false;
		}

		if (!$scope.address) {
			$scope.errorAddress = true;
			check = false;
		}

		if (!$scope.city) {
			$scope.errorCity = true;
			check = false;
		}

		if (!$scope.price) {
			$scope.errorPrice = true;
			check = false;
		}

		if (!$scope.location) {
			$scope.errorLocation = true;
			check = false;
		}

		if (!$scope.surface_area) {
			$scope.errorSurfaceArea = true;
			check = false;
		}

		if (!$scope.building_area) {
			$scope.errorBuildingArea = true;
			check = false;
		}

		// if (!$scope.auction_date) {
		// 	$scope.errorAuctionDate = true;
		// 	check = false;
		// }

		if (check) {
			var img = [];
			var status;
			var auction_date;

			for (var ii = 0; ii < $scope.imgFile.length; ii++) {
				if ($scope.imgFile[ii] && $('#file'+ii).length > 0) {
					img.push($scope.imgFile[ii].base64);
				};
			};

			if ($scope.status) {
				status = 1;
			} else {
				status = 0;
			}

			if ($scope.auction_date) {
				auction_date = new Date($scope.auction_date);
			} else {
				auction_date = '';
			}

			var data = {
				'name': $scope.name,
				'img': img,
				'address': $scope.address,
				'city': $scope.city,
				'price': $scope.price,
				'auction_date': auction_date,
				'location': $scope.location,
				'surface_area': $scope.surface_area,
				'building_area': $scope.building_area,
				'info': $scope.info,
				'status': status
			}

			RequestService.save(data, 'property').then(function(response) {
				// RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
					$state.go('app.property.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// });
			});
		}
	}

	$scope.addImg = function() {
		var number = $('#divImg .form-group').length;

		$('#divImg').append($compile('\
			<div class="form-group">\
			<div class="col-sm-offset-2 col-sm-10">\
			<label class="btn btn-primary" for="file'+number+'">\
			<input type="file" ng-model="imgFile[['+number+']]" name="file'+number+'" id="file'+number+'" accept="image/*" maxsize="2000" style="display:none;" base-sixty-four-input>\
			Browse\
			</label>\
			\
			<button class="btn btn-flat btn-danger" id="btn-remove" type="button">\
			<i class="fa fa-times"></i>\
			</button>\
			\
			<span class="text-muted" ng-if="imgFile['+number+'] == null">*Ukuran yang dianjurkan 360 x 240 (.jpg)</span>\
			<span class="text-muted" ng-if="imgFile['+number+']">{{ imgFile['+number+'].filename }}</span>\
			</div>\
			\
			<div class="col-sm-offset-2 col-sm-10" ng-if="imgFile['+number+']">\
			<img ng-src="data:image;base64,{{ imgFile['+number+'].base64 }}" class="thumb">\
			</div>\
			</div>\
			')($scope));

		$('#divImg').on('click', '#btn-remove', function(){
			$(this).parent().parent().remove();
		});
	};

	$scope.init();
});

app.controller('propertyEditCtrl', function($scope, $stateParams, $state, $rootScope, $timeout, $compile, RequestService) {
	$scope.init = function() {
		if ($stateParams.property) { 
			$scope.cities = [];
			$scope.baseUrl = RequestService.url.replace('adm/', '');

			RequestService.getall('city').then(function(cities) {
				for (var ii = 0; ii < cities.length; ii++) {
					$scope.cities.push(cities[ii]);
				}
				
				$scope.property = $stateParams.property;

				$scope.imgFile = [];

				$scope.imgLen = [];

				var data = { 'id': $stateParams.property.id };

				RequestService.getby(data, 'property', 'id').then(function(property) {
					$scope.property = property;

					for (var ii = 0; ii < $scope.property.img.length; ii++) {
						$scope.imgLen.push(ii);
					}

					$scope.errorName = false;
					$scope.errorAddress = false;
					$scope.errorCity = false;
					$scope.errorPrice = false;
					$scope.errorLocation = false;
					// $scope.errorAuctionDate = false;

					$('#divImgOld').on('click', '#btn-remove', function(){
						$(this).parent().parent().remove();
					});

					var auction_date = new Date($scope.property.auction_date);
					$scope.auction_date = auction_date.getMonth() + 1 + '/' + auction_date.getDate() + '/' + auction_date.getFullYear();

					$('.datepicker').datepicker({
						autoclose: true
					});

					if (!$scope.property.auction_date) {
						$timeout(function() {
							$('.datepicker').val('');
							$scope.auction_date = '';
						}, 100);
					}

					if ($scope.property.status) {
						$scope.status_ = true;
					} else {
						$scope.status_ = false;
					}
				});
			});
		} else $state.go('app.property.index');
	}

	$scope.updateProperty = function() {
		var check = true;

		$scope.errorName = false;
		$scope.errorAddress = false;
		$scope.errorCity = false;
		$scope.errorPrice = false;
		$scope.errorLocation = false;
		// $scope.errorAuctionDate = false;

		// var number = $('#divImg .form-group').length;

		// var numberOld = $('#divImgOld .form-group').length;

		if (!$scope.property.name) {
			$scope.errorName = true;
			check = false;
		}

		if (!$scope.property.address) {
			$scope.errorAddress = true;
			check = false;
		}

		if (!$scope.property.city) {
			$scope.errorCity = true;
			check = false;
		}

		if (!$scope.property.price) {
			$scope.errorPrice = true;
			check = false;
		}

		// if (!$scope.auction_date) {
		// 	$scope.errorAuctionDate = true;
		// 	check = false;
		// }

		if (!$scope.property.location) {
			$scope.errorLocation = true;
			check = false;
		}

		if (check) {
			// var auction_date = new Date($scope.auction_date);

			var img = [];
			var old_img = [];
			var status;

			for (var ii = 0; ii < $scope.imgFile.length; ii++) {
				if ($scope.imgFile[ii] && $('#file'+ii).length > 0) {
					img.push($scope.imgFile[ii].base64);
				};
			};

			for (var ii = 0; ii < $scope.property.img.length; ii++) {
				if ($('#fileOld'+ii).length > 0) {
					img.push($scope.property.img[ii]);
				};
			};

			// if ($scope.imgFile0) {
			// 	img = [$scope.imgFile0.base64];

			// 	if ($scope.imgFile2) {
			// 		img.push($scope.imgFile2.base64);
			// 	}

			// 	if ($scope.imgFile3) {
			// 		img.push($scope.imgFile3.base64);
			// 	}
			// } else {
			// 	img = $scope.property.img;
			// }

			if ($scope.status_) {
				status = 1;
			} else {
				status = 0;
			}

			var data = {
				'id': $stateParams.property.id,
				'name': $scope.property.name,
				'img': img,
				'old_img': old_img,
				'address': $scope.property.address,
				'city': $scope.property.city,
				'price': $scope.property.price,
				'auction_date': $scope.auction_date,
				'location': $scope.property.location,
				'surface_area': $scope.property.surface_area,
				'building_area': $scope.property.building_area,
				'info': $scope.property.info,
				'status': status
			}

			RequestService.save(data, 'property').then(function(response) {
				// if ($scope.imgFile) {
				// 	RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
				// 		$state.go('app.property.index');

				// 		$rootScope.modalHeader  = 'Pesan';
				// 		$rootScope.modalMessage = response.data.message;

				// 		$('#indexModal').modal('show');
				// 	});
				// } else {
					$state.go('app.property.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// }
			});
		}
	}

	$scope.addImg = function() {
		var number = $('#divImg .form-group').length;

		$('#divImg').append($compile('\
			<div class="form-group">\
			<div class="col-sm-offset-2 col-sm-10">\
			<label class="btn btn-primary" for="file'+number+'">\
			<input type="file" ng-model="imgFile[['+number+']]" name="file'+number+'" id="file'+number+'" accept="image/*" maxsize="2000" style="display:none;" base-sixty-four-input>\
			Browse\
			</label>\
			\
			<button class="btn btn-flat btn-danger" id="btn-remove" type="button">\
			<i class="fa fa-times"></i>\
			</button>\
			\
			<span class="text-muted" ng-if="imgFile['+number+'] == null">*Ukuran yang dianjurkan 360 x 240 (.jpg)</span>\
			<span class="text-muted" ng-if="imgFile['+number+']">{{ imgFile['+number+'].filename }}</span>\
			</div>\
			\
			<div class="col-sm-offset-2 col-sm-10" ng-if="imgFile['+number+']">\
			<img ng-src="data:image;base64,{{ imgFile['+number+'].base64 }}" class="thumb">\
			</div>\
			</div>\
			')($scope));

		$('#divImg').on('click', '#btn-remove', function(){
			$(this).parent().parent().remove();
		});
	};

	$scope.init();
});