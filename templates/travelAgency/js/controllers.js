app.controller('agencyListCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.travelAgencies = [];

		$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
			var q = $q.defer();
			var data = {};

			RequestService.getall('travelagency').then(function(data) {
				q.resolve(data);
			});

			return q.promise;
		}).withOption('createdRow', createdRow);

		$scope.dtColumns = [
		DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withOption('searchable', false).withClass('text-center').renderWith(numberHtml),
		DTColumnBuilder.newColumn('name').withTitle('Nama travel'),
		DTColumnBuilder.newColumn('province').withTitle('Provinsi'),
		DTColumnBuilder.newColumn('city').withTitle('Kota/Kabupaten'),
		DTColumnBuilder.newColumn('district').withTitle('Kecamatan'),
		DTColumnBuilder.newColumn('zone').withTitle('kelurahan'),
		DTColumnBuilder.newColumn(null).withTitle('Action').notSortable().withOption('searchable', false).renderWith(actionHtml)
		];

		$scope.dtInstance = {};

		function createdRow(row, data, dataIndex) {
			$compile(angular.element(row).contents())($scope);
		}

		function numberHtml(data, type, full, meta) {
			return meta.row + 1;
		}

		function actionHtml(data, type, full, meta) {
			$scope.travelAgencies[meta.row] = data;

			return '\
			<a ng-click="edit(' + meta.row + ')" class="btn btn-warning btn-xs">Manage Agency</a>\
			<a ng-click="deleteTravelAgent(' + meta.row + ')" class="btn btn-danger btn-xs">Delete</a>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.edit = function(index){
		$state.go('app.manageAgency',{"name":$scope.travelAgencies[index].name});
	}
	$scope.addAgency = function(){
		$state.go('app.manageAgency',{"name":null});
	}

	$scope.deleteTravelAgent = function(index){
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Are you sure you want to delete this agency?';
		$rootScope.confirmButton  = 'Hapus';
		$scope.travelagencyId = null;
		$scope.travelagencyId = $scope.travelAgencies[index].id;
		$scope.travelagencyImg = $scope.travelAgencies[index].img;
		$scope.travelagencyPortfolio = $scope.travelAgencies[index].portfolio_img;
		$rootScope.modalAction = function() {
			$('#indexConfirm').modal('hide');
			var data = {};
			data.id = $scope.travelagencyId;
			data.img = $scope.travelagencyImg;
			data.portfolio_img = $scope.travelagencyPortfolio;
			RequestService.delete(data,'travelagency').then(function(response) {
				$scope.reloadData();
			});
		}
		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('manageAgencyCtrl', function($scope, $rootScope, $state, $compile, $q, RequestService, TimeService,$stateParams,bsLoadingOverlayService) {
	$scope.init = function() {
		$scope.baseUrl = RequestService.url.replace('adm/', '');
		$scope.association = [{"name":"HIMPUH"},{"name":"AMPHURI"},{"name":"KESTHURI"},{"name":"ASPHURINDO"}];
		$scope.agency = {};
		$scope.province = [];
		$scope.regency = [];
		$scope.district = [];
		$scope.zone = [];
		$scope.village = {};
		$scope.city = {};
		$scope.destrict = {};
		$scope.provinc = {};
		$scope.old_portfolio_img = [];
		
		$('.datepicker').datepicker({
			autoclose: true
		});
		RequestService.getAddressByType(null,"province").then(function(response) {
			$scope.province = response;
			if(!$stateParams.name){
				$scope.update = false;
			}else{	
				$scope.update = true;
				$scope.getTravelAgent();		
			}
		});
		
	}

	$scope.addImg = function() {
		var number = $('#divImg .form-group').length;
		// if (number > 4)
		// 	return;

		$('#divImg').append($compile('\
			<div class="form-group">\
			<div class="col-sm-offset-2 col-sm-10">\
			<label class="btn btn-primary" for="file'+number+'">\
			<input type="file" ng-model="agency.portfolio_img_file[['+number+']]" name="file'+number+'" id="file'+number+'" accept="image/*" style="display:none;" base-sixty-four-input>\
			Browse\
			</label>\
			\
			<button class="btn btn-flat btn-danger" id="btn-remove" type="button">\
			<i class="fa fa-times"></i>\
			</button>\
			\
			<span class="text-muted" ng-if="agency.portfolio_img_file['+number+'] == null">Pilih sebuah gambar</span>\
			<span class="text-muted" ng-if="agency.portfolio_img_file['+number+']">{{ agency.portfolio_img_file['+number+'].filename }}</span>\
			</div>\
			\
			<div class="col-sm-offset-2 col-sm-10" ng-if="agency.portfolio_img_file['+number+']">\
			<img ng-src="data:image;base64,{{ agency.portfolio_img_file['+number+'].base64 }}" class="thumb">\
			</div>\
			</div>\
			')($scope));

		$('#divImg').on('click', '#btn-remove', function(){
			$(this).parent().parent().remove();
		});
	};

	$scope.getTravelAgent=function(){
		var data = {};
		data.name = $stateParams.name; 
		RequestService.getby(data,'travelagency','name').then(function(response) {
			$scope.agency = response[0];
			for (var ii = 0; ii < $scope.agency.portfolio_img.length; ii++) {
				$scope.old_portfolio_img.push(ii);
			}

			$('#divImgOld').on('click', '#btn-remove', function(){
				$(this).parent().parent().remove();
			});

			var date = new Date($scope.agency.agreement_start_date);
			$scope.agency.agreement_start_date = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
			date = new Date($scope.agency.agreement_end_date);
			$scope.agency.agreement_end_date = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
			
			if($scope.province.length >0)
			{
				for(var i=0;i<$scope.province.length;i++)
				{
					if($scope.province[i].province == response[0].province)
					{	
						$scope.provinc = $scope.province[i];
						$scope.getRegency($scope.province[i].id);
					}
				}
			}						
		});
	}

	$scope.getRegency = function(id){
		$scope.regency = [];
		$scope.district = [];
		$scope.zone = [];
		$scope.village = {};
		$scope.city = {};
		$scope.destrict = {};
		
		RequestService.getAddressByType(id,"regency").then(function(response) {
			if($scope.update){
				for(var i=0;i<response.length;i++){
					if(response[i].regency == $scope.agency.city){
						$scope.city = response[i]; 
						$scope.getDistrict(response[i].id);
					}
				}
			}

			$scope.regency = response;

		});
	}

	$scope.getDistrict = function(id){
		$scope.district = [];
		$scope.zone = [];
		$scope.village = {};
		$scope.destrict = {};
		RequestService.getAddressByType(id,"district").then(function(response) {
			if($scope.update){
				for(var i=0;i<response.length;i++){
					if(response[i].district == $scope.agency.district){
						$scope.destrict = response[i]; 
						$scope.getZone(response[i].id);
					}
				}
			}
			$scope.district = response;
		});
	}
	$scope.getZone = function(id){
		$scope.zone = [];
		$scope.village = {};
		RequestService.getAddressByType(id,"village").then(function(response) {
			if($scope.update){
				for(var i=0;i<response.length;i++){
					if(response[i].village == $scope.agency.zone){
						$scope.village = response[i]; 
					}
				}
			}
			$scope.zone = response;
		});
	}
	$scope.manageAgency = function(){
		var check = true;

		$scope.travel_name = false;
		$scope.provinsiAddress = false;
		$scope.kota = false;
		$scope.dist = false;
		$scope.vill = false;
		$scope.agenaddress = false;
		$scope.picname = false;
		$scope.pichp = false;
		$scope.umrlicense = false;
		$scope.hjlicense = false;
		$scope.license = false;
		$scope.asita = false;
		$scope.associ = false;
		$scope.iata = false;
		$scope.agrement = false;
		$scope.startDate = false;
		$scope.endDate = false;
		if (!$scope.agency.name) 
		{
			$scope.travel_name = true;
			check = false;
		}
		if (!$scope.provinc.province) 
		{
			$scope.provinsiAddress = true;
			check = false;
		}
		if (!$scope.city.regency) 
		{
			$scope.kota = true;
			check = false;
		}
		if (!$scope.destrict.district) 
		{
			$scope.dist = true;
			check = false;
		}
		if (!$scope.village.village)
		{
			$scope.vill = true;
			check = false;
		}
		if(!$scope.agency.address)
		{
			$scope.agenaddress = true;
			check = false;
		}
		if(!$scope.agency.pic_name){
			$scope.picname = true;
			check = false;
		}
		if(!$scope.agency.pic_hp){
			$scope.pichp = true;
			check = false;
		}
		if(!$scope.agency.umroh_license){
			$scope.umrlicense = true;
			check = false;
		}
		if(!$scope.agency.hajj_license){
			$scope.hjlicense = true;
			check = false;
		}
		if(!$scope.agency.travel_license){
			$scope.license = true;
			check = false;
		}
		if(!$scope.agency.asita_no){
			$scope.asita = true;
			check = false;
		}
		if(!$scope.agency.association){
			$scope.associ = true;
			check = false;
		}
		if(!$scope.agency.iata_no){
			$scope.iata = true;
			check = false;
		}
		if(!$scope.agency.agreement){
			$scope.agrement = true;
			check = false;
		}
		if(!$scope.agency.agreement_start_date){
			$scope.startDate = true;
			check = false;
		}
		if(!$scope.agency.agreement_end_date){
			$scope.endDate = true;
			check = false;
		}
		if(!$scope.update){
			if(!$scope.agency.logo_img){
				$scope.errorImg = true;
				check = false;
			}
			if(!$scope.agency.portfolio_img_file){
				$scope.errorPortfolio = true;
				check = false;
			}
		}
		
		if (check) {
			$scope.agency.province = $scope.provinc.province;
			$scope.agency.city = $scope.city.regency;
			$scope.agency.district = $scope.destrict.district;
			$scope.agency.zone = $scope.village.village;

			if($scope.update){
				var img = '';
				var old_img = '';

				if ($scope.agency.logo_img) {
					img = $scope.agency.logo_img.base64;
					old_img = $scope.agency.img;
				} else {
					img = $scope.agency.img;
				}

				var p_img = [];

				var ii = 0;
				for (var image in $scope.agency.portfolio_img_file) {
					if ($scope.agency.portfolio_img_file[ii] && $('#file'+ii).length > 0) {
						p_img.push($scope.agency.portfolio_img_file[ii].base64);
					}
					ii++;
				};

				for (var ii = 0; ii < $scope.agency.portfolio_img.length; ii++) {
					if ($('#fileOld'+ii).length > 0) {
						p_img.push($scope.agency.portfolio_img[ii]);
					};
				};

				$scope.agency.img = img;
				$scope.agency.old_img = old_img;
				$scope.agency.portfolio_img = p_img;

				RequestService.save($scope.agency,'travelagency').then(function(response) {
					$state.go('app.travelAgency');	
				});

			}else{
				var p_img = [];

				var ii = 0;
				for (var img in $scope.agency.portfolio_img_file) {
					if ($scope.agency.portfolio_img_file[ii] && $('#file'+ii).length > 0) {
						p_img.push($scope.agency.portfolio_img_file[ii].base64);
					};

					ii++;
				};

				$scope.agency.img = $scope.agency.logo_img.base64;
				$scope.agency.portfolio_img = p_img;

				RequestService.save($scope.agency,'travelagency').then(function(response) {
					$state.go('app.travelAgency');	
				});
			}

		}
			// 	if(!$scope.files){
			// 		alert("Please select Portfolio images");
			// 		return;
			// 	}else{

			// 	}

			// alert(JSON.stringify($scope.agency));
				// }
			}

			$scope.upload = function(){
				RequestService.upload($scope.file).then(function(response) {
					$state.go('app.travelAgency');	
				});
			}

			var randomString=function() {
				var text = "";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

				for (var i = 0; i < 5; i++)
					text += possible.charAt(Math.floor(Math.random() * possible.length));

				return text;
			}
			$scope.cancel = function(){
				$state.go('app.travelAgency');
			}

			$scope.init();
		});

// app.controller('settingUserCreateCtrl', function($scope, $state, $rootScope, RequestService) {
// 	$scope.init = function() {
// 		$scope.errorEmail = false;
// 		$scope.errorName = false;
// 		$scope.errorType = false;
// 		$scope.errorPassword = false;
// 		$scope.errorRepassword = false;

// 		$scope.types = [{'name': 'ADMIN'}, {'name': 'ADMIN CABANG'}, {'name': 'ADMIN TRAVEL'}];
// 	}

// 	$scope.insertUser = function() {
// 		var check = true;

// 		$scope.errorEmail = false;
// 		$scope.errorName = false;
// 		$scope.errorType = false;
// 		$scope.errorPassword = false;
// 		$scope.errorRepassword = false;

// 		if (!$scope.email) {
// 			$scope.errorEmail = true;
// 			check = false;
// 		}

// 		if (!$scope.name) {
// 			$scope.errorName = true;
// 			check = false;
// 		}

// 		if (!$scope.type) {
// 			$scope.errorType = true;
// 			check = false;
// 		}

// 		if (!$scope.password) {
// 			$scope.errorPassword = true;
// 			check = false;
// 		}

// 		if (!$scope.repassword) {
// 			$scope.errorRepassword = true;
// 			check = false;
// 		}

// 		if ($scope.password != $scope.repassword) {
// 			$scope.errorPassword = true;
// 			$scope.errorRepassword = true;
// 			check = false;
// 		}		

// 		if (check) {
// 			var data = {
// 				'name': $scope.name,
// 				'email': $scope.email,
// 				'user_type': $scope.type,
// 				'pass': $scope.password,
// 				'description': $scope.description
// 			}

// 			RequestService.save(data, 'user').then(function(response) {
// 				$state.go('app.settingUser.index');

// 				$rootScope.modalHeader  = 'Pesan';
// 				$rootScope.modalMessage = response.data.message;

// 				$('#indexModal').modal('show');
// 			});
// 		}
// 	}

// 	$scope.init();
// });

// app.controller('settingUserEditCtrl', function($scope, $stateParams, $state, $rootScope, RequestService) {
// 	$scope.init = function() {
// 		$scope.change=false;
// 		if ($stateParams.user) { 
// 			$scope.user = $stateParams.user;

// 			$scope.user.type = $scope.user.user_type;

// 			$scope.errorEmail = false;
// 			$scope.errorName = false;

// 			$scope.types = [{'name': 'ADMIN'}, {'name': 'ADMIN CABANG'}, {'name': 'ADMIN TRAVEL'}];
// 		} else $state.go('app.settingUser.index');
// 	}

// 	$scope.updateUser = function() {
// 		var check = true;

// 		$scope.errorEmail = false;
// 		$scope.errorName = false;

// 		if (!$scope.user.email) {
// 			$scope.errorEmail = true;
// 			check = false;
// 		}

// 		if (!$scope.user.name) {
// 			$scope.errorName = true;
// 			check = false;
// 		}
// 		if ($scope.change) 
// 		{
// 			if (!$scope.user.oPass) 
// 			{
// 				alert("Please enter Old password");
// 				return;
// 			}
// 			else if (!$scope.user.nPass) 
// 			{
// 				alert("Please enter new password");
// 				return;
// 			}
// 			else if (!$scope.user.rPass) 
// 			{
// 				alert("Please enter new password");
// 				return;
// 			}
// 			else if ($scope.user.pass!=$scope.user.oPass) 
// 			{
// 				alert("Old password doesnot match");
// 				return;
// 			}
// 			else if ($scope.user.nPass!=$scope.user.rPass)
// 			{
// 				alert("New password and confirm password doesnot match");
// 				return;
// 			}
// 			else
// 			{
// 				$scope.user.pass=$scope.user.nPass;
// 			}
// 		}
// 		if (check) {
// 			var data = {
// 				'id': $scope.user.id,
// 				'name': $scope.user.name,
// 				'email': $scope.user.email,
// 				'pass': $scope.user.pass,
// 				'user_type': $scope.user.type,
// 				'description': $scope.user.description
// 			}

// 			alert(JSON.stringify($scope.user));
// 			RequestService.save(data, 'user').then(function(response) {
// 				$state.go('app.settingUser.index');

// 				$rootScope.modalHeader  = 'Pesan';
// 				$rootScope.modalMessage = response.data.message;

// 				$('#indexModal').modal('show');
// 			});
// 		}
// 	};

// 	/*$scope.changePassword=function()
// 	{

// 	}*/
// 	$scope.init();
// });