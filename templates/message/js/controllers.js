app.controller('MessageIndexCtrl', function($scope) {
	$scope.init = function() {

	}

	$scope.init();
});

app.controller('MessageCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService, $uibModal) {
	$scope.init = function() {
		$scope.messages=[];

		$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
			var q = $q.defer();
			var data = {};

			RequestService.getall('message').then(function(data) {
				q.resolve(data);
			});

			return q.promise;
		}).withOption('createdRow', createdRow);

		$scope.dtColumns = [
		DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
		DTColumnBuilder.newColumn('customer_name').withTitle('Customer'),
		DTColumnBuilder.newColumn(null).withTitle('Subject').renderWith(detailHtml),
		DTColumnBuilder.newColumn('create_date').withTitle('Date Create'),
		DTColumnBuilder.newColumn('reply_date').withTitle('Reply Date'),
		DTColumnBuilder.newColumn(null).withTitle('Action').notSortable().withOption('searchable', false).withOption('width', '106px').renderWith(actionHtml)
		];

		$scope.dtInstance = {};

		function createdRow(row, data, dataIndex) {
			$compile(angular.element(row).contents())($scope);
		}

		function numberHtml(data, type, full, meta) {
			return meta.row + 1;
		}

		function detailHtml(data, type, full, meta) {
			return '<button type="button" class="btn btn-link" ng-click="reply(messages['+meta.row+'])">' + data.subject + '</button> ';
		}

		function actionHtml(data, type, full, meta) {
			$scope.messages[meta.row] = data;

			return '\
			<button type="button" class="button btn-success" ng-if="messages['+meta.row+'].reply_date==null" ng-click="reply(messages['+meta.row+'])">Reply</button>\
			<button type="button" class="button btn-danger" ng-click="delete(messages['+meta.row+'])">Delete</button>\
			';
		}
		$rootScope.modalHeader  = 'Pesan';
		$rootScope.modalMessage = 'Messages fetched';

		$('#indexModal').modal('show');
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.reply = function(message)
	{
		var modalInstance = $uibModal.open({
			templateUrl: 'templates/message/html/reply-model.html',
			controller: 'ReplyMessageCtrl',
			size: 'md',
			animation: true,
			resolve: {
				item: function () {
					return message;
				}
			}
		});
	}
	$scope.delete = function(message) {
		RequestService.delete(message,'message').then(function(response) {
			$scope.reloadData();

			$rootScope.modalHeader  = 'Pesan';
			$rootScope.modalMessage = response.data.message;
			$('#indexModal').modal('show');
		}); 
	};
	$scope.$on('message:sent',function(e)
	{
		$scope.reloadData();
	});

	$scope.init();
});


app.controller('ReplyMessageCtrl', function($scope, $state, RequestService,$uibModalInstance,item,$rootScope) {
	$scope.message;
	$scope.init = function() {
		$scope.$on('$viewContentLoaded', function(){
			$.AdminLTE.layout.activate();
		});
		$scope.message=item;
		console.log(item);
	};

	$scope.sendMessage = function() {
		$scope.noReply = false;
		var check = true;
		if (!$scope.message.reply) 
		{
			$scope.noReply = true;
			check = false;
		}
		if (check) 
		{
			var obj={};
			obj.reply = $scope.message.reply;
			obj.id= $scope.message.id;
			RequestService.update(obj,'message').then(function(response) {
				$scope.init();
				$scope.cancel();
				$rootScope.$broadcast('message:sent');
                /*$rootScope.modalHeader  = 'Pesan';
                $rootScope.modalMessage = response.data.message;
                $('#indexModal').modal('show');*/
            });
		}
	};

	$scope.cancel=function()
	{
		console.log("message send");
		$uibModalInstance.close();
	};

	$scope.init();
});