app.controller('suggestionCtrl', function($scope) {
	$scope.init = function() {

	}

	$scope.init();
});

app.controller('suggestionIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.suggestions = [];

	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
	        var q = $q.defer();

	        RequestService.getall('suggestion').then(function(data) {
				q.resolve(data);
			});

	        return q.promise;
	    }).withOption('createdRow', createdRow);

	    $scope.dtColumns = [
	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
	        DTColumnBuilder.newColumn('name').withTitle('Nama'),
	        DTColumnBuilder.newColumn('phone').withTitle('Nomor Telepon'),
	        DTColumnBuilder.newColumn('email').withTitle('Email'),
	        DTColumnBuilder.newColumn('suggestion').withTitle('Saran'),
	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
	    ];

	    $scope.dtInstance = {};

	    function createdRow(row, data, dataIndex) {
	    	$compile(angular.element(row).contents())($scope);
	    }

	    function numberHtml(data, type, full, meta) {
	    	return meta.row + 1;
	    }

		// function imageHtml(data, type, full, meta) {
		//        return '<img src="' + data.img + '" width="55px">';
		// }

		// function timeHtml(data, type, full, meta) {
		// 	var date = new Date(data.time);

		// 	return TimeService.days[date.getDay()] + ', ' 
		// 		+ ('0' + date.getDate()).slice(-2) + ' '
		// 		+ TimeService.months[date.getMonth()] + ' '
		// 		+ date.getFullYear() + ' '
		// 		+ ('0' + date.getHours()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2);
		// }

		function actionHtml(data, type, full, meta) {
			$scope.suggestions[meta.row] = data;

			return '\
				<button class="btn btn-danger btn-xs btn-block" ng-click="destroySuggestion(suggestions[' + meta.row + '].id)"><i class="fa fa-trash-o"></i> Delete</button>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			suggestion: objectData
		});
	}

	$scope.destroySuggestion = function(id) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'suggestion').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});