app.controller('promoCtrl', function($scope) {
	$scope.init = function() {

	}

	$scope.init();
});

// app.controller('promoIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
// 	$scope.init = function() {
// 		$scope.promos = [];

// 	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
// 	        var q = $q.defer();
// 	        var data = {};

// 	        if ($scope.status != 99) {
// 		        data.status = $scope.status;
// 	        }

// 	        RequestService.getbystatus(data, 'promo').then(function(data) {
// 				q.resolve(data);
// 			});

// 	        return q.promise;
// 	    }).withOption('createdRow', createdRow);

// 	    $scope.dtColumns = [
// 	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
// 	        DTColumnBuilder.newColumn(null).withTitle('Gambar').notSortable().renderWith(imageHtml),
// 	        DTColumnBuilder.newColumn(null).withTitle('Deskripsi').renderWith(detailHtml),
// 	        DTColumnBuilder.newColumn(null).withTitle('Status').renderWith(statusHtml),
// 	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
// 	    ];

// 	    $scope.dtInstance = {};

// 	    function createdRow(row, data, dataIndex) {
// 	    	$compile(angular.element(row).contents())($scope);
// 	    }

// 	    function numberHtml(data, type, full, meta) {
// 	    	return meta.row + 1;
// 	    }

// 	    function detailHtml(data, type, full, meta) {
// 	        return '<a href="" ng-click="navigateTo(\'app.promo.detail\', promos[' + meta.row + '])">' + data.description + '</a>';
// 		}

// 	    function statusHtml(data, type, full, meta) {
// 	    	var status;

// 	    	if (data.status) {
// 	    		status = '<span class="status-active">Aktif</span>';
// 	    	} else {
// 	    		status = '<span class="status-nonactive">Non-Aktif</span>';
// 	    	}

// 	        return status;
// 		}

// 	    function imageHtml(data, type, full, meta) {
// 	        return '<img src="data:image;base64,' + data.img + '" width="100px">';
// 		}

// 		// function timeHtml(data, type, full, meta) {
// 		// 	var date = new Date(data.time);

// 		// 	return TimeService.days[date.getDay()] + ', ' 
// 		// 		+ ('0' + date.getDate()).slice(-2) + ' '
// 		// 		+ TimeService.months[date.getMonth()] + ' '
// 		// 		+ date.getFullYear() + ' '
// 		// 		+ ('0' + date.getHours()).slice(-2) + ':'
// 		// 		+ ('0' + date.getMinutes()).slice(-2) + ':'
// 		// 		+ ('0' + date.getMinutes()).slice(-2);
// 		// }

// 		function actionHtml(data, type, full, meta) {
// 			$scope.promos[meta.row] = data;

// 			return '\
// 				<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.promo.edit\', promos[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
// 				<button class="btn btn-danger btn-xs btn-block" ng-click="destroyPromo(promos[' + meta.row + '].id)"><i class="fa fa-trash-o"></i> Delete</button>\
// 			';
// 		}
// 	}

// 	$scope.reloadData = function() {
// 		$scope.dtInstance.reloadData();
// 	}

// 	$scope.navigateTo = function(targetPage, objectData) {
// 		$state.go(targetPage, {
// 			promo: objectData
// 		});
// 	}

// 	$scope.destroyPromo = function(id) {
// 		$rootScope.confirmHeader  = 'Konfirmasi';
// 		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
// 		$rootScope.confirmButton  = 'Hapus';

// 		$rootScope.modalAction = function() {
// 			var data = { 'id' : id };

// 			$('#indexConfirm').modal('hide');
			
// 			RequestService.delete(data, 'promo').then(function(response) {
// 				$scope.reloadData();
// 			});
// 		}

// 		$('#indexConfirm').modal('show');
// 	}

// 	$scope.init();
// });

app.controller('promoIndexCtrl', function($scope, $rootScope, $state, RequestService) {
	$scope.promos = [];
	$scope.pages = [];
	$scope.last;
	$scope.limit = 10;
	$scope.currentPage = 1;
	$scope.baseUrl = RequestService.url.replace('adm/', '');
	var data = {};

	$scope.init = function() {
        // if ($scope.status != 99) {
	       //  data.status = $scope.status;
        // }

		RequestService.getbypaging(data, 'promo', $scope.currentPage, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.promos.push(data[ii]);
			};
		});

		RequestService.gettotal(data, 'promo').then(function(data) {
			for (var ii = 1; ii <= Math.ceil(data / $scope.limit); ii++) {
				$scope.pages.push(ii);
			};

			$scope.last = $scope.pages.length;
		});
	};

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			promo: objectData
		});
	}

	$scope.gotoPage = function(page) {
		$scope.promos = [];
		$scope.currentPage = page;

		RequestService.getbypaging(data, 'promo', page, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.promos.push(data[ii]);
			};
		});
	}

	$scope.reloadData = function() {
		$scope.promos = [];
		$scope.pages = [];

		if ($scope.status != 99) {
	        data.status = $scope.status;
        } else {
        	data = {};
        }

		RequestService.getbypaging(data, 'promo', $scope.currentPage, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.promos.push(data[ii]);
			};
		});

		RequestService.gettotal(data, 'promo').then(function(data) {
			for (var ii = 1; ii <= Math.ceil(data / $scope.limit); ii++) {
				$scope.pages.push(ii);
			};

			$scope.last = $scope.pages.length;
		});
	}

	$scope.destroyPromo = function(id, old_img) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id, 'old_img' : old_img };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'promo').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('promoDetailCtrl', function($scope, $stateParams, $state, RequestService) {
	$scope.baseUrl = RequestService.url.replace('adm/', '');
	$scope.init = function() {
		if ($stateParams.promo) { 
			$scope.promo = $stateParams.promo;

			if ($scope.promo.status) {
				$scope.promo.status_ = 'Aktif';
			} else {
				$scope.promo.status_ = 'Non-Aktif';
			}
		} else $state.go('app.promo.index');
	}

	$scope.init();
});

app.controller('promoCreateCtrl', function($scope, $state, $rootScope, RequestService) {
	$scope.init = function() {
		$scope.errorImg = false;
		$scope.errorDescription = false;

		$scope.status = true;
	}

	$scope.insertPromo = function() {
		var check = true;

		$scope.errorImg = false;
		$scope.errorDescription = false;

		if (!$scope.imgFile) {
			$scope.errorImg = true;
			check = false;
		}

		if (!$scope.description) {
			$scope.errorDescription = true;
			check = false;
		}

		if (check) {
			var status;

			if ($scope.status) {
				status = 1;
			} else {
				status = 0;
			}

			var data = {
				'img': $scope.imgFile.base64,
	            'description': $scope.description,
	            'status': status,
	            'content': $scope.content
			}

			RequestService.save(data, 'promo').then(function(response) {
				// RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
					$state.go('app.promo.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// });
			});
		}
	}

	$scope.init();
});

app.controller('promoEditCtrl', function($scope, $stateParams, $state, $rootScope, RequestService) {
	$scope.init = function() {
		if ($stateParams.promo) { 
			$scope.promo = $stateParams.promo;

			$scope.errorDescription = false;

			if ($scope.promo.status) {
				$scope.status_ = true;
			} else {
				$scope.status_ = false;
			}
		} else $state.go('app.promo.index');
	}

	$scope.updatePromo = function() {
		var check = true;

		$scope.errorDescription = false;

		if (!$scope.promo.description) {
			$scope.errorDescription = true;
			check = false;
		}

		if (check) {
			var img = '';
			var old_img = '';
			var status;

			if ($scope.imgFile) {
				img = $scope.imgFile.base64;
				old_img = $scope.promo.img;
			} else {
				img = $scope.promo.img;
			}

			if ($scope.status_) {
				status = 1;
			} else {
				status = 0;
			}

			var data = {
				'id': $scope.promo.id,
				'img': img,
				'old_img': old_img,
	            'description': $scope.promo.description,
	            'status': status,
	            'content': $scope.promo.content
			}

			RequestService.save(data, 'promo').then(function(response) {
				// if ($scope.imgFile) {
				// 	RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
				// 		$state.go('app.promo.index');

				// 		$rootScope.modalHeader  = 'Pesan';
				// 		$rootScope.modalMessage = response.data.message;

				// 		$('#indexModal').modal('show');
				// 	});
				// } else {
					$state.go('app.promo.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// }
			});
		}
	}

	$scope.init();
});