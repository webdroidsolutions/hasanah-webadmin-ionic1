app.controller('surveyQuestionIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.surveys = [];

		$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
			var q = $q.defer();
			var data = {};

			if ($scope.status != 99) {
				data.status = $scope.status;
			}

			RequestService.getbystatus(data, 'survey').then(function(data) {
				console.log(data);
				q.resolve(data);
			});

			return q.promise;
		}).withOption('createdRow', createdRow);

		$scope.dtColumns = [
		DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
		DTColumnBuilder.newColumn('question').withTitle('Pertanyaan'),
		DTColumnBuilder.newColumn('question_type').withTitle('Tipe'),
		DTColumnBuilder.newColumn(null).withTitle('Status').renderWith(statusHtml),
		DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
		];

		$scope.dtInstance = {};

		function createdRow(row, data, dataIndex) {
			$compile(angular.element(row).contents())($scope);
		}

		function numberHtml(data, type, full, meta) {
			return meta.row + 1;
		}

		function statusHtml(data, type, full, meta) {
			var status;

			if (data.status) {
				status = '<span class="status-active">Aktif</span>';
			} else {
				status = '<span class="status-nonactive">Non-Aktif</span>';
			}

			return status;
		}

		// function imageHtml(data, type, full, meta) {
		//        return '<img src="' + data.img + '" width="55px">';
		// }

		// function timeHtml(data, type, full, meta) {
		// 	var date = new Date(data.time);

		// 	return TimeService.days[date.getDay()] + ', ' 
		// 		+ ('0' + date.getDate()).slice(-2) + ' '
		// 		+ TimeService.months[date.getMonth()] + ' '
		// 		+ date.getFullYear() + ' '
		// 		+ ('0' + date.getHours()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2);
		// }

		function actionHtml(data, type, full, meta) {
			$scope.surveys[meta.row] = data;

			return '\
			<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.survey.question.edit\', surveys[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
			<button class="btn btn-danger btn-xs btn-block" ng-click="destroySurvey(surveys[' + meta.row + '].id)"><i class="fa fa-trash-o"></i> Delete</button>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			survey: objectData
		});
	}

	$scope.destroySurvey = function(id) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'survey').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('surveyQuestionCreateCtrl', function($scope, $state, $rootScope, RequestService) {
	$scope.init = function() {
		$scope.errorQuestion = false;
		$scope.errorType = false;
		$scope.errorDefault = false;
		$scope.errorMax = false;

		$scope.status = true;
	}

	$scope.addOption = function() {
		var number = $('#divOption .form-group').length;

		$('#divOption').append('\
			<div class="form-group has-feedback" id="groupOption'+number+'" data-number="'+number+'">\
			<div class="col-sm-offset-2 col-sm-4" id="inputValue'+number+'">\
			<input type="text" id="value'+number+'" name="options['+number+'][value]" class="form-control" placeholder="Input Value">\
			</div>\
			<div class="col-sm-4" id="inputText'+number+'">\
			<input type="text" id="text'+number+'" name="options['+number+'][text]" class="form-control" placeholder="Input Text">\
			</div>\
			<div class="col-sm-2">\
			<button class="btn btn-flat btn-danger" id="btn-remove" type="button">\
			<i class="fa fa-times"></i>\
			</button>\
			</div>\
			</div>\
			');

		$('#divOption').on('click', '#btn-remove', function(){
			$(this).parent().parent().remove();
		});
	}

	$scope.insertSurvey = function() {
		var check = true;

		$scope.errorQuestion = false;
		$scope.errorImg = false;
		$scope.errorType = false;
		// $scope.errorDefault = false;
		// $scope.errorMax = false;

		var number = $('#divOption .form-group').length;

		// for (var ii = 0; ii < number; ii++) {
		// 	$('#inputValue'+ii).removeClass('has-error');
		// 	$('#inputText'+ii).removeClass('has-error');
		// }

		if (!$scope.question) {
			$scope.errorQuestion = true;
			check = false;
		}

		if (!$scope.question_type) {
			$scope.errorType = true;
			check = false;
		}

		// for (var ii = 0; ii < number; ii++) {
		// 	if (!$('#value'+ii).val()) {
		// 		$('#inputValue'+ii).addClass('has-error');

		// 		check = false;
		// 	}

		// 	if (!$('#text'+ii).val()) {
		// 		$('#inputText'+ii).addClass('has-error');

		// 		check = false;
		// 	}
		// }

		// if (!$scope.default) {
		// 	$scope.errorDefault = true;
		// 	check = false;
		// }

		// if (!$scope.max) {
		// 	$scope.errorMax = true;
		// 	check = false;
		// }

		if (check) {
			var options = [];
			var status;

			for (var ii = 0; ii < number; ii++) {
				if ($('#value'+ii).val() && $('#text'+ii).val()) {
					options.push({ 'value' : $('#value'+ii).val(), 'text' : $('#text'+ii).val() });
				}
			}

			if ($scope.status) {
				status = 1;
			} else {
				status = 0;
			}

			var data = {
				'question': $scope.question,
				'question_type': $scope.question_type,
				'options': options,
				'default': $scope.default,
				'max': $scope.max,
				'status': status
			}

			RequestService.save(data, 'survey').then(function(response) {
				$state.go('app.survey.question.index');

				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = response.data.message;

				$('#indexModal').modal('show');
			});
		}
	}

	$scope.init();
});

app.controller('surveyQuestionEditCtrl', function($scope, $stateParams, $state, $rootScope, RequestService) {
	$scope.init = function() {
		if ($stateParams.survey) { 
			$scope.survey = $stateParams.survey;

			$scope.errorQuestion = false;
			$scope.errorType = false;
			$scope.errorDefault = false;
			$scope.errorMax = false;

			$('#divOption, #divOld').on('click', '#btn-remove', function(){
				$(this).parent().parent().remove();
			});

			if ($scope.survey.status) {
				$scope.status = true;
			} else {
				$scope.status = false;
			}
		} else $state.go('app.survey.index');
	}

	$scope.addOption = function() {
		var number = $('#divOption .form-group').length;

		$('#divOption').append('\
			<div class="form-group has-feedback" id="groupOption'+number+'" data-number="'+number+'">\
			<div class="col-sm-offset-2 col-sm-4" id="inputValue'+number+'">\
			<input type="text" id="value'+number+'" name="options['+number+'][value]" class="form-control" placeholder="Input Value">\
			</div>\
			<div class="col-sm-4" id="inputText'+number+'">\
			<input type="text" id="text'+number+'" name="options['+number+'][text]" class="form-control" placeholder="Input Text">\
			</div>\
			<div class="col-sm-2">\
			<button class="btn btn-flat btn-danger" id="btn-remove" type="button">\
			<i class="fa fa-times"></i>\
			</button>\
			</div>\
			</div>\
			');
	}

	$scope.updateSurvey = function() {
		var check = true;

		$scope.errorQuestion = false;
		$scope.errorType = false;
		$scope.errorDefault = false;
		$scope.errorMax = false;

		var number = $('#divOption .form-group').length;

		if (!$scope.survey.question) {
			$scope.errorQuestion = true;
			check = false;
		}

		if (!$scope.survey.question_type) {
			$scope.errorType = true;
			check = false;
		}

		// if (!$scope.survey.default) {
		// 	$scope.errorDefault = true;
		// 	check = false;
		// }

		// if (!$scope.survey.max) {
		// 	$scope.errorMax = true;
		// 	check = false;
		// }

		if (check) {
			var options = [];
			var status;

			for (var ii = 0; ii < number; ii++) {
				if ($('#value'+ii).val() && $('#text'+ii).val()) {
					options.push({ 'value' : $('#value'+ii).val(), 'text' : $('#text'+ii).val() });
				}
			}

			for (var ii = 0; ii < $scope.survey.options.length; ii++) {
				if ($('#valueOld'+ii).val() && $('#textOld'+ii).val()) {
					options.push({ 'value' : $('#valueOld'+ii).val(), 'text' : $('#textOld'+ii).val() });
				}
			}

			status = 0;
			if ($scope.status) {
				status = 1;
			} 

			var data = {
				'id': $scope.survey.id,
				'question': $scope.survey.question,
				'question_type': $scope.survey.question_type,
				'options': options,
				'default': $scope.survey.default,
				'max': $scope.survey.max,
				'status': status
			}

			RequestService.save(data, 'survey').then(function(response) {
				$state.go('app.survey.question.index');

				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = response.data.message;

				$('#indexModal').modal('show');
			});
		}
	}

	$scope.init();
});

app.controller('surveyAnswerIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.answers = [];

		$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
			var q = $q.defer();

			RequestService.getall('surveyresult').then(function(data) {
				q.resolve(data);
			});

			return q.promise;
		}).withOption('createdRow', createdRow);

		$scope.dtColumns = [
		DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
		DTColumnBuilder.newColumn('device').withTitle('Model'),
		DTColumnBuilder.newColumn('answer').withTitle('Jawaban'),
		DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
		];

		$scope.dtInstance = {};

		function createdRow(row, data, dataIndex) {
			$compile(angular.element(row).contents())($scope);
		}

		function numberHtml(data, type, full, meta) {
			return meta.row + 1;
		}

		// function imageHtml(data, type, full, meta) {
		//        return '<img src="' + data.img + '" width="55px">';
		// }

		// function timeHtml(data, type, full, meta) {
		// 	var date = new Date(data.time);

		// 	return TimeService.days[date.getDay()] + ', ' 
		// 		+ ('0' + date.getDate()).slice(-2) + ' '
		// 		+ TimeService.months[date.getMonth()] + ' '
		// 		+ date.getFullYear() + ' '
		// 		+ ('0' + date.getHours()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2);
		// }

		function actionHtml(data, type, full, meta) {
			$scope.answers[meta.row] = data;

			//<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.survey.question.edit\', answers[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
			return '\
			<button class="btn btn-danger btn-xs btn-block" ng-click="destroyAnswer(answers[' + meta.row + '].id)"><i class="fa fa-trash-o"></i> Delete</button>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			survey: objectData
		});
	}

	$scope.destroyAnswer = function(id) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'surveyresult').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});