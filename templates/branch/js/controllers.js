app.controller('branchCtrl', function($scope) {
	$scope.init = function() {

	}

	$scope.init();
});

// app.controller('branchIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
// 	$scope.init = function() {
// 		$scope.branchs = [];

// 	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
// 	        var q = $q.defer();
// 	        var data = {};

// 	        if ($scope.status != 99) {
// 		        data.status = $scope.status;
// 	        }

// 	        RequestService.getbystatus(data, 'branch').then(function(data) {
// 				q.resolve(data);
// 			});

// 	        return q.promise;
// 	    }).withOption('createdRow', createdRow);

// 	    $scope.dtColumns = [
// 	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
// 	        DTColumnBuilder.newColumn(null).withTitle('Nama').renderWith(detailHtml),
// 	        DTColumnBuilder.newColumn(null).withTitle('Gambar').notSortable().renderWith(imageHtml),
// 	        DTColumnBuilder.newColumn('address').withTitle('Alamat'),
// 	        DTColumnBuilder.newColumn(null).withTitle('Status').renderWith(statusHtml),
// 	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
// 	    ];

// 	    $scope.dtInstance = {};

// 	    function createdRow(row, data, dataIndex) {
// 	    	$compile(angular.element(row).contents())($scope);
// 	    }

// 	    function numberHtml(data, type, full, meta) {
// 	    	return meta.row + 1;
// 	    }

// 	    function detailHtml(data, type, full, meta) {
// 	        return '<a href="" ng-click="navigateTo(\'app.branch.detail\', branchs[' + meta.row + '])">' + data.name + '</a>';
// 		}

// 		function statusHtml(data, type, full, meta) {
// 	    	var status;

// 	    	if (data.status) {
// 	    		status = '<span class="status-active">Aktif</span>';
// 	    	} else {
// 	    		status = '<span class="status-nonactive">Non-Aktif</span>';
// 	    	}

// 	        return status;
// 		}

// 	    function imageHtml(data, type, full, meta) {
// 	        return '<img src="data:image;base64,' + data.img + '" width="55px">';
// 		}

// 		// function timeHtml(data, type, full, meta) {
// 		// 	var date = new Date(data.time);

// 		// 	return TimeService.days[date.getDay()] + ', ' 
// 		// 		+ ('0' + date.getDate()).slice(-2) + ' '
// 		// 		+ TimeService.months[date.getMonth()] + ' '
// 		// 		+ date.getFullYear() + ' '
// 		// 		+ ('0' + date.getHours()).slice(-2) + ':'
// 		// 		+ ('0' + date.getMinutes()).slice(-2) + ':'
// 		// 		+ ('0' + date.getMinutes()).slice(-2);
// 		// }

// 		function actionHtml(data, type, full, meta) {
// 			$scope.branchs[meta.row] = data;

// 			return '\
// 				<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.branch.edit\', branchs[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
// 				<button class="btn btn-danger btn-xs btn-block" ng-click="destroyBranch(branchs[' + meta.row + '].id)"><i class="fa fa-trash-o"></i> Delete</button>\
// 			';
// 		}
// 	}

// 	$scope.reloadData = function() {
// 		$scope.dtInstance.reloadData();
// 	}

// 	$scope.navigateTo = function(targetPage, objectData) {
// 		$state.go(targetPage, {
// 			branch: objectData
// 		});
// 	}

// 	$scope.destroyBranch = function(id) {
// 		$rootScope.confirmHeader  = 'Konfirmasi';
// 		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
// 		$rootScope.confirmButton  = 'Hapus';

// 		$rootScope.modalAction = function() {
// 			var data = { 'id' : id };

// 			$('#indexConfirm').modal('hide');
			
// 			RequestService.delete(data, 'branch').then(function(response) {
// 				$scope.reloadData();
// 			});
// 		}

// 		$('#indexConfirm').modal('show');
// 	}

// 	$scope.init();
// });

app.controller('branchIndexCtrl', function($scope, $rootScope, $state, RequestService) {
	$scope.branchs = [];
	$scope.pages = [];
	$scope.last;
	$scope.limit = 10;
	$scope.currentPage = 1;
	$scope.baseUrl = RequestService.url.replace('adm/', '');
	var data = {};

	$scope.init = function() {
        // if ($scope.status != 99) {
	       //  data.status = $scope.status;
        // }

		RequestService.getbypaging(data, 'branch', $scope.currentPage, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.branchs.push(data[ii]);
			};
		});

		RequestService.gettotal(data, 'branch').then(function(data) {
			for (var ii = 1; ii <= Math.ceil(data / $scope.limit); ii++) {
				$scope.pages.push(ii);
			};

			$scope.last = $scope.pages.length;
		});
	};

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			branch: objectData
		});
	}

	$scope.gotoPage = function(page) {
		$scope.branchs = [];
		$scope.currentPage = page;

		RequestService.getbypaging(data, 'branch', page, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.branchs.push(data[ii]);
			};
		});
	}

	$scope.reloadData = function() {
		$scope.branchs = [];
		$scope.pages = [];

		if ($scope.status != 99) {
	        data.status = $scope.status;
        } else {
        	data = {};
        }

		RequestService.getbypaging(data, 'branch', $scope.currentPage, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.branchs.push(data[ii]);
			};
		});

		RequestService.gettotal(data, 'branch').then(function(data) {
			for (var ii = 1; ii <= Math.ceil(data / $scope.limit); ii++) {
				$scope.pages.push(ii);
			};

			$scope.last = $scope.pages.length;
		});
	}

	$scope.destroyBranch = function(id, old_img) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id, 'old_img' : old_img };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'branch').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('branchDetailCtrl', function($scope, $stateParams, $state, RequestService) {
	$scope.baseUrl = RequestService.url.replace('adm/', '');
	$scope.init = function() {
		if ($stateParams.branch) { 
			$scope.branch = $stateParams.branch;

			if ($scope.branch.status) {
				$scope.branch.status_ = 'Aktif';
			} else {
				$scope.branch.status_ = 'Non-Aktif';
			}
		} else $state.go('app.branch.index');
	}

	$scope.init();
});

app.controller('branchCreateCtrl', function($scope, $state, $rootScope, RequestService) {
	$scope.init = function() {
		$scope.errorName = false;
		// $scope.errorImg = false;
		$scope.errorAddress = false;
		$scope.errorPostcode = false;
		$scope.errorProvince = false;
		$scope.errorPhone = false;
		$scope.errorLocation = false;

		$scope.status = true;
	}

	$scope.insertBranch = function() {
		var check = true;

		$scope.errorName = false;
		// $scope.errorImg = false;
		$scope.errorAddress = false;
		$scope.errorPostcode = false;
		$scope.errorProvince = false;
		$scope.errorPhone = false;
		$scope.errorLocation = false;

		if (!$scope.name) {
			$scope.errorName = true;
			check = false;
		}

		// if (!$scope.imgFile) {
		// 	$scope.errorImg = true;
		// 	check = false;
		// }

		if (!$scope.address) {
			$scope.errorAddress = true;
			check = false;
		}

		if (!$scope.postcode) {
			$scope.errorPostcode = true;
			check = false;
		}

		if (!$scope.province) {
			$scope.errorProvince = true;
			check = false;
		}

		if (!$scope.phone) {
			$scope.errorPhone = true;
			check = false;
		}

		if (!$scope.location) {
			$scope.errorLocation = true;
			check = false;
		}

		if (check) {
			var date = new Date();
			var status;
			var imgFile;

			if ($scope.status) {
				status = 1;
			} else {
				status = 0;
			}

			if ($scope.imgFile) {
				imgFile = $scope.imgFile.base64
			} else {
				imgFile = "iVBORw0KGgoAAAANSUhEUgAABQEAAALQCAIAAACv3SE/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAALr1JREFUeNrs3Tt0E2fC+GFbli8CvsYUOAdKU8KBJm7g+9gOp3QgW0ABATpsSjDZgiLhUoLTkQAFFJsL3X9Nt5wDjdOEA6VdwgkUuFnwDV/+70gJa3QZzciSLNvPczh7skGWpZmRMr+5vG/7f4b/tw0AAAC2gIxFAAAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACggS0CAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAABoYAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAABoYAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAABoYAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAABoYAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAABoYAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAABoYAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAABoYAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAABoYAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAABoYAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAABoYAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAABoYAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAABoYAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAANbBEAAACggQEAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAA0MAAAACggQEAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAA0MAAAACggQEAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAA0MAAAACggQEAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAA0MAAAACggQEAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAA0MAAAACggQEAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAA0MAAAACwiWUtAqD1ZXr72nf21eWpliafVfqr9tyOzJ7+dXybK29fL0+/rn0p7e5v37ajdV5PfVZ9+jfV5JfdsfdA7Ut45t3yq6mqD6vjlrn8cmpl9p2vlNYUVnTz107YgDv6DxZvmdOvP0yMWyOABgZYv6+qgcGuwVN1CIBXUzM3zlT8LfsPd5+4tI5vc2H83sL43Zp/vHNgsPPIsYaH+vSb5ek/PtbU0tTvDc2qjr0Hu4fO13Et13/j3He45sU+/+B6wgbuOXG5vXfXGl/q4m+P5iavNXmrLhzAyvR+1t7bV1g7K7P/iTkUtTXTt/vL4eznRwv/d2nq2fyvY0k2jPp8xPoPln67hteggQENDLAZfJh4FLezvrt/Q7+7+YfRfnPX0HB7bnsD99d7d3XkY6yjv3D+81QhjBefPwn7zeF/67zKHv+8/Gqy5+zV5G8qrMeuwdNrOZqQdrGHN959YjTVYl+ZfT//4FrCxbU8/XrmxtfdJ0ez+w6t4XV+HxZmM9O388jx7P7DldI92lomHoUl4Lx0buTm6i+f8MnKjdyavTXStAwG2GrcDwxsIUuTv8d9Ie7p3+hv8MPEeNh1Dn3V5N8bOqfzyLGes99uv/JT6M/23I66rrVnM1e+StUDXYOnmnlEI4RcqsUeHhken+p4QQjFudvfhI6tMYAfXG9aAEdnNYeGt135Z9gkYs5dh9LrPnFp25WfOgcGt/KXUnb/4dJttT23veuL076xATQwwFpthfMq4T2uSwZ/jOHQn9vqXcKFAkz1pnpOjtY3xasu9rkfLicP4No2xdCxi789qiWAm3Vda8i53MjN5BeHh9gLJRx+pJkrq7X2wyocrFnLOX8ANDDAVgngj+904eHYOr6A6CxWKOGLd9YyXlTxm5p+nepNFa6Ibua7Xpp8liRQF8bvrmVTnP91bGX6TfLHL7542rwA7u3Ljdyq4Qx8pvczV0QDoIEB6mxL7WSH7ElVSg0p4d5dueGbnUeO1/NNpTkV3HnkWHb/4Wa+5YV/VbkJOayUNV6THDbjVE1b9SXVUc+57+Jviq4U/3MPrvpeSrisANDAAIn3Ndd7mp8mW2jiAEgxuofO95wcrdezLb1IN+ZW94nRTG9f097s8vTrpalnjV4pi4kbOHRU01Kqc2Cw0hngxd8ezVz5+7uR/5u5cSb63yt/n39w/eOCWhi/t5WHiV6scGQnLBZf2gANYlxoYKtYfru1Gjh+ALDVu9oxQygXLmbO7N7bntuRn0e0lmubs58f7Zysz1Qri8+ffpxCJon23Paec981c6qkkHMxSynhSqla2ivTb5JMldTMtuzYfyjhBhZe//LEeNgewhbVeeR400bwbk0rs+9mb43kRm6tPoU+/+B63YdYB0ADAzQriqaeLb+civb7X02uarP/yezuz+zsi/63MSMY1+UEYCGi/kypfMNm9x/O7j+UKkTbovOxl5ZeTq79JS2/nEz7I2Hx9pwcnbt/rVmr+/fCfFGNWynRb3k1mU3SwFPNa+CyYzitzL6PSdywXZkouLBVzFz5Kjsw2J7bEc25/fzJ8ha7aAVAAwM0RAjOan01Vdt5zrK7/ksvniw+fxp3Muevv8qfYj0YwrLudyyHl1H3uYLDOwp/2n8d6/5yOFUJh8fP3rqw1lqoqQ3qeCK6+jKvfLlBHe/QDttqW4Jxg1dm/7PedTfpmyfZR/Xdh9a4eQFAAwNsHu3V7gutS4KG7Az7suFP8mcLjyyEZSMKpF5VX/qa5+5f65gY7zl7NWFmh1eS2d2/9hOhtYV919BwXU5Er6XSl6f/aHZZrff1/2GlF85t+v4BQAMDtJz4q1iTWBi/l6p+N/wSm3xWeitjjM6Bwfk1T9pUW9hHNwafHJ29dWFzrJ2EMd/MS2or3aK8ljt+M7197Tv7OvoPZvb0h09WkgunQ3L3nPsuPDIaD+ztH1UXVGZ3f3b/4fD84Qfz971vb/vrGvJosqvnT5Is6rJzgC2/nCpsbOFddH1xOryLsHzCMy9OPCq6JCH+x+PfbMfeg9H9FPnXX/Tj4XctTf5elw0+LKLoGNafS6k/rOvl6T9Wpl8vNevyCgANDFCLquEUXV+6hiaZu39tC05nEt7y/INrPWe/TbgnPb9+ExeHfffuk6Nzt7/ZBIt93S9yLnNAZOr3stfGdw2eSliSq37kdJkB2GbfJWng7MBg+MHVPxtScD58NksOB3QODIZfVLbbCz8e/je8+NB7oeHjSy83fLP0X86OXQgvOGx1qw8SFV5bduBo2A4/1mnMj8dUd3j9MTcjhN/SeeRYW35Q7oV/3a35aEjnkeNdR44XLaXwfzvy/ya8gLAMw4faCF7AxmJuJGADiE6u1uNeyqJTJSVd8a62iI2mfrlxZsvO5xl2fxOOvZRkKOOGyu47VMf5ivnkQ1q5EkMElj3VWXHXJDr3W/z4kFvxn98/+7nc+i0qwNCl2y7+2H3iUpINMjwmPDI/9fGOtMskOil9crT0Konw7nIjN2v+EgsvJmRzwrvxw8NyF+/UMFF2prcvWkpD5+OXUvjbnrPfhiD3EQA0MEA9RdOH3L5cdhbNdF95e6qMwFzDKLXzD643bczhFj5I8UvCR6ZqoUYI+/QNGoj7v1vR1FYc6zga5LnCGw8RGJoteSYt/Lv86FBVQy6s2dJgW5x4VPQkocnTbgPZfYdCeaZdJl2DpyvOmfz8aW0BHOI5m2A4tKLlHzI11VvO9H4Wyjn5j3SfuFRDZgNoYIA4y6+mFtZ8GW1H/8H4Byy+SHdFXwjgSqe/wu5j6L2wE1z4E/650em1jhafP1n7EYqmyZ27WsM5Pap/HO5fi9kMorOpJ0cTftjLXlVRuLg3rjn/VnwSeGX6TdEndPntH6UnZsOvi+6enXoWc/wiuro4zdnO7L7DlV5wYeS8Gpbwyuy70vt7o1Ho/3rxMVejhM0+RTb37ko78lz30LCPALBRuB8Y2DDCvmzIyKr7wXENvPdAW+wALkuTz5IPOxyNgFUSwH+Of7PvcMmTnGr7a86khX//vPkunG7cGNR1F129ee67tU/URPE2MP06/ubw7OdHt+3un7v9TdXbUz88/iU0c/GHKz+ZdqXPTjRA1L7DpV8apbkbPrldg6cKhbwwfjd/BOfd6ufJDgx2D50v09iDp5MPARXzTbXwcKzmoarm71/LXbxT+HqJ7vUt+TIJ33IhR0uPuEU38e49kPZSl8IMzx+nLM7uj8K+7Cc9PH/nwKAhsoANwXlgYCOJBl958bTmH08SaYvJ9uHCyyga7TbUb8/J0W1X/pm/cXF7hd307VEGXPwxN3Jz3a8K3srywx2dthzqLvTk/IPrMWeDozGiLt6puvFXurIg5kxsyLOiz12l063hkxu6MX8b/9eh2YpytDBVb3gXZTMvU22KtTIZOf0mfF2EX1cY1KD01HTaAw35hH4/O3ah7Dh8+dHaL5QdQCG7L93lyuFlz1z5KiyNj8cswnoJTx7eS/mPle80QAMDNCSD1zb8ctWb1pLsnob9y/lP7wGObjK8eCfhKDWFBssN38yPl7NJLsqt4Vbqukt+W3Jbfrxiu+wNWQsT47O3RmIyOMntwdGk2eU+iTGf3+xA8adv6cWTSqdb525/Mxdduf0u5l2UzcjMnr3pvq8efv/+yleFXxf+YebGmbkHV+uwhG98HfOJC++r7HxUVQdE+GTpTT1bPXj1J2/q17HyC2fz3u4BaGCA9RT2yeZibzus1sBVhpOpdC/iJzvQD66u3jUMe/M9Z79Ne/tcW+HS0Cs/bY4SS/guGprK8w/HUo1H1XPWjcENET5BM1e+il8X3Scuxd9BWvYUbnvvrrIZnOktM5T0wr8qzkscfzF2eLZK44enyrzChOFFS6YuH4H41x9N51vu85jZnSLgY44oRUcoyk2GpIEBDQzQwD3suR8u15hqCa4GjD+dGI09s2ovNgRw6Y2LyaUdMndjH7+oxwRXVQ5P3P4m+W+JxstNP9gvidb17LvZWxfmH34f85jOI8diRskKmVe2ossexsqWfIKiAaJSToob0jFk+baLP2678s+qcwIlWAI1DnxV4/5cvtvD9rz9xr96zn5b9pqUVMfpliZ/j/nbtMMHArQUY2IBG1Ko0PkH12uIz7AXWHXglvC3XYOnK+0Br77IMLO7v6seo6EW3siGHk4myb3WS1O/NyO9bl/OjdxKuLsfXnbInvk1DzlO+Y/S459DSoXQrXSGMKRa19vXZS/czf/4L6XbVf4wVvFsZKVHkSo9Z8kXwo7orOn+Q2knHEqytdc88FWK9N3dH957dC90XSffXpp61oQXD7BenAcGNuzu9cR4paFZ4pXeNxgfukW7hqtPAnd/OVzDJdCVMnjjng1OODVobXOippV2Gq3OI8dMbdrQ1REzilJb/sbsSss/Ghmr5Kx+4TDWJ1W890BRAYafqnrJ8V+D2P0UzW1b7wCO3vjLxo79HhZCdMr64o9hA65vALfCuwPQwADlzd2/lur+zz/3mPsPVB3ctdKIOKsvkw573vWdDShJnLdqA1dPiGh03OdNun4y7fGR7hOjNYz3S0KFe/hjrovuHhqudGN22YsjOj7d3mo4CRxd81xhCPeV2feLL55GU3+nGWKtVOOuegjfPNvz6V727Pryq6mwqOd++EdDE9dZYkADA6xfBqe5//Ojri+qz4tTuhsd7RyvqrhKo+Zsuf+Q7O5PMiB2wmtT67ZhpBk/3I3BzTgw8fjnShkcjXRV4SKIsrfUZvcdWt3MRTf5RzfiVr6tIPxg4dxp6V8t/vYopOP7i1+Eb5XSOZNaRNfg6dzwzdITv/m5ju/NXPn7zI0zYaGt8XiTxAU0MEDrKtz/mXaY6LDTXHVA4LATXHSSeWnVMDDRbYQNuH5ywwnLIWZYo/8uuqlnzb/bOT+zS9INI5R8kjfCGjO40vzeXRWOKEVDEJc7pf/x8unSaYHjR6LKjdwsPX2aH8j673P3r61OxxYcM7zzyPGuwVOl/z7U7/srX0XzHv81DJh5vwA0MLCZRdf+PbiWsty2JzmLWzQJ8Oo7DFPNtJlQK0yxmzaAyxZFSca8Dzm6DhvG9OtU44dnPz+6RQboXkcLv5a/Vbu9d1ely9HjL4cuvQ4/poG7Bk+Xbq4r029mb10oHUS6EZ/xNe2x9fZ1D50v/fezYxeafJEFgAYGWH+Lz5/Ez8JSKjRw1fM8Ybd49dOuniwk0/vZFl/m+SF57lQP4CgwRtbr0sqlyWcL4/eSP75raNgcp40+MFHpHv72nX2VVmLpZe2FqzCiyzE+vQ5/8bdHMRtb2WMcRdN9t+xnvOwdHOH9lj12lmoq4DLrwrzZgAYGWPfcqlom0WWWaYZBas9t7xqsfldwNLnLX7vsq88UtW/VIZSiuViOHC8MyVN1NNqw6GZufJ38vtxGWBi/W+n627JbRc/JUQHQ6AMTaX+k7PBU2f2HSweUXvj3zzGbbtnbaMs3ZG9f8wdbjtfRf7DM5l3h/XaUO4mdfOiEqufAfUaADc38wMAGEIIzNzQ8c+Wr+NOJc/evbdvdn/w8XueRYyFxS6+BLH7a29/kRm5u1kFiMjv74m8dzOzeG/Z3w5+wW5z/50RzQeVH6LnbIjMez9+/1nHxTsKkCdtP98nRdbl4e4vI7Ex9/CiasnuoeB6y7P5DRYeilqaexRxw6dhT5tRopdGbO1pvuqyyG3Cl91s0TtifD57+o24rcY/LJQANDNDo/b/c9hCis7cuxLdoeMC2xLUTdH05XLV2CjO7FJ1xWt9zm/X8z8DnR5OM6pzQyuz7pRdPFp8/bdo0SMleVTRw2raLPyZdJvsOdR45Hj+0EmW6qLev6hGltgrnM6PVNBP30V6cGC8azDk/sl3RaFi/xDZkmfZeflv+BVcao6txMx7VUefAYL3mLQfYnP/BsgiADfOFtbu/+8vhJLWTfDTgUDtJBlANxVs06szK7H+skfLyJ41b7VLJwqSpyR/fPXTeyLqphMWVu3in6lBz0X345Q5RrUy/iT+uVHpIoijzapuAuuxarvQiW/NbsWSx7EhylweABgbYGLKfH626e5d2mOieE5dreCUbbgzn5ghZkt13qPvEpe03/l/Pue+yrXRBado7xnvOXnXTY6rPUVj73UPncyM3K6330JZlRzZuqzD48yef6+nX8fd1Vx0buewlJB39B4rGo+4cGKz0Itsqn8ReL0UDZRWGaq8U8LZngD/3Jy0CYGPpGjy1Mv06fo+5MEx0zI7sJ/uFvbtCV9cwucjS1LOwA22NVPwPzL5D4U9r3Rj861gm8R3jhcvvZ26csSoTfCpPf+yu8KEIfwpXxS+/fb009Xt77n+iodQGBiu1WXhwkivPFyceVZqUOzxD1ZPAq8d1/6Tez323MH4v/Hh4kV1/O17HWwPqa/HF09K3Hx1yGhoOSy8Ufnb/4dK7plcz5jmABgY27A730PDSy8mqV0527OlPuDsbujrsAae9xXfx+VMNnOQQQ/eJS6GR5h5cXfeT54Vbu3MjtxLeLRldfj80PP9wzHqMX0rhE1R6BOGvT9+pqs8w98PlJGPOhQ/pyvSbsiFdiMD4Hw8f8LI/Hl5/z9lvy3b1Wu6qDeVf3+W89Pxp2UMAnUeOFd0p3Za/MnyjXM4NsA7/5bIIgI2XVdEJultVr+sLtZM8a3tOjqbfJX1iXSQv4dzwzVaYdii6tTtN04a6yLbeEMGtpuYh4kJnzj+4nvzgSKULChaTXWiQ8HBGCMiZG2eWX02W/lXyu8TrftI1vPeEy3lh/N7cg6tl/8pd7gAaGNjQGXyzalBF40gnmxIzfy4r3UAy0Q2Kae4vJfv50bDW1v2CzNASqVZc94lRF5HGB3D4oMWPyVz5B0dSXSdftnXD2kwyHnVb/kxy1VX/cVLr5ZdlgjP5pR+NGEF67v61+AH/wt/O/fCPhfG7KxXGu87s3muLBXAtNLBRFYaJDjuFsXuE0TDRCa997Ro8FXZbU12vG3bfW/buwYQWxu9VvRc609vXnp/TtaP/YNqJgkvXWlgdoXzWd3KpVFNJh3fac3K06rxcW1lYMvMPxz48/rnri9OlUxaVrd/QzDXcJd6xt8yQVAv//jnVqu+cfFb2vtkQkNG963/dmVxpE+0cGFyv+9sLRw3C1lh20w31Pn//WuFwQPjfspdDdw4cNekXgAYGNvJX2OdHu96+jk+4wjDRZe/3K9Vz4vLMja+Tp04I5rID1Wwy0V51fsd69QGCsBcegiTsUqc9R1q4lH39M/j2N7mLd1LcGFztgAthO8kvomgy7fzmEZ0yzfR+Fkosuhd39t3K9Oull1NLz58kPG1b5iM/cLQ0/NJuSNGFAM+fZAcGs/sPhZfXltux/GoymtR6Ynz1Zz8aI2D6jzLBX3KKdXbsQpml8TLpq0r14+HNztw4Ezq8Y/+hjt17C8s2OqYwMV50/G729uX2bdVvPQjvuvSUdfxczYWXV/ZlA2hggIZLOkz0g+vdJy5Vz7PeXd0nR0MdJX8BC7+O5c+Obt9qS/7PPe/HP2d6+0JORFOqJl4IrXBmNWRY8oMjhQMunS+nnENLIhqiOfyp97nSsKWVXopcwzXYbfkT12FVxq/N8JiEV4WscbC3Gn48fONVn00q2aGBj0e40i5A88MBG5f7gYGNn8FDw1VPRSa/BTS771DIuVQptcW7KCyBhfG7M1e+SnWTbQ03YDci1VIVVPfQeTcGr6PSD+bK9JtFQ9MBoIGBrSb5MNFLU4lOXKRNnVCACZ95EytMOzT3wz/ix+z5NGmOrfsotfMPx1KtuyRbGo35mO/IDgyWfvQsGQA0MLBlM7j6MNFzt79JeH1g2tSZrzZe6xax+PzJ7K2R5Iti3U8FF7aK5C84uor73HdWdPOVXmwf1tp6jU0FgAYGaIGvs/yoRfGPKZyrTBI8YW+7O82MwdFoQD9cthba/hqELOGDO/oPrPup4GirSLPuwmvuHhq2opv66e7tK70Q2r3ZAGhgYKuLRi2qditvKLQkwRMetvDrWKrfvjT5bP7BdWuhrTAL64unCR/cWXKBa/OFdbcwfi/54zuPHMvuP2xFN0d7bkfPue+KTwJPv9HAAGhggOhW3qpxUjVWQ7/N3rpQw/QtHybGU6XUJpb8CELHvpaIyYXxu8m7PdrSToxmevus6Ebr2Htg28U7pffnzz8cM10zALUxNxKw6TL4xOjy2z/i7/sNsRr2rbOfHy39q/mH36/l/FJIqczOvrLPvKUsT79e/O1RkuXQntseCmd95wr+c9Xfv9Zx8U57764kDy7cGLy+0ztt1ujt6D/YVhgEa//hsqtj8cVTw0EDUDPngYHNJhof69zVGoaJXpl9Pzt2oTSAO48cT3Xha3hmF0VHofI86WnVjj17W+EFh5qdvX05+fhYSW5BJ3UD9x/sGjwV/nQeOVY2gKMbzu9fs6AA0MAAqzK4d1du5Gb1WF01THT4h5krXy1NPvs0p3f0nBztHjrfc/bbVEM3fZgYDxm8xUeKTn6mrr1lLiqO7gN/mOI+8OgW9Ba4n3nrCCvIuXcANDBAuW+33f091QZ2/jhM9OJvj2ZunCnasc709oWQ/ng1b274ZqraCRk8e2ukFS7xXd9iSdTArTTjblhxYXtI/vjuE5dSzSZN7avm8S8CGAANDFBRwmGiZ658NVdyaWV2/+FcyUg8oXZSZXD05DfObOVRshLmSmZPazVk2B5SHbxIcu09a7H426PZsQvGwQKgPruIFgGwiXUPnV+Zfh1/UW7pXnXX4OmuwVPln/DEpbb8qcLkryEacHhivOuL0x37DhfN77LpLU0+6+g/sBFf+dztb3IX7yRcX+29u7pPjoYf8YmrQ+6u+rSGD+/y9B9FdygAgAYGiM3gBMNE/zdmcjtCzGT3HYp9wksdew/MpRmVZ3n6dXh8e24su/9wx/5DHf0Hi+IqvLwPE48W06T1ZrL8suWuGI9W2Q+Xc8M3Ez4+bDNdg6cXxu/6xK11yb+aWtjadxAAoIEB1qQwTPTMja+rXkVZuIU4yb2d2c+P5nr75m5/k+rKzPDgDxPjhXPIIbY/XgC8iU9zJRxIrDUvcA3rZWH8XqUrAkqFRy5N/e6kJQC0OPcDA1sgg5MNE70y/bq997Okddd/YNuVn1INFl1UfSGWCn+soJa9yTO6jv3F0+SP7znrxmAA0MAArfBll2yY6NlbI8knNIrOMA/fDE8re2KOFCR52PKryZZ9C/P3r61Mv0mxSYzctN4BQAMDrL+VmeonG9NOD9uWvy5625WfugZPr7GEo6ujN9cUO9n9hxM+spVPhkdHRm5fTn5kJKzEDToMGABoYIDNUr+z7+cfXJ9PFrcfJsbTzmbUntveNXiq5hIOP9I5MLjt4p3k0bgxGnjgaJKHpbrYeF3UcGQEAGjdXRSLANjkATz9Zvb25VTTvS6M383s6Y8fHbpSCYc/IeqWnj9dmvx9efp1fPp27D2Y3X9oU86ZlNmddAEuTjxq/bfzYWI8vKPOI8d8oABAAwO0rpCj0f2cJUMuhf6MH4cp/FRm5GZtFyeH9ivk38rs++VXk+EXrZ77J7Ozr723L9P7WXvvrk285KvefV2wMv0mfvbm1jH/cCyzx3XOAKCBAVrVwvi90vlaC4NjRbO/3v4mrs1m383dv5YbubWWM7ThZ/9MppSnlDdBACc8fLCxJtQN20yu1iMjAECLcD8wsAmtzL6f++EfpX3VOTAYsrZwmW730HD8kyy/mpr74XIzX/bmGF86BHD280R3AoclXJgteeNsV9GRkeTjYwEAGhig4UJZzd74uvQK29Bm3ScufTyv23nkWEji+Kdamnw2/+B6876R92zsE4yZ3r7cyM2EARyEntyIW1eTj4wAABoYoKLF3x7N3rpQNBhVaLNtF38sbbOQxFWHYv4wMT7/8HsLNl57bkfX4OncxTvJb5ddGL+XaqCyhC+jCW+2yUdGAID6cj8wsHmEWP3w+Oeif9mx90DP2auVbuvtPjG6/PaP+BgLz9mxpz/56c2WStNG/4rM7v6uvx1PO7T14m+PGnEncGZPf3OmGv4wMd7e29c1eGq91mym97NkD+uLH5wcADQwwIa0Mvt+9tZIacp2DZ6OD5VQbrmRWzNXvoofJnru/rWe8I3Z4AyOfw21BWoD6quvfWdfR//B/CDJB2sYMywE8Fqugs7s3tsKm1w0gdbOvvU6MhIKPNHDdva1aWAA0MDAJhPdAHzrQlFAtud2dJ8cTTJFbT6Db5Y+Q2kGd7193dBTf6unUKpLrCZ5WMfeA11tp+OeJz+ZU2GRrj2q1xjAhfVV8b30H2zOeeCPm0QTjoxUWAg7km0Dny21PfMVAQAaGNg8Pjz+Zf7hWPGu/+7+3LmrySfgDY/v/nK4apstjN8Nvd19YnQtEyY1Tcfeg4ke1n+gaXPelr1YPeWbinupzR9UbL0yOH45rN6wfUUAQPF/Hy0CYINamX0//+B6aQAXJkBKHsAFIWM6jxyv+rDF50+iQad/e9T6yyc70EI3MC+/mpq5cWaNARy9qX2HY3v+YPPfWsjgJm8Pmd6+hHFbdcg3ANDAABtD/vrnkdLZZbuHhldPgJRK99D5JGmxPP06ZM/Mlb9/ePzL0tR/LzQN/9w6M8d27G3e2d14YZksjN8LAVyXUaDjoy6s96rzXW2CDM4mfo/tvbsSnjEGgK3DtdDAxhN6Y/7XsdIbgHMjN9d48Wfu3NWZG18nGZsqlHDRKehQX90nWqI3onuhh4ZboX4/PP45/KnXWF9dg6ernt4Pjyk9MtKcDG7ORdGZ3r4kFyx8FLaEmRtnfGkAwH//Y2oRABvL/MPvQ28UZVXH3gPbrvy09rsfQ2KFiKqhOXtOjnafuNQii6j7y+H1vRF08cXT+QfX31/8YmH8br0COJqEKcGAZLWtwXpl8IfHvzT6t/Sc+y7VZQ5huYWN0/cGAHzkPDCwYaxMv5m9fbmGCZBS6Txy7MPEePILdzO9fSFLWmTwoeSjYTdi7SxN/b40+Wzx+ZO6T/JUmOQ54YMLG0Mj5h+uav7hWH7UtEuNWrk1Hd3Ifn60J5/ovkMAQAMDG0Z0arHk9G+Dki+UxuytC4kCeHd/NP5WawwTnXY07LUIpRfWxfLL6H9D+hb+oUG/q/PI8e6h86l+JGRwZk9/6QbTBB8mxpdeTtZ9q1jjoZYog3M71mWBAECraf/P8P9aCkCLCwGwPP267L9v39nXiN+YcJrZ0CTt23bU65euvH1d9m0mb+AaXkxm995Kk81Glftqso6vsJYAHhjsGhqurSc/PP6ljhdjp/uPa25HSNaiYcmWpp4lPLZS+mzdJ0fj5wQunb159YBtH1eoDAYADQwADVF0lX7NDQwA1JExsQCgIRbG79ZrUigAQAMDQKsLARwyeP7h960zdzQAbHHGxAKAxvrw+OfFifGOvQctCgBYd84DA0DDrcy+W3z+xHIAAA0MAAAAGhgAAAA0MAAAAGhgAAAA0MAAAABoYIsAAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAAANDAAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAAANDAAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAAANDAAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAAANDAAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAAANDAAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAAANDAAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAAANDAAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAAANDAAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAAANDAAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAAANDAAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAAANDAAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAAANDAAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACABgYAAEADWwQAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAAGhgAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAAGhgAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAAGhgAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAAGhgAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAAGhgAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAAGhgAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAAGhgAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAAGhgAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAAGhgAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAAGhgAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAAGhgAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAAGhgAAAANDAAAABoYAAAANDAAAAAoIEBAABAAwMAAIAGBgAAAA0MAAAAGhgAAAA0MAAAABrYIgAAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACggQEAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACggQEAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACggQEAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACggQEAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAAaGAAAADQwAAAAKCBAQAAQAMDAACggQEAAEADAwAAgAYGAAAADQwAAAAaGAAAADQwAAAANND/F2AA80LufdcXiK4AAAAASUVORK5CYII=";
			}

			var data = {
				'name': $scope.name,
				'img': imgFile,
	            'address': $scope.address,
	            'postcode': $scope.postcode,
	            'province': $scope.province,
	            'phone': $scope.phone,
	            'location': $scope.location,
	            'status': status
			}

			RequestService.save(data, 'branch').then(function(response) {
				// RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
					$state.go('app.branch.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// });
			});
		}
	}

	$scope.init();
});

app.controller('branchEditCtrl', function($scope, $stateParams, $state, $rootScope, RequestService) {
	$scope.init = function() {
		if ($stateParams.branch) { 
			$scope.branch = $stateParams.branch;

			$scope.errorName = false;
			$scope.errorAddress = false;
			$scope.errorPostcode = false;
			$scope.errorProvince = false;
			$scope.errorPhone = false;
			$scope.errorLocation = false;

			if ($scope.branch.status) {
				$scope.status_ = true;
			} else {
				$scope.status_ = false;
			}
		} else $state.go('app.branch.index');
	}

	$scope.updateBranch = function() {
		var check = true;

		$scope.errorName = false;
		$scope.errorAddress = false;
		$scope.errorPostcode = false;
		$scope.errorProvince = false;
		$scope.errorPhone = false;
		$scope.errorLocation = false;

		if (!$scope.branch.name) {
			$scope.errorName = true;
			check = false;
		}

		if (!$scope.branch.address) {
			$scope.errorAddress = true;
			check = false;
		}

		if (!$scope.branch.postcode) {
			$scope.errorPostcode = true;
			check = false;
		}

		if (!$scope.branch.province) {
			$scope.errorProvince = true;
			check = false;
		}

		if (!$scope.branch.phone) {
			$scope.errorPhone = true;
			check = false;
		}

		if (!$scope.branch.location) {
			$scope.errorLocation = true;
			check = false;
		}

		if (check) {
			var img = '';
			var old_img = '';
			var status;

			if ($scope.imgFile) {
				img = $scope.imgFile.base64;
				old_img = $scope.branch.img;
			} else {
				img = $scope.branch.img;
			}

			if ($scope.status_) {
				status = 1;
			} else {
				status = 0;
			}

			var data = {
				'id': $scope.branch.id,
				'name': $scope.branch.name,
				'img': img,
				'old_img': old_img,
	            'address': $scope.branch.address,
	            'postcode': $scope.branch.postcode,
	            'province': $scope.branch.province,
	            'phone': $scope.branch.phone,
	            'location': $scope.branch.location,
	            'status': status
			}

			RequestService.save(data, 'branch').then(function(response) {
				// if ($scope.imgFile) {
				// 	RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
				// 		$state.go('app.branch.index');

				// 		$rootScope.modalHeader  = 'Pesan';
				// 		$rootScope.modalMessage = response.data.message;

				// 		$('#indexModal').modal('show');
				// 	});
				// } else {
					$state.go('app.branch.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// }
			});
		}
	}

	$scope.init();
});