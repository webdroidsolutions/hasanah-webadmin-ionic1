app.controller('userCtrl', function($scope) {
	$scope.init = function() {

	}

	$scope.init();
});

// app.controller('userIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
// 	$scope.init = function() {
// 		$scope.users = [];
// 		$scope.getArray = [];

// 	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
// 	        var q = $q.defer();

// 	        RequestService.getall('userdevice').then(function(data) {
// 	        	for (var ii = 0; ii < data.length; ii++) {
// 	        		$scope.getArray.push({
// 	        			a: ii+1, 
// 	        			b: data[ii].name,
// 	        			c: data[ii].device,
// 	        			d: data[ii].phone,
// 	        			e: data[ii].email,
// 	        			f: data[ii].address
// 	        		});
// 	        	}

// 				q.resolve(data);
// 			});

// 	        return q.promise;
// 	    }).withOption('createdRow', createdRow);

// 	    $scope.dtColumns = [
// 	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
// 	        DTColumnBuilder.newColumn('name').withTitle('Nama'),
// 	        DTColumnBuilder.newColumn('device').withTitle('Model'),
// 	        DTColumnBuilder.newColumn('phone').withTitle('Nomor Telepon'),
// 	        DTColumnBuilder.newColumn('email').withTitle('Email'),
// 	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
// 	    ];

// 	    $scope.dtInstance = {};

// 	    function createdRow(row, data, dataIndex) {
// 	    	$compile(angular.element(row).contents())($scope);
// 	    }

// 	    function numberHtml(data, type, full, meta) {
// 	    	return meta.row + 1;
// 	    }

// 		// function imageHtml(data, type, full, meta) {
// 		//        return '<img src="' + data.img + '" width="55px">';
// 		// }

// 		// function timeHtml(data, type, full, meta) {
// 		// 	var date = new Date(data.time);

// 		// 	return TimeService.days[date.getDay()] + ', ' 
// 		// 		+ ('0' + date.getDate()).slice(-2) + ' '
// 		// 		+ TimeService.months[date.getMonth()] + ' '
// 		// 		+ date.getFullYear() + ' '
// 		// 		+ ('0' + date.getHours()).slice(-2) + ':'
// 		// 		+ ('0' + date.getMinutes()).slice(-2) + ':'
// 		// 		+ ('0' + date.getMinutes()).slice(-2);
// 		// }

// 		function actionHtml(data, type, full, meta) {
// 			$scope.users[meta.row] = data;

// 			return '\
// 				<button class="btn btn-danger btn-xs btn-block" ng-click="destroyUser(users[' + meta.row + '].id)"><i class="fa fa-trash-o"></i> Delete</button>\
// 			';
// 		}
// 	}

// 	$scope.reloadData = function() {
// 		$scope.dtInstance.reloadData();
// 	}

// 	$scope.navigateTo = function(targetPage, objectData) {
// 		$state.go(targetPage, {
// 			user: objectData
// 		});
// 	}

// 	$scope.destroyUser = function(id) {
// 		$rootScope.confirmHeader  = 'Konfirmasi';
// 		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
// 		$rootScope.confirmButton  = 'Hapus';

// 		$rootScope.modalAction = function() {
// 			var data = { 'id' : id };

// 			$('#indexConfirm').modal('hide');
			
// 			RequestService.delete(data, 'userdevice').then(function(response) {
// 				$scope.reloadData();
// 			});
// 		}

// 		$('#indexConfirm').modal('show');
// 	}

// 	$scope.init();
// });

app.controller('userIndexCtrl', function($scope, $rootScope, $state, RequestService) {
	$scope.users = [];
	$scope.getArray = [];
	$scope.pages = [];
	$scope.last;
	$scope.limit = 10;
	$scope.currentPage = 1;
	var data = {};

	$scope.init = function() {
        // if ($scope.status != 99) {
	       //  data.status = $scope.status;
        // }

		RequestService.getbypaging(data, 'userdevice', $scope.currentPage, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.users.push(data[ii]);
			};
		});

		RequestService.getreport(data, 'userdevice').then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.getArray.push({
        			a: ii+1, 
        			b: data[ii].name,
        			c: data[ii].device,
        			d: data[ii].phone,
        			e: data[ii].email,
        			f: data[ii].address
        		});
			};
		});

		RequestService.gettotal(data, 'userdevice').then(function(data) {
			for (var ii = 1; ii <= Math.ceil(data / $scope.limit); ii++) {
				$scope.pages.push(ii);
			};

			$scope.last = $scope.pages.length;
		});
	};

	$scope.gotoPage = function(page) {
		$scope.users = [];
		$scope.currentPage = page;

		RequestService.getbypaging(data, 'userdevice', page, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.users.push(data[ii]);
			};
		});
	}

	$scope.reloadData = function() {
		$scope.users = [];
		$scope.pages = [];

		if ($scope.status != 99) {
	        data.status = $scope.status;
        } else {
        	data = {};
        }

		RequestService.getbypaging(data, 'userdevice', $scope.currentPage, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.users.push(data[ii]);
			};
		});

		RequestService.gettotal(data, 'userdevice').then(function(data) {
			for (var ii = 1; ii <= Math.ceil(data / $scope.limit); ii++) {
				$scope.pages.push(ii);
			};

			$scope.last = $scope.pages.length;
		});
	}

	$scope.destroyUser = function(id) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'userdevice').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});
