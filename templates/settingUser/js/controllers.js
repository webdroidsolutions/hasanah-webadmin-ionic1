app.controller('settingUserCtrl', function($scope) {
	$scope.init = function() {

	}

	$scope.init();
});

app.controller('settingUserIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.users = [];

	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
	        var q = $q.defer();
	        var data = {};

	        // if ($scope.status != 99) {
		       //  data.status = $scope.status;
	        // }

	        RequestService.getall('user').then(function(data) {
				q.resolve(data);
			});

	        return q.promise;
	    }).withOption('createdRow', createdRow);

	    $scope.dtColumns = [
	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
	        DTColumnBuilder.newColumn('name').withTitle('Nama'),
	        DTColumnBuilder.newColumn('email').withTitle('Email'),
	        DTColumnBuilder.newColumn('user_type').withTitle('Tipe'),
	        DTColumnBuilder.newColumn('description').withTitle('Deskripsi'),
	        // DTColumnBuilder.newColumn(null).withTitle('Status').renderWith(statusHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
	    ];

	    $scope.dtInstance = {};

	    function createdRow(row, data, dataIndex) {
	    	$compile(angular.element(row).contents())($scope);
	    }

	    function numberHtml(data, type, full, meta) {
	    	return meta.row + 1;
	    }

		// function detailHtml(data, type, full, meta) {
		//     return '<a href="" ng-click="navigateTo(\'app.settingUser.detail\', users[' + meta.row + '])">' + data.name + '</a>';
		// }

		// function statusHtml(data, type, full, meta) {
		// 	var status;

		// 	if (data.status) {
		// 		status = '<span class="status-active">Aktif</span>';
		// 	} else {
		// 		status = '<span class="status-nonactive">Non-Aktif</span>';
		// 	}

		//     return status;
		// }

		//    function imageHtml(data, type, full, meta) {
		//        return '<img src="data:image;base64,' + data.img + '" width="55px">';
		// }

		// function timeHtml(data, type, full, meta) {
		// 	var date = new Date(data.time);

		// 	return TimeService.days[date.getDay()] + ', ' 
		// 		+ ('0' + date.getDate()).slice(-2) + ' '
		// 		+ TimeService.months[date.getMonth()] + ' '
		// 		+ date.getFullYear() + ' '
		// 		+ ('0' + date.getHours()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2);
		// }

		function actionHtml(data, type, full, meta) {
			$scope.users[meta.row] = data;

			return '\
				<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.settingUser.edit\', users[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
				<button class="btn btn-danger btn-xs btn-block" ng-click="destroyUser(users[' + meta.row + '].id)"><i class="fa fa-trash-o"></i> Delete</button>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			user: objectData
		});
	}

	$scope.destroyUser = function(id) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'user').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('settingUserCreateCtrl', function($scope, $state, $rootScope, RequestService) {
	$scope.obj={};
	$scope.init = function() {
		$scope.errorEmail = false;
		$scope.errorName = false;
		$scope.errorType = false;
		$scope.errorPassword = false;
		$scope.errorRepassword = false;
		$scope.errorTravelagency=false;
		RequestService.getRoles('role').then(function(response) {
            $scope.types=response;
        });
	}

	$scope.changeType=function()
	{
		if ($scope.type.toUpperCase()==='TRAVEL ADMIN') {
			RequestService.getall('travelagency').then(function(response) {
	            $scope.agents=response;
	        });
		} 
	};

	$scope.insertUser = function() {
		var check = true;

		$scope.errorEmail = false;
		$scope.errorName = false;
		$scope.errorType = false;
		$scope.errorPassword = false;
		$scope.errorRepassword = false;

		if (!$scope.email) {
			$scope.errorEmail = true;
			check = false;
		}

		if (!$scope.name) {
			$scope.errorName = true;
			check = false;
		}

		if (!$scope.type) {
			$scope.errorType = true;
			check = false;
		}

		if (!$scope.password) {
			$scope.errorPassword = true;
			check = false;
		}

		if (!$scope.repassword) {
			$scope.errorRepassword = true;
			check = false;
		}

		if ($scope.password != $scope.repassword) {
			$scope.errorPassword = true;
			$scope.errorRepassword = true;
			check = false;
		}		

		if ($scope.type.toUpperCase()==='TRAVEL ADMIN') {
			if (!$scope.obj.travelagent_id) {
				$scope.errorTravelagency=true;
				check = false;
			}
		}

		if (check) {
			var data = {
				'name': $scope.name,
				'email': $scope.email,
				'user_type': $scope.type,
				'pass': $scope.password,
				'description': $scope.description,
				'travelagent_id':$scope.obj.travelagent_id
			}

			RequestService.save(data, 'user').then(function(response) {
				$state.go('app.settingUser.index');

				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = response.data.message;

				$('#indexModal').modal('show');
			});
		}
	}

	$scope.init();
});

app.controller('settingUserEditCtrl', function($scope, $stateParams, $state, $rootScope, RequestService) {
	$scope.init = function() {
		$scope.change=false;
		if ($stateParams.user) { 
			$scope.user = $stateParams.user;

			$scope.user.type = $scope.user.user_type;

			$scope.errorEmail = false;
			$scope.errorName = false;
			$scope.errorTravelagency=false;
			RequestService.getRoles('role').then(function(response) {
	            $scope.types=response;
	        });
	        $scope.changeType();
			//$scope.types = [{'name': 'ADMIN'}, {'name': 'ADMIN CABANG'}, {'name': 'ADMIN TRAVEL'}];
		} else $state.go('app.settingUser.index');
	}

	$scope.changeType=function()
	{
		if ($scope.user.type.toUpperCase()==='TRAVEL ADMIN') {
			RequestService.getall('travelagency').then(function(response) {
	            $scope.agents=response;
	        });
			//$scope.errorTravelagency=true;
		} 
	};

	$scope.updateUser = function() {
		var check = true;

		$scope.errorEmail = false;
		$scope.errorName = false;

		if (!$scope.user.email) {
			$scope.errorEmail = true;
			check = false;
		}

		if (!$scope.user.name) {
			$scope.errorName = true;
			check = false;
		}

		if ($scope.user.type.toUpperCase()==='TRAVEL ADMIN') {
			if (!$scope.user.travelagent_id) {
				$scope.errorTravelagency=true;
				check = false;
			}
		}
		else {
			$scope.user.travelagent_id=null;
		}

		if ($scope.change) 
		{
			if (!$scope.user.oPass) 
			{
				alert("Please enter Old password");
				return;
			}
			else if (!$scope.user.nPass) 
			{
				alert("Please enter new password");
				return;
			}
			else if (!$scope.user.rPass) 
			{
				alert("Please enter new password");
				return;
			}
			else if ($scope.user.pass!=$scope.user.oPass) 
			{
				alert("Old password doesnot match");
				return;
			}
			else if ($scope.user.nPass!=$scope.user.rPass)
			{
				alert("New password and confirm password doesnot match");
				return;
			}
			else
			{
				$scope.user.pass=$scope.user.nPass;
			}
		}
		if (check) {
			var data = {
				'id': $scope.user.id,
				'name': $scope.user.name,
				'email': $scope.user.email,
				'pass': $scope.user.pass,
				'user_type': $scope.user.type,
				'description': $scope.user.description,
				'travelagent_id':$scope.user.travelagent_id
			}

			alert(JSON.stringify($scope.user));
			RequestService.save(data, 'user').then(function(response) {
				$state.go('app.settingUser.index');

				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = response.data.message;

				$('#indexModal').modal('show');
			});
		}
	};

	/*$scope.changePassword=function()
	{

	}*/
	$scope.init();
});