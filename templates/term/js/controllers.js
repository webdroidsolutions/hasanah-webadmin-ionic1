app.controller('termCtrl', function($scope) {
	$scope.init = function() {

	}

	$scope.init();
});

app.controller('termIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.terms = [];

	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
	        var q = $q.defer();

	        RequestService.getall('term').then(function(data) {
				q.resolve(data);
			});

	        return q.promise;
	    }).withOption('createdRow', createdRow);

	    $scope.dtColumns = [
	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
	        DTColumnBuilder.newColumn('name').withTitle('Nama'),
	        DTColumnBuilder.newColumn('content').withTitle('Deskripsi'),
	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
	    ];

	    $scope.dtInstance = {};

	    function createdRow(row, data, dataIndex) {
	    	$compile(angular.element(row).contents())($scope);
	    }

	    function numberHtml(data, type, full, meta) {
	    	return meta.row + 1;
	    }

		// function imageHtml(data, type, full, meta) {
		//        return '<img src="' + data.img + '" width="55px">';
		// }

		// function timeHtml(data, type, full, meta) {
		// 	var date = new Date(data.time);

		// 	return TimeService.days[date.getDay()] + ', ' 
		// 		+ ('0' + date.getDate()).slice(-2) + ' '
		// 		+ TimeService.months[date.getMonth()] + ' '
		// 		+ date.getFullYear() + ' '
		// 		+ ('0' + date.getHours()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2);
		// }

		function actionHtml(data, type, full, meta) {
			$scope.terms[meta.row] = data;

			return '\
				<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.term.edit\', terms[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
				<button class="btn btn-danger btn-xs btn-block" ng-click="destroyTerm(terms[' + meta.row + '].id)"><i class="fa fa-trash-o"></i> Delete</button>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			term: objectData
		});
	}

	$scope.destroyTerm = function(id) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'term').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('termCreateCtrl', function($scope, $state, $rootScope, RequestService) {
	$scope.init = function() {
		$scope.errorName = false;
		$scope.errorImg = false;
	}

	$scope.insertTerm = function() {
		var check = true;

		$scope.errorName = false;

		if (!$scope.name) {
			$scope.errorName = true;
			check = false;
		}

		if (check) {
			var date = new Date();

			var data = {
				'name': $scope.name,
	            'content': $scope.content
			}

			RequestService.save(data, 'term').then(function(response) {
				$state.go('app.term.index');

				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = response.data.message;

				$('#indexModal').modal('show');
			});
		}
	}

	$scope.init();
});

app.controller('termEditCtrl', function($scope, $stateParams, $state, $rootScope, RequestService) {
	$scope.init = function() {
		if ($stateParams.term) { 
			$scope.term = $stateParams.term;

			$scope.errorName = false;
		} else $state.go('app.term.index');
	}

	$scope.updateTerm = function() {
		var check = true;

		$scope.errorName = false;

		if (!$scope.term.name) {
			$scope.errorName = true;
			check = false;
		}

		if (check) {
			var data = {
				'id': $scope.term.id,
				'name': $scope.term.name,
	            'content': $scope.term.content
			}

			RequestService.save(data, 'term').then(function(response) {
				$state.go('app.term.index');

				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = response.data.message;

				$('#indexModal').modal('show');
			});
		}
	}

	$scope.init();
});