app.controller('filingFinanceIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.filings = [];
		$scope.getArray = [];
		$scope.emptyCsv = true;

		$('#daterange').daterangepicker({
			autoUpdateInput: false,
			locale: {
				cancelLabel: 'Clear'
			}
		});

		$('#daterange').on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});

		$('#daterange').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));

			var data = {
				date_start: picker.startDate.format('YYYY-MM-DD'),
				date_end: picker.endDate.format('YYYY-MM-DD')
			}

			RequestService.getreport(data, 'financefiling').then(function(data) {
				$scope.getArray = [];
				if (data.length > 0) {
					for (var ii = 0; ii < data.length; ii++) {
		        		$scope.getArray.push({
		        			a: ii+1, 
		        			b: data[ii].financefiling.name,
		        			c: data[ii].finance.name,
		        			d: data[ii].financefiling.facility,
		        			e: data[ii].branch.name,
		        			f: data[ii].financefiling.address, 
		        			g: data[ii].financefiling.email, 
		        			h: data[ii].financefiling.phone, 
		        			i: data[ii].financefiling.birthday,
		        			j: data[ii].financefiling.gender,
		        			k: data[ii].financefiling.city,
		        			l: data[ii].financefiling.kelurahan,
		        			m: data[ii].financefiling.kecamatan,
		        			n: data[ii].financefiling.rt,
		        			o: data[ii].financefiling.rw,
		        			p: data[ii].financefiling.postal_code,
		        			q: data[ii].financefiling.job,
		        			r: data[ii].financefiling.company_type,
		        			s: data[ii].financefiling.job_position,
		        			t: data[ii].financefiling.business_field,
		        			u: data[ii].financefiling.year_start_work,
		        			v: data[ii].financefiling.service_length,
		        			w: data[ii].financefiling.office_name,
		        			x: data[ii].financefiling.office_phone,
		        			y: data[ii].financefiling.income,
		        			z: data[ii].financefiling.contact_to
		        		});
		        	}

		        	$scope.emptyCsv = false;
				} else {
					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = 'Data tidak tersedia';

					$('#indexModal').modal('show');					

		        	$scope.emptyCsv = true;
				}
			});
		});

	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
	        var q = $q.defer();

	        RequestService.getall('financefiling').then(function(data) {
				q.resolve(data);
			});

	        return q.promise;
	    }).withOption('createdRow', createdRow);

	    $scope.dtColumns = [
	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Tanggal').renderWith(timeHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Nama').renderWith(detailHtml),
	        DTColumnBuilder.newColumn('finance.name').withTitle('Pengajuan'),
	        DTColumnBuilder.newColumn('branch.name').withTitle('Cabang Terdekat'),
	        // DTColumnBuilder.newColumn(null).withTitle('Pengajuan').renderWith(filingType),
	        DTColumnBuilder.newColumn('financefiling.address').withTitle('Alamat'),
	        DTColumnBuilder.newColumn('financefiling.email').withTitle('Email'),
	        DTColumnBuilder.newColumn('financefiling.phone').withTitle('No.Telp'),
	        // DTColumnBuilder.newColumn('description').withTitle('Deskripsi'),
	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
	    ];

	    $scope.dtInstance = {};

	    function createdRow(row, data, dataIndex) {
	    	$compile(angular.element(row).contents())($scope);
	    }

	    function numberHtml(data, type, full, meta) {
	    	return meta.row + 1;
	    }

	    function detailHtml(data, type, full, meta) {
	        return '<a href="" ng-click="navigateTo(\'app.filing.finance.detail\', filings[' + meta.row + '])">' + data.financefiling.name + '</a>';
		}

		// function imageHtml(data, type, full, meta) {
		//        return '<img src="' + data.img + '" width="55px">';
		// }

		function timeHtml(data, type, full, meta) {
			var date = new Date(data.financefiling.date);

			return ('0' + date.getDate()).slice(-2) + ' '
				+ TimeService.months[date.getMonth()] + ' '
				+ date.getFullYear();
		}

		function actionHtml(data, type, full, meta) {
			$scope.filings[meta.row] = data;

				// <button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.financeFiling.edit\', filings[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
			return '\
				<button class="btn btn-danger btn-xs btn-block" ng-click="destroyFiling(filings[' + meta.row + '].financefiling.id)"><i class="fa fa-trash-o"></i> Delete</button>\
			';
		}

		function filingType(data, type, full, meta) {
			var data = { id: data.type_id };

			// RequestService.getby(data, 'finance', 'id').then(function(response) {
				// return response.name;
			// });
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			financeFiling: objectData
		});
	}

	$scope.destroyFiling = function(id) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'financefiling').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('filingFinanceDetailCtrl', function($scope, $stateParams, $state) {
	$scope.init = function() {
		if ($stateParams.financeFiling) { 
			$scope.financeFiling = $stateParams.financeFiling;
		} else $state.go('app.filing.finance.index');
	}

	$scope.init();
});

app.controller('filingUmrohIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.filings = [];
		$scope.getArray = [];
		$scope.emptyCsv = true;

		$('#daterange').daterangepicker();

		$('#daterange').on('apply.daterangepicker', function(ev, picker) {
			var data = {
				date_start: picker.startDate.format('YYYY-MM-DD'),
				date_end: picker.endDate.format('YYYY-MM-DD')
			}

			RequestService.getreport(data, 'umrohfiling').then(function(data) {
				$scope.getArray = [];
				if (data.length > 0) {
					for (var ii = 0; ii < data.length; ii++) {
		        		$scope.getArray.push({
		        			a: ii+1, 
		        			b: data[ii].umrohfiling.name,
		        			c: data[ii].umroh.name,
		        			d: data[ii].umrohfiling.facility,
		        			e: data[ii].branch.name,
		        			f: data[ii].umrohfiling.address, 
		        			g: data[ii].umrohfiling.email, 
		        			h: data[ii].umrohfiling.phone, 
		        			i: data[ii].umrohfiling.birthday,
		        			j: data[ii].umrohfiling.gender,
		        			k: data[ii].umrohfiling.city,
		        			l: data[ii].umrohfiling.kelurahan,
		        			m: data[ii].umrohfiling.kecamatan,
		        			n: data[ii].umrohfiling.rt,
		        			o: data[ii].umrohfiling.rw,
		        			p: data[ii].umrohfiling.postal_code,
		        			q: data[ii].umrohfiling.job,
		        			r: data[ii].umrohfiling.company_type,
		        			s: data[ii].umrohfiling.job_position,
		        			t: data[ii].umrohfiling.business_field,
		        			u: data[ii].umrohfiling.year_start_work,
		        			v: data[ii].umrohfiling.service_length,
		        			w: data[ii].umrohfiling.office_name,
		        			x: data[ii].umrohfiling.office_phone,
		        			y: data[ii].umrohfiling.income,
		        			z: data[ii].umrohfiling.contact_to
		        		});
		        	}

		        	$scope.emptyCsv = false;
				} else {
					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = 'Data tidak tersedia';

					$('#indexModal').modal('show');					

		        	$scope.emptyCsv = true;
				}
			});
		});

	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
	        var q = $q.defer();

	        RequestService.getall('umrohfiling').then(function(data) {
				q.resolve(data);
			});

	        return q.promise;
	    }).withOption('createdRow', createdRow);

	    $scope.dtColumns = [
	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Tanggal').renderWith(timeHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Nama').renderWith(detailHtml),
	        DTColumnBuilder.newColumn('umroh.name').withTitle('Pengajuan'),
	        DTColumnBuilder.newColumn('branch.name').withTitle('Cabang Terdekat'),
	        DTColumnBuilder.newColumn('umrohfiling.address').withTitle('Alamat'),
	        DTColumnBuilder.newColumn('umrohfiling.email').withTitle('Email'),
	        DTColumnBuilder.newColumn('umrohfiling.phone').withTitle('No.Telp'),
	        // DTColumnBuilder.newColumn('description').withTitle('Deskripsi'),
	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
	    ];

	    $scope.dtInstance = {};

	    function createdRow(row, data, dataIndex) {
	    	$compile(angular.element(row).contents())($scope);
	    }

	    function numberHtml(data, type, full, meta) {
	    	return meta.row + 1;
	    }

	    function detailHtml(data, type, full, meta) {
	        return '<a href="" ng-click="navigateTo(\'app.filing.umroh.detail\', filings[' + meta.row + '])">' + data.umrohfiling.name + '</a>';
		}

		// function imageHtml(data, type, full, meta) {
		//        return '<img src="' + data.img + '" width="55px">';
		// }

		// function timeHtml(data, type, full, meta) {
		// 	var date = new Date(data.time);

		function timeHtml(data, type, full, meta) {
			var date = new Date(data.umrohfiling.date);

			return ('0' + date.getDate()).slice(-2) + ' '
				+ TimeService.months[date.getMonth()] + ' '
				+ date.getFullYear();
		}

		function actionHtml(data, type, full, meta) {
			$scope.filings[meta.row] = data;

			return '\
				<button class="btn btn-danger btn-xs btn-block" ng-click="destroyFiling(filings[' + meta.row + '].umrohfiling.id)"><i class="fa fa-trash-o"></i> Delete</button>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			umrohFiling: objectData
		});
	}

	$scope.destroyFiling = function(id) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'umrohfiling').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('filingUmrohDetailCtrl', function($scope, $stateParams, $state) {
	$scope.init = function() {
		if ($stateParams.umrohFiling) { 
			$scope.umrohFiling = $stateParams.umrohFiling;
		} else $state.go('app.filing.umroh.index');
	}

	$scope.init();
});

app.controller('filingPropertyIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.filings = [];
		$scope.getArray = [];
		$scope.emptyCsv = true;

		$('#daterange').daterangepicker();

		$('#daterange').on('apply.daterangepicker', function(ev, picker) {
			var data = {
				date_start: picker.startDate.format('YYYY-MM-DD'),
				date_end: picker.endDate.format('YYYY-MM-DD')
			}

			RequestService.getreport(data, 'propertyfiling').then(function(data) {
				$scope.getArray = [];
				if (data.length > 0) {
					for (var ii = 0; ii < data.length; ii++) {
		        		$scope.getArray.push({
		        			a: ii+1, 
		        			b: data[ii].propertyfiling.name,
		        			c: data[ii].property.name,
		        			d: data[ii].propertyfiling.facility,
		        			e: data[ii].branch.name,
		        			f: data[ii].propertyfiling.address, 
		        			g: data[ii].propertyfiling.email, 
		        			h: data[ii].propertyfiling.phone, 
		        			i: data[ii].propertyfiling.birthday,
		        			j: data[ii].propertyfiling.gender,
		        			k: data[ii].propertyfiling.city,
		        			l: data[ii].propertyfiling.kelurahan,
		        			m: data[ii].propertyfiling.kecamatan,
		        			n: data[ii].propertyfiling.rt,
		        			o: data[ii].propertyfiling.rw,
		        			p: data[ii].propertyfiling.postal_code,
		        			q: data[ii].propertyfiling.job,
		        			r: data[ii].propertyfiling.company_type,
		        			s: data[ii].propertyfiling.job_position,
		        			t: data[ii].propertyfiling.business_field,
		        			u: data[ii].propertyfiling.year_start_work,
		        			v: data[ii].propertyfiling.service_length,
		        			w: data[ii].propertyfiling.office_name,
		        			x: data[ii].propertyfiling.office_phone,
		        			y: data[ii].propertyfiling.income,
		        			z: data[ii].propertyfiling.contact_to
		        		});
		        	}

		        	$scope.emptyCsv = false;
				} else {
					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = 'Data tidak tersedia';

					$('#indexModal').modal('show');					

		        	$scope.emptyCsv = true;
				}
			});
		});

	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
	        var q = $q.defer();

	        RequestService.getall('propertyfiling').then(function(data) {
				q.resolve(data);
			});

	        return q.promise;
	    }).withOption('createdRow', createdRow);

	    $scope.dtColumns = [
	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Tanggal').renderWith(timeHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Nama').renderWith(detailHtml),
	        DTColumnBuilder.newColumn('property.name').withTitle('Pengajuan'),
	        DTColumnBuilder.newColumn('branch.name').withTitle('Cabang Terdekat'),
	        DTColumnBuilder.newColumn('propertyfiling.address').withTitle('Alamat'),
	        DTColumnBuilder.newColumn('propertyfiling.email').withTitle('Email'),
	        DTColumnBuilder.newColumn('propertyfiling.phone').withTitle('No.Telp'),
	        // DTColumnBuilder.newColumn('description').withTitle('Deskripsi'),
	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
	    ];

	    $scope.dtInstance = {};

	    function createdRow(row, data, dataIndex) {
	    	$compile(angular.element(row).contents())($scope);
	    }

	    function numberHtml(data, type, full, meta) {
	    	return meta.row + 1;
	    }

	    function detailHtml(data, type, full, meta) {
	        return '<a href="" ng-click="navigateTo(\'app.filing.property.detail\', filings[' + meta.row + '])">' + data.propertyfiling.name + '</a>';
		}

		// function imageHtml(data, type, full, meta) {
		//        return '<img src="' + data.img + '" width="55px">';
		// }

		// function timeHtml(data, type, full, meta) {
		// 	var date = new Date(data.time);

		function timeHtml(data, type, full, meta) {
			var date = new Date(data.propertyfiling.date);

			return ('0' + date.getDate()).slice(-2) + ' '
				+ TimeService.months[date.getMonth()] + ' '
				+ date.getFullYear();
		}

		function actionHtml(data, type, full, meta) {
			$scope.filings[meta.row] = data;

			return '\
				<button class="btn btn-danger btn-xs btn-block" ng-click="destroyFiling(filings[' + meta.row + '].propertyfiling.id)"><i class="fa fa-trash-o"></i> Delete</button>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			propertyFiling: objectData
		});
	}

	$scope.destroyFiling = function(id) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'propertyfiling').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('filingPropertyDetailCtrl', function($scope, $stateParams, $state) {
	$scope.init = function() {
		if ($stateParams.propertyFiling) { 
			$scope.propertyFiling = $stateParams.propertyFiling;
		} else $state.go('app.filing.property.index');
	}

	$scope.init();
});