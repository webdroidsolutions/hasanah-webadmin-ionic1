app.controller('loginCtrl', function($scope, $rootScope, $state, RequestService) {
	$scope.init = function() {
		$('body').addClass('login-page');

		RequestService.ceksession();

		$scope.errorEmail = false;
		$scope.errorPassword = false;

		$('#email-user').focus();
	}

	$scope.doLogin = function() {
		var check = true;

		$scope.errorEmail = false;
		$scope.errorPassword = false;

		if (!$scope.email) {
			$scope.errorEmail = true;
			check = false;
		}

		if (!$scope.password) {
			$scope.errorPassword = true;
			check = false;
		}

		if (check) {
			var data = {
				'email': $scope.email,
				'pass': $scope.password
			}

			RequestService.login(data).then(function(response) {
				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = response.data.message;

				$('#indexModal').modal('show');

				if (response.data.status === 'success') {
					if (response.data.user_type == 'ADMIN') {
						$state.go('app.home.slide.index');
					} else {
						$state.go('app.umroh.index');
					}

					localStorage.setItem('links',JSON.stringify(response.data.menus));
					localStorage.setItem('sn', response.data.sn);
					localStorage.setItem('name', response.data.name);
					localStorage.setItem('user_type', response.data.user_type);
					localStorage.setItem('travelagent_id', response.data.travelagentid);
				}
			});
		}
	}
	
	$scope.forgotPassword = function() {
		var check = true;

		$scope.errorEmail = false;
		if (!$scope.email) {
			$scope.errorEmail = true;
			check = false;
			alert("Please enter password");
		}

		if (check) {
			var data = {
				'email': $scope.email			}

			RequestService.forgotPassword(data).then(function(response) {
				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = response.data.message;

				$('#indexModal').modal('show');

				/*if (response.data.status === 'success') {
					if (response.data.user_type == 'ADMIN') {
						$state.go('app.home.slide.index');
					} else {
						$state.go('app.umroh.index');
					}

					localStorage.setItem('sn', response.data.sn);
					localStorage.setItem('name', response.data.name);
					localStorage.setItem('user_type', response.data.user_type);
				}*/
			});
		}
	}


	$scope.init();
})