app.controller('umrohCtrl', function($scope) {
	$scope.init = function() {

	}

	$scope.init();
});

/*app.controller('umrohIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.umrohs = [];

	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
	        var q = $q.defer();
	        var data = {};

	        if ($scope.status != 99) {
		        data.status = $scope.status;
	        }

	        RequestService.getbystatus(data, 'umroh').then(function(data) {
				q.resolve(data);
			});

	        return q.promise;
	    }).withOption('createdRow', createdRow);

	    $scope.dtColumns = [
	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Nama').renderWith(detailHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Gambar').notSortable().renderWith(imageHtml),
	        DTColumnBuilder.newColumn('city').withTitle('Kota'),
	        DTColumnBuilder.newColumn('quota').withTitle('Kuota'),
	        DTColumnBuilder.newColumn(null).withTitle('Status').renderWith(statusHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
	    ];

	    $scope.dtInstance = {};

	    function createdRow(row, data, dataIndex) {
	    	$compile(angular.element(row).contents())($scope);
	    }

	    function numberHtml(data, type, full, meta) {
	    	return meta.row + 1;
	    }

	    function detailHtml(data, type, full, meta) {
	        return '<a href="" ng-click="navigateTo(\'app.umroh.detail\', umrohs[' + meta.row + '])">' + data.name + '</a>';
		}

		function statusHtml(data, type, full, meta) {
	    	var status;

	    	if (data.status) {
	    		status = '<span class="status-active">Aktif</span>';
	    	} else {
	    		status = '<span class="status-nonactive">Non-Aktif</span>';
	    	}

	        return status;
		}

	    function imageHtml(data, type, full, meta) {
	        return '<img src="data:image/png;base64,' + data.img + '" width="47px">';
		}

		// function timeHtml(data, type, full, meta) {
		// 	var date = new Date(data.time);

		// 	return TimeService.days[date.getDay()] + ', ' 
		// 		+ ('0' + date.getDate()).slice(-2) + ' '
		// 		+ TimeService.months[date.getMonth()] + ' '
		// 		+ date.getFullYear() + ' '
		// 		+ ('0' + date.getHours()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2);
		// }

		function actionHtml(data, type, full, meta) {
			$scope.umrohs[meta.row] = data;

			return '\
				<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.umroh.edit\', umrohs[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
				<button class="btn btn-danger btn-xs btn-block" ng-click="destroyUmroh(umrohs[' + meta.row + '].id)"><i class="fa fa-trash-o"></i> Delete</button>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			umroh: objectData
		});
	}

	$scope.destroyUmroh = function(id) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'umroh').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});*/

app.controller('umrohIndexCtrl', function($scope, $rootScope, $state, RequestService) {
	$scope.baseUrl = RequestService.url.replace('adm/', '');
	$scope.umrohs = [];
	$scope.pages = [];
	$scope.last;
	$scope.limit = 10;
	$scope.currentPage = 1;
	var data = {};

	$scope.init = function() {
		RequestService.getbypaging(data, 'umroh', $scope.currentPage, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.umrohs.push(data[ii]);
			};
		});

		RequestService.gettotal(data, 'umroh').then(function(data) {
			for (var ii = 1; ii <= Math.ceil(data / $scope.limit); ii++) {
				$scope.pages.push(ii);
			};

			$scope.last = $scope.pages.length;
		});
		RequestService.getall('travelagency').then(function(response) {
			$scope.agents=response;
		});
	};

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			umroh: objectData
		});
	}

	$scope.gotoPage = function(page) {
		$scope.umrohs = [];
		$scope.currentPage = page;

		RequestService.getbypaging(data, 'umroh', page, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.umrohs.push(data[ii]);
			};
		});
	}

	$scope.reloadData = function() {
		$scope.umrohs = [];
		$scope.pages = [];

		if ($scope.status != 99) {
			data.status = $scope.status;
		} else {
			data = {};
		}

		RequestService.getbypaging(data, 'umroh', $scope.currentPage, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.umrohs.push(data[ii]);
			};
		});

		RequestService.gettotal(data, 'umroh').then(function(data) {
			for (var ii = 1; ii <= Math.ceil(data / $scope.limit); ii++) {
				$scope.pages.push(ii);
			};

			$scope.last = $scope.pages.length;
		});
	}

	$scope.destroyUmroh = function(id, old_img, old_pdf) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id, 'old_img' : old_img, 'old_pdf' : old_pdf };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'umroh').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('umrohDetailCtrl', function($scope, $stateParams, $state, RequestService) {
	$scope.baseUrl = RequestService.url.replace('adm/', '');
	$scope.init = function() {
		if ($stateParams.umroh) { 
			$scope.umroh = $stateParams.umroh;

			var departure = new Date($scope.umroh.departure);
			$scope.departure = departure.getMonth() + 1 + '/' + departure.getDate() + '/' + departure.getFullYear();

			if ($scope.umroh.status) {
				$scope.umroh.status = 'Aktif';
			} else {
				$scope.umroh.status = 'Non-Aktif';
			}
		} else $state.go('app.umroh.index');
	}

	$scope.init();
});

app.controller('umrohCreateCtrl', function($filter, $scope, $state, $rootScope, RequestService,bsLoadingOverlayService) {
	$scope.init = function() {
		$scope.cities = [];
		$scope.obj={};
		$scope.price = 0;
		$scope.errorName = false;
		$scope.errorCity = false;
		$scope.errorDeparture = false;
		$scope.errorPrice = false;
		$scope.errorPacket = false;
		$scope.errorPlane = false;
		$scope.errorHotel = false;
		$scope.errorPhone = false;
		$scope.errorQuota = false;
		$scope.errorDepartDate = false;
		$scope.errorQuad = false;
		$scope.errorTravelagency = false;
		$scope.errorMeca = false;
		$scope.errorImg = false;
		$scope.errorPdf = false;
		var today=new Date();
		// var today = Date.parse($filter('gmISODate')(new Date()))
		$scope.activeDate;
		$scope.selectedDates = [today];
		$scope.type = 'individual';

		$('.datepicker').datepicker({
			autoclose: true
		});
		
		$scope.status = true;
		RequestService.getall('travelagency').then(function(response) {
			$scope.agents=response;
		});
		RequestService.getall('city').then(function(cities) {
			for (var ii = 0; ii < cities.length; ii++) {
				$scope.cities.push(cities[ii]);
			}
		});
	}

	$scope.isActive=function(date){
		var found=false;
		for (var i = 0; i < $scope.selectedDates.length; i++) {
			if ($scope.selectedDates[i]==date) 
			{
				found=true;
				return true;
			}
		}
		return found;
	};

	$scope.removeFromSelected = function(dt) {
		$scope.selectedDates.splice($scope.selectedDates.indexOf(dt), 1);
	}

	$scope.insertUmroh = function() {
		var check = true;

		$scope.errorName = false;
		$scope.errorCity = false;
		$scope.errorDeparture = false;
		$scope.errorPrice = false;
		$scope.errorPacket = false;
		$scope.errorPlane = false;
		$scope.errorHotel = false;
		$scope.errorPhone = false;
		$scope.errorQuota = false;
		$scope.errorDepartDate = false;
		$scope.errorQuad = false;
		$scope.errorTravelagency = false;
		$scope.errorPdf = false;
		$scope.errorMeca = false;
		$scope.errorImg = false;
		$scope.errorPdf = false;
		
		if (!$scope.name) {
			$scope.errorName = true;
			check = false;
		}

		if (!$scope.obj.mecca_pilgrimage_list) {
			$scope.errorMeca = true;
			check = false;
		}

		if (!$scope.city) {
			$scope.errorCity = true;
			check = false;
		}

		if (!$scope.departure) {
			$scope.errorDeparture = true;
			check = false;
		}

		if (!$scope.price) {
			$scope.errorPrice = true;
			check = false;
		}

		if (!$scope.packet) {
			$scope.errorPacket = true;
			check = false;
		}

		if (!$scope.plane) {
			$scope.errorPlane = true;
			check = false;
		}

		if (!$scope.hotel) {
			$scope.errorHotel = true;
			check = false;
		}

		if (!$scope.phone) {
			$scope.errorPhone = true;
			check = false;
		}

		if (!$scope.quota) {
			$scope.errorQuota = true;
			check = false;
		}

		if (!$scope.quad_price) {
			$scope.errorQuad = true;
			check = false;
		}

		if (!$scope.obj.travelagent_id) {
			$scope.errorTravelagency = true;
			check = false;
		}

		if (!$scope.imgFile) {
			$scope.errorImg = true;
			check = false;
		}

		if (!$scope.pdfFile) {
			$scope.errorPdf = true;
			check = false;
		}

		if ($scope.selectedDates.length==0) {
			$scope.errorDepartDate = true;
			check = false;
		}
		else
		{
			var dates=[];
			for (var i = 0; i < $scope.selectedDates.length; i++) {
				dates[i]=$filter('date')($scope.selectedDates[i], 'MM/dd/yyyy');
			}
			
			for (var i = 0; i < dates.length; i++)
			{

				var obj={};
				obj.date=dates[i];
				dates[i]=obj;
			}
			var checkedDates=[];
			for (var i = 0; i < dates.length; i++)
			{
				var ctr=0;
				for (var j = 0; j < checkedDates.length; j++)
				{
					if (dates[i].date==checkedDates[j].date) 
					{
						ctr++;
					}
				}
				if (ctr==0) {
					checkedDates.push(dates[i]);
				}
			}	
			$scope.departure_date_list=checkedDates;
			console.log($scope.departure_date_list);
		    // var json=str.split(',');
		    // for (var i = 0; i < json.length; i++) {
		    // 	json[i]=new Date(json[i]);
		    // }
		}
		
		if (check) {
			var departure = new Date($scope.departure);
			var status;

			if ($scope.status) {
				status = 1;
			} else {
				status = 0;
			}

			var data = {
				'name': $scope.name,
				'img': $scope.imgFile.base64,
				'city': $scope.city,
				'departure': departure,
				'price': $scope.price,
				'packet': $scope.packet,
				'plane': $scope.plane,
				'hotel': $scope.hotel,
				'status': status,
				'phone': $scope.phone,
				'quota': $scope.quota,
				'content': $scope.content,
				'departure_date_list':$scope.departure_date_list,
				'quad_price':$scope.quad_price,
				'pdf_file':$scope.pdfFile,
				'travelagent_id':$scope.obj.travelagent_id,
				'mecca_pilgrimage_list':$scope.obj.mecca_pilgrimage_list
			}

			console.log(data);

			RequestService.save(data, 'umroh').then(function(response) {
				// RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
					$state.go('app.umroh.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// });
			});
		}
	}
	$scope.init();
});

app.controller('umrohEditCtrl', function($filter, $scope, $stateParams, $state, $rootScope, RequestService,bsLoadingOverlayService) {
	$scope.baseUrl = RequestService.url.replace('adm/', '');
	$scope.init = function() {
		if ($stateParams.umroh) { 
			$scope.cities = [];
			RequestService.getall('travelagency').then(function(response) {
				$scope.agents=response;
			});
			RequestService.getall('city').then(function(cities) {
				for (var ii = 0; ii < cities.length; ii++) {
					$scope.cities.push(cities[ii]);
				}
			});

			$scope.umroh = $stateParams.umroh;
			console.log($scope.umroh.departure_date_list);
			if ($scope.umroh.departure_date_list)
				$scope.umroh.departure_date_list = JSON.parse($scope.umroh.departure_date_list.replace('{', '[').replace('}', ']'));

			$scope.errorName = false;
			$scope.errorCity = false;
			$scope.errorDeparture = false;
			$scope.errorPrice = false;
			$scope.errorPacket = false;
			$scope.errorPlane = false;
			$scope.errorHotel = false;
			$scope.errorPhone = false;
			$scope.errorQuota = false;
			$scope.errorQuad = false;
			$scope.errorQuad = false;
			$scope.errorTravelagency = false;
			$scope.errorMeca = false;
			$scope.data={};
			$scope.activeDate;
			$scope.selectedDates=[];

			var departure = new Date($scope.umroh.departure);
			$scope.departure = departure.getMonth() + 1 + '/' + departure.getDate() + '/' + departure.getFullYear();

			if ($scope.umroh.departure_date_list) 
			{
				for (var i = 0; i < $scope.umroh.departure_date_list.length; i++) 
				{
					$scope.selectedDates.push(new Date($scope.umroh.departure_date_list[i]));
				}
			}

			$('.datepicker').datepicker({
				autoclose: true
			});

			if ($scope.umroh.status) {
				$scope.status = true;
			} else {
				$scope.status = false;
			}
		} else $state.go('app.umroh.index');
	}

	$scope.removeFromSelected = function(dt) {
		$scope.selectedDates.splice($scope.selectedDates.indexOf(dt), 1);
	}	

	$scope.updateUmroh = function() {
		var check = true;

		$scope.errorName = false;
		$scope.errorCity = false;
		$scope.errorDeparture = false;
		$scope.errorPrice = false;
		$scope.errorPacket = false;
		$scope.errorPlane = false;
		$scope.errorHotel = false;
		$scope.errorPhone = false;
		$scope.errorQuota = false;
		$scope.errorQuad = false;
		$scope.errorDepartDate = false;
		$scope.errorTravelagency = false;
		$scope.errorPdf = false;
		$scope.errorMeca = false;

		$scope.openDate= function() 
		{
			$scope.data.openStart=true;
		};

		if (!$scope.umroh.name) {
			$scope.errorName = true;
			check = false;
		}

		if (!$scope.umroh.mecca_pilgrimage_list) {
			$scope.errorMeca = true;
			check = false;
		}

		if (!$scope.umroh.city) {
			$scope.errorCity = true;
			check = false;
		}

		if (!$scope.umroh.departure) {
			$scope.errorDeparture = true;
			check = false;
		}

		if (!$scope.umroh.price) {
			$scope.errorPrice = true;
			check = false;
		}

		if (!$scope.umroh.packet) {
			$scope.errorPacket = true;
			check = false;
		}

		if (!$scope.umroh.plane) {
			$scope.errorPlane = true;
			check = false;
		}

		if (!$scope.umroh.hotel) {
			$scope.errorHotel = true;
			check = false;
		}

		if (!$scope.umroh.phone) {
			$scope.errorPhone = true;
			check = false;
		}

		if (!$scope.umroh.quota) {
			$scope.errorQuota = true;
			check = false;
		}

		if (!$scope.umroh.quad_price) {
			$scope.errorQuad = true;
			check = false;
		}
		if (!$scope.umroh.travelagentid) {
			$scope.errorTravelagency = true;
			check = false;
		}
		if ($scope.selectedDates.length==0) {
			$scope.errorDepartDate = true;
		}

		var dates=[];
		for (var i = 0; i < $scope.selectedDates.length; i++) {
			dates[i]=$filter('date')($scope.selectedDates[i], 'MM/dd/yyyy');
		}

		for (var i = 0; i < dates.length; i++)
		{
			var obj={};
			obj.date=dates[i];
			dates[i]=obj;
		}

		var checkedDates=[];
		for (var i = 0; i < dates.length; i++)
		{
			var ctr=0;
			for (var j = 0; j < checkedDates.length; j++)
			{
				if (dates[i].date==checkedDates[j].date) 
				{
					ctr++;
				}
			}
			if (ctr==0) {
				checkedDates.push(dates[i]);
			}
		}	
		$scope.umroh.departure_date_list=checkedDates;

		if (check) {
			var img = '';
			var old_img = '';

			if ($scope.imgFile) {
				img = $scope.imgFile.base64;
				old_img = $scope.umroh.img;
			} else {
				img = $scope.umroh.img;
			}

			var pdfFile = '';
			var old_pdfFile = '';

			if ($scope.pdfFile) {
				pdfFile = $scope.pdfFile;
				old_pdfFile = $scope.umroh.pdf_file_name;
			} else {
				pdfFile = $scope.umroh.pdf_file_name;
			}

			var departure = new Date($scope.departure);

			var status;

			if ($scope.status) {
				status = 1;
			} else {
				status = 0;
			}

			var data = {
				'id': $scope.umroh.id,
				'name': $scope.umroh.name,
				'img': img,
				'old_img': old_img,
				'city': $scope.umroh.city,
				'departure': departure,
				'price': $scope.umroh.price,
				'packet': $scope.umroh.packet,
				'plane': $scope.umroh.plane,
				'hotel': $scope.umroh.hotel,
				'phone': $scope.umroh.phone,
				'quota': $scope.umroh.quota,
				'content': $scope.umroh.content,
				'status': status,
				'quad_price':$scope.umroh.quad_price,
				'travelagent_id':$scope.umroh.travelagentid,	            
				'departure_date_list':$scope.umroh.departure_date_list,
				'mecca_pilgrimage_list':$scope.umroh.mecca_pilgrimage_list,
				'pdf_file':pdfFile,
				'old_pdf_file':old_pdfFile,
			}

			RequestService.save(data, 'umroh').then(function(response) {
				// if ($scope.imgFile) {
				// 	RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
				// 		$state.go('app.umroh.index');

				// 		$rootScope.modalHeader  = 'Pesan';
				// 		$rootScope.modalMessage = response.data.message;

				// 		$('#indexModal').modal('show');
				// 	});
				// } else {
					$state.go('app.umroh.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// }
			});
		}
	}

	$scope.init();
});