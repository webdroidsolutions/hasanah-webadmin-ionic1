app.controller('productCtrl', function($scope) {
	$scope.init = function() {

	}

	$scope.init();
});

app.controller('productIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.products = [];

	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
	        var q = $q.defer();
	        var data = {};

	        if ($scope.status != 99) {
		        data.status = $scope.status;
	        }

	        RequestService.getbystatus(data, 'product').then(function(data) {
				q.resolve(data);
			});

	        return q.promise;
	    }).withOption('createdRow', createdRow);

	    $scope.dtColumns = [
	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Nama').renderWith(detailHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Gambar').notSortable().renderWith(imageLink),
	        DTColumnBuilder.newColumn('category').withTitle('Kategori'),
	        DTColumnBuilder.newColumn('description').withTitle('Deskripsi'),
	        DTColumnBuilder.newColumn(null).withTitle('Status').renderWith(statusHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
	    ];

	    $scope.dtInstance = {};

	    function createdRow(row, data, dataIndex) {
	    	$compile(angular.element(row).contents())($scope);
	    }

	    function numberHtml(data, type, full, meta) {
	    	return meta.row + 1;
	    }

	    function detailHtml(data, type, full, meta) {
	        return '<a href="" ng-click="navigateTo(\'app.product.detail\', products[' + meta.row + '])">' + data.name + '</a>';
		}

		function statusHtml(data, type, full, meta) {
	    	var status;

	    	if (data.status) {
	    		status = '<span class="status-active">Aktif</span>';
	    	} else {
	    		status = '<span class="status-nonactive">Non-Aktif</span>';
	    	}

	        return status;
		}

	    function imageHtml(data, type, full, meta) {
	        return '<img src="data:image;base64,' + data.img + '" width="55px">';
		}
	    function imageLink(data, type, full, meta) {
	    	return '<img src="' + RequestService.url.replace('adm/', '') + data.img + '" width="55px">';
		}

		// function timeHtml(data, type, full, meta) {
		// 	var date = new Date(data.time);

		// 	return TimeService.days[date.getDay()] + ', ' 
		// 		+ ('0' + date.getDate()).slice(-2) + ' '
		// 		+ TimeService.months[date.getMonth()] + ' '
		// 		+ date.getFullYear() + ' '
		// 		+ ('0' + date.getHours()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2);
		// }

		function actionHtml(data, type, full, meta) {
			$scope.products[meta.row] = data;

			return '\
				<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.product.edit\', products[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
				<button class="btn btn-danger btn-xs btn-block" ng-click="destroyProduct(products[' + meta.row + '].id, products[' + meta.row + '].img)"><i class="fa fa-trash-o"></i> Delete</button>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			product: objectData
		});
	}

	$scope.destroyProduct = function(id, old_img) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id, 'old_img' : old_img };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'product').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('productDetailCtrl', function($scope, $stateParams, $state) {
	$scope.init = function() {
		if ($stateParams.product) { 
			$scope.product = $stateParams.product;

			if ($scope.product.status) {
				$scope.product.status_ = 'Aktif';
			} else {
				$scope.product.status_ = 'Non-Aktif';
			}
		} else $state.go('app.product.index');
	}

	$scope.init();
});

app.controller('productCreateCtrl', function($filter,$scope, $state, $rootScope, RequestService) {
	$scope.init = function() {
		$scope.errorName = false;
		$scope.errorImg = false;
		$scope.errorDescription = false;
		$scope.errorTravelagency = false;
		$scope.errorCategory = false;
		$scope.errorQuad = false;
		$scope.errorDepartDate = false;
		$scope.data={};  

		var today=new Date();
		// var today = Date.parse($filter('gmISODate')(new Date()))
		$scope.activeDate;
		$scope.selectedDates = [today];
		$scope.type = 'individual';
		
		$scope.removeFromSelected = function(dt) {
			$scope.selectedDates.splice($scope.selectedDates.indexOf(dt), 1);
		}

		$scope.obj={};
		$scope.status = true;

		$scope.categories = [{"name": "Produk Dana"}, {"name": "Produk Pembiayaan"}];

	}

	$scope.isActive=function(date){
		var found=false;
		for (var i = 0; i < $scope.selectedDates.length; i++) {
			if ($scope.selectedDates[i]==date) 
			{
				found=true;
				return true;
			}
		}
		return found;
	};


	$scope.insertProduct = function() {
		var check = true;

		$scope.errorName = false;
		$scope.errorImg = false;
		$scope.errorDescription = false;
		$scope.errorCategory = false;
		$scope.errorMeca = false;

		if (!$scope.name) {
			$scope.errorName = true;
			check = false;
		}

		if (!$scope.imgFile) {
			$scope.errorImg = true;
			check = false;
		}

		if (!$scope.description) {
			$scope.errorDescription = true;
			check = false;
		}

		if (!$scope.category) {
			$scope.errorCategory = true;
			check = false;
		}

		if (check) {
			var date = new Date();
			var status;

			if ($scope.status) {
				status = 1;
			} else {
				status = 0;
			}

			var data = {
				'name': $scope.name,
				'img': $scope.imgFile.base64,
	            'description': $scope.description,
	            'category': $scope.category,
	            'content': $scope.content,
	            'status': status
			}

			RequestService.save(data, 'product').then(function(response) {
				// RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
					$state.go('app.product.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// });
			});
		}
	}

	$scope.init();
});

app.controller('productEditCtrl', function($scope, $stateParams, $state, $rootScope, RequestService, $filter) {
	$scope.init = function() {
		if ($stateParams.product) { 
			$scope.product = $stateParams.product;

			$scope.errorName = false;
			$scope.errorDescription = false;
			$scope.errorDepartDate = false;
			//$scope.product.depart_date_list=new Date($scope.product.depart_date_list);
			$scope.vm={};
			$scope.vm.dateOptions = {
		       // maxDate: new Date(),
		       // dateDisabled: myDisabledDates
		   	}; 

			$scope.openDate= function() 
			{
			    $scope.data.openStart=true;
			};


			if ($scope.product.status) {
				$scope.status_ = true;
			} else {
				$scope.status_ = false;
			}

			$scope.categories = [{"name": "Produk Dana"}, {"name": "Produk Pembiayaan"}];
		} else $state.go('app.product.index');
	}

	$scope.updateProduct = function() {
		var check = true;

		$scope.errorName = false;
		$scope.errorDescription = false;

		if (!$scope.product.name) {
			$scope.errorName = true;
			check = false;
		}

		if (!$scope.product.description) {
			$scope.errorDescription = true;
			check = false;
		}

		if (check) {
			var img = '';
			var old_img = '';
			var status;

			if ($scope.imgFile) {
				img = $scope.imgFile.base64;
				old_img = $scope.product.img;
			} else {
				img = $scope.product.img;
			}

			if ($scope.status_) {
				status = 1;
			} else {
				status = 0;
			}

			var data = {
				'id': $scope.product.id,
				'name': $scope.product.name,
				'img': img,
				'old_img': old_img,
	            'description': $scope.product.description,
	            'category': $scope.product.category,
	            'content': $scope.product.content,
	            'status': status
			}

			RequestService.save(data, 'product').then(function(response) {
				// if ($scope.imgFile) {
				// 	RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
				// 		$state.go('app.product.index');

				// 		$rootScope.modalHeader  = 'Pesan';
				// 		$rootScope.modalMessage = response.data.message;

				// 		$('#indexModal').modal('show');
				// 	});
				// } else {
					$state.go('app.product.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// }
			});
		}
	}

	$scope.init();
});