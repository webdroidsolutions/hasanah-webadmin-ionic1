app.controller('propertyRegisterCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.users = [];
		$scope.getArray = [];
		$scope.emptyCsv = true;

		$('#daterange').daterangepicker({
			autoUpdateInput: false,
			locale: {
				cancelLabel: 'Clear'
			}
		});

		$('#daterange').on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});

		$('#daterange').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));

			var data = {
				date_start: picker.startDate.format('YYYY-MM-DD'),
				date_end: picker.endDate.format('YYYY-MM-DD')
			}

			RequestService.getreport(data, 'propertyregister').then(function(data) {
			// RequestService.getall('propertyregister').then(function(data) {
				if (data.length > 0) {
					for (var ii = 0; ii < data.length; ii++) {
						$scope.getArray.push({
							a: ii+1, 
							b: data[ii].propertyregister.name,
							c: data[ii].propertyregister.date,
							d: data[ii].propertyregister.gender,
							e: data[ii].propertyregister.address,
							f: data[ii].propertyregister.phone, 
							g: data[ii].property.name, 
							h: data[ii].branch.name
						});
					}

					$scope.emptyCsv = false;
				} else {
					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = 'Data tidak tersedia';

					$('#indexModal').modal('show');					

					$scope.emptyCsv = true;
				}
			});
		});

		$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
			var q = $q.defer();
			var data = {};

	        // if ($scope.status != 99) {
		       //  data.status = $scope.status;
	        // }

	        RequestService.getall('propertyregister').then(function(data) {
	        	q.resolve(data);
	        });

	        return q.promise;
	    }).withOption('createdRow', createdRow);

		$scope.dtColumns = [
		DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
		DTColumnBuilder.newColumn('propertyregister.name').withTitle('Nama'),
		DTColumnBuilder.newColumn(null).withTitle('Tanggal').renderWith(timeHtml),
		DTColumnBuilder.newColumn('propertyregister.gender').withTitle('Jenis Kelamin'),
		DTColumnBuilder.newColumn('propertyregister.address').withTitle('Alamat'),
		DTColumnBuilder.newColumn('propertyregister.phone').withTitle('No. Telpon'),
		DTColumnBuilder.newColumn('property.name').withTitle('Property'),
		DTColumnBuilder.newColumn('branch.name').withTitle('Cabang Terdekat'),
        // DTColumnBuilder.newColumn(null).withTitle('Status').renderWith(statusHtml),
        // DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
        ];

        $scope.dtInstance = {};

        function createdRow(row, data, dataIndex) {
        	$compile(angular.element(row).contents())($scope);
        }

        function numberHtml(data, type, full, meta) {
        	return meta.row + 1;
        }

		// function detailHtml(data, type, full, meta) {
		//     return '<a href="" ng-click="navigateTo(\'app.settingUser.detail\', users[' + meta.row + '])">' + data.name + '</a>';
		// }

		// function statusHtml(data, type, full, meta) {
		// 	var status;

		// 	if (data.status) {
		// 		status = '<span class="status-active">Aktif</span>';
		// 	} else {
		// 		status = '<span class="status-nonactive">Non-Aktif</span>';
		// 	}

		//     return status;
		// }

		//    function imageHtml(data, type, full, meta) {
		//        return '<img src="data:image;base64,' + data.img + '" width="55px">';
		// }

		function timeHtml(data, type, full, meta) {
			var date = new Date(data.propertyregister.date);

			return TimeService.days[date.getDay()] + ', ' 
			+ ('0' + date.getDate()).slice(-2) + ' '
			+ TimeService.months[date.getMonth()] + ' '
			+ date.getFullYear() + ' '
			+ ('0' + date.getHours()).slice(-2) + ':'
			+ ('0' + date.getMinutes()).slice(-2) + ':'
			+ ('0' + date.getMinutes()).slice(-2);
		}

		function actionHtml(data, type, full, meta) {
			$scope.users[meta.row] = data;

			return '\
			<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.settingUser.edit\', users[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
			<button class="btn btn-danger btn-xs btn-block" ng-click="destroyUser(users[' + meta.row + '].id)"><i class="fa fa-trash-o"></i> Delete</button>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			user: objectData
		});
	}

	$scope.destroyUser = function(id) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'user').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});