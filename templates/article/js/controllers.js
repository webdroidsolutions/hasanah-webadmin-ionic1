app.controller('articleCategoryIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.categories = [];

	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
	        var q = $q.defer();

	        RequestService.getall('articlecategory').then(function(data) {
				q.resolve(data);
			});

	        return q.promise;
	    }).withOption('createdRow', createdRow);

	    $scope.dtColumns = [
	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
	        DTColumnBuilder.newColumn('name').withTitle('Nama'),
	        DTColumnBuilder.newColumn(null).withTitle('Icon').notSortable().renderWith(imageLink),
	        DTColumnBuilder.newColumn('description').withTitle('Deskripsi'),
	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
	    ];

	    $scope.dtInstance = {};

	    function createdRow(row, data, dataIndex) {
	    	$compile(angular.element(row).contents())($scope);
	    }

	    function numberHtml(data, type, full, meta) {
	    	return meta.row + 1;
	    }

	    function imageHtml(data, type, full, meta) {
	        return '<img src="data:image;base64,' + data.img + '" width="50px">';
		}
		 function imageLink(data, type, full, meta) {
	    	return '<img src="' + RequestService.url.replace('adm/', '') + data.img + '" width="55px">';
		}

		function actionHtml(data, type, full, meta) {
			$scope.categories[meta.row] = data;

			return '\
				<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.article.category.edit\', categories[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
				<button class="btn btn-danger btn-xs btn-block" ng-click="destroyCategory(categories[' + meta.row + '].id, categories[' + meta.row + '].img)"><i class="fa fa-trash-o"></i> Delete</button>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			category: objectData
		});
	}

	$scope.destroyCategory = function(id, old_img) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id, 'old_img' : old_img };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'articlecategory').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('articleCategoryCreateCtrl', function($scope, $state, $rootScope, RequestService) {
	$scope.init = function() {
		$scope.errorName = false;
		$scope.errorDescription = false;
	}

	$scope.insertCategory = function() {
		var check = true;

		$scope.errorName = false;
		$scope.errorDescription = false;
		$scope.errorImg = false;

		if (!$scope.name) {
			$scope.errorName = true;
			check = false;
		}

		if (!$scope.description) {
			$scope.errorDescription = true;
			check = false;
		}

		if (!$scope.imgFile) {
			$scope.errorImg = true;
			check = false;
		}

		if (check) {
			var data = {
	            'name': $scope.name,
	            'description': $scope.description,
				'img': $scope.imgFile.base64
			};

			RequestService.save(data, 'articlecategory').then(function(response) {
				// RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
					$state.go('app.article.category.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// });
			});
		}
	}

	$scope.init();
});

app.controller('articleCategoryEditCtrl', function($scope, $state, $stateParams, $rootScope, RequestService) {
	$scope.init = function() {
		if ($stateParams.category) {
			$scope.category = $stateParams.category;

			$scope.errorName = false;
			$scope.errorDescription = false;
		} else $state.go('app.article.category.index');
	}

	$scope.updateCategory = function() {
		var check = true;

		$scope.errorName = false;
		$scope.errorDescription = false;

		if (!$scope.category.name) {
			$scope.errorName = true;
			check = false;
		}

		if (!$scope.category.description) {
			$scope.errorDescription = true;
			check = false;
		}

		if (check) {
			var img = '';
			var old_img = '';

			if ($scope.imgFile) {
				img = $scope.imgFile.base64;
				old_img = $scope.category.img;
			} else {
				img = $scope.category.img;
			}

			var data = {
				'id': $scope.category.id,
	            'name': $scope.category.name,
	            'description': $scope.category.description,
				'img': img,
				'old_img': old_img
			};

			RequestService.save(data, 'articlecategory').then(function(response) {
				// if ($scope.imgFile) {
				// 	RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
				// 		$state.go('app.article.category.index');

				// 		$rootScope.modalHeader  = 'Pesan';
				// 		$rootScope.modalMessage = response.data.message;

				// 		$('#indexModal').modal('show');
				// 	});
				// } else {
					$state.go('app.article.category.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// }
			});
		}
	}

	$scope.init();
});

app.controller('articleSubCategoryIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.subCategory = [];

	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
	        var q = $q.defer();

	        RequestService.getall('articlesubcategory').then(function(data) {
				q.resolve(data);
			});

	        return q.promise;
	    }).withOption('createdRow', createdRow);

	    $scope.dtColumns = [
	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
	        DTColumnBuilder.newColumn('name').withTitle('Nama'),
	        DTColumnBuilder.newColumn('name_article_category').withTitle('Kategori'),
	        DTColumnBuilder.newColumn(null).withTitle('Ikon').notSortable().renderWith(imageLink),
	        DTColumnBuilder.newColumn('description').withTitle('Deskripsi'),
	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
	    ];

	    $scope.dtInstance = {};

	    function createdRow(row, data, dataIndex) {
	    	$compile(angular.element(row).contents())($scope);
	    }

	    function numberHtml(data, type, full, meta) {
	    	return meta.row + 1;
	    }

	    function imageHtml(data, type, full, meta) {
	        return '<img src="data:image;base64,' + data.img + '" width="50px">';
		}
		 function imageLink(data, type, full, meta) {
	    	return '<img src="' + RequestService.url.replace('adm/', '') + data.img + '" width="55px">';
		}

		function actionHtml(data, type, full, meta) {
			$scope.subCategory[meta.row] = data;

			return '\
				<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.article.subCategory.edit\', subCategory[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
				<button class="btn btn-danger btn-xs btn-block" ng-click="destroySubCategory(subCategory[' + meta.row + '].id, subCategory[' + meta.row + '].img)"><i class="fa fa-trash-o"></i> Delete</button>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			subCategory: objectData
		});
	}

	$scope.destroySubCategory = function(id, old_img) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id, 'old_img' : old_img };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'articlesubcategory').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('articleSubCategoryCreateCtrl', function($scope, $state, $rootScope, RequestService) {
	$scope.init = function() {
		$scope.categories = [];

		$scope.errorName = false;
		$scope.errorCategory = false;
		$scope.errorDescription = false;
		$scope.errorImg = false;

		RequestService.getall('articlecategory').then(function(categories) {
			for (var ii = 0; ii < categories.length; ii++) {
				$scope.categories.push(categories[ii]);
			}
		});
	}

	$scope.insertSubCategory = function() {
		var check = true;

		$scope.errorName = false;
		$scope.errorCategory = false;
		$scope.errorDescription = false;
		$scope.errorImg = false;

		if (!$scope.name) {
			$scope.errorName = true;
			check = false;
		}

		if (!$scope.category) {
			$scope.errorCategory = true;
			check = false;
		}

		if (!$scope.description) {
			$scope.errorDescription = true;
			check = false;
		}

		if (!$scope.imgFile) {
			$scope.errorImg = true;
			check = false;
		}

		if (check) {
			var data = {
	            'name': $scope.name,
	            'id_article_category': $scope.category,
	            'description': $scope.description,
				'img': $scope.imgFile.base64
			};

			RequestService.save(data, 'articlesubcategory').then(function(response) {
				// RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
					$state.go('app.article.subCategory.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// });
			});
		}
	}

	$scope.init();
});

app.controller('articleSubCategoryEditCtrl', function($scope, $state, $stateParams, $rootScope, RequestService) {
	$scope.init = function() {
		if ($stateParams.subCategory) {
			$scope.subCategory = $stateParams.subCategory;

			$scope.categories = [];

			$scope.errorName = false;
			$scope.errorCategory = false;
			$scope.errorDescription = false;

			RequestService.getall('articlecategory').then(function(categories) {
				for (var ii = 0; ii < categories.length; ii++) {
					$scope.categories.push(categories[ii]);
				}
			});
		} else $state.go('app.article.subCategory.index');
	}

	$scope.updateSubCategory = function() {
		var check = true;

		$scope.errorName = false;
		$scope.errorCategory = false;
		$scope.errorDescription = false;

		if (!$scope.subCategory.name) {
			$scope.errorName = true;
			check = false;
		}

		if (!$scope.subCategory.id_article_category) {
			$scope.errorCategory = true;
			check = false;
		}

		if (!$scope.subCategory.description) {
			$scope.errorDescription = true;
			check = false;
		}

		if (check) {
			var img = '';
			var old_img = '';

			if ($scope.imgFile) {
				img = $scope.imgFile.base64;
				old_img = $scope.subCategory.img;
			} else {
				img = $scope.subCategory.img;
			}

			var data = {
				'id': $scope.subCategory.id,
	            'name': $scope.subCategory.name,
	            'description': $scope.subCategory.description,
	            'id_article_category': $scope.subCategory.id_article_category,
				'img': img,
				'old_img': old_img
			};

			RequestService.save(data, 'articlesubcategory').then(function(response) {
				// if ($scope.imgFile) {
				// 	RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
				// 		$state.go('app.article.subCategory.index');

				// 		$rootScope.modalHeader  = 'Pesan';
				// 		$rootScope.modalMessage = response.data.message;

				// 		$('#indexModal').modal('show');
				// 	});
				// } else {
					$state.go('app.article.subCategory.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// }
			});
		}
	}

	$scope.init();
});

// app.controller('articleListIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
// 	$scope.init = function() {
// 		$scope.articles = [];

// 	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
// 	        var q = $q.defer();
// 	        var data = {};

// 	        if ($scope.status != 99) {
// 		        data.status = $scope.status;
// 	        }

// 	        RequestService.getbystatus(data, 'article').then(function(data) {
// 				q.resolve(data);
// 			});

// 	        return q.promise;
// 	    }).withOption('createdRow', createdRow);

// 	    $scope.dtColumns = [
// 	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
// 	        DTColumnBuilder.newColumn(null).withTitle('Judul').renderWith(detailHtml),
// 	        DTColumnBuilder.newColumn('name_article_sub_category').withTitle('Sub Kategori'),
// 	        DTColumnBuilder.newColumn(null).withTitle('Gambar').notSortable().renderWith(imageHtml),
// 	        DTColumnBuilder.newColumn('author').withTitle('Pembuat'),
// 	        DTColumnBuilder.newColumn(null).withTitle('Waktu').renderWith(timeHtml),
// 	        DTColumnBuilder.newColumn('description').withTitle('Deskripsi Singkat'),
// 	        // DTColumnBuilder.newColumn('headline').withTitle('Headline'),
// 	        DTColumnBuilder.newColumn(null).withTitle('Status').renderWith(statusHtml),
// 	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
// 	    ];

// 	    $scope.dtInstance = {};

// 	    function createdRow(row, data, dataIndex) {
// 	    	$compile(angular.element(row).contents())($scope);
// 	    }

// 	    function numberHtml(data, type, full, meta) {
// 	    	return meta.row + 1;
// 	    }

// 	    function detailHtml(data, type, full, meta) {
// 	        return '<a href="" ng-click="navigateTo(\'app.article.list.detail\', articles[' + meta.row + '])">' + data.title + '</a>';
// 		}

// 		function statusHtml(data, type, full, meta) {
// 	    	var status;

// 	    	if (data.status) {
// 	    		status = '<span class="status-active">Aktif</span>';
// 	    	} else {
// 	    		status = '<span class="status-nonactive">Non-Aktif</span>';
// 	    	}

// 	        return status;
// 		}

// 	    function imageHtml(data, type, full, meta) {
// 	        return '<img src="data:image;base64,' + data.img + '" width="100px">';
// 		}

// 		function timeHtml(data, type, full, meta) {
// 			var date = new Date(data.time);

// 			return TimeService.days[date.getDay()] + ', ' 
// 				+ ('0' + date.getDate()).slice(-2) + ' '
// 				+ TimeService.months[date.getMonth()] + ' '
// 				+ date.getFullYear() + ' '
// 				+ ('0' + date.getHours()).slice(-2) + ':'
// 				+ ('0' + date.getMinutes()).slice(-2) + ':'
// 				+ ('0' + date.getMinutes()).slice(-2);
// 		}

// 		function actionHtml(data, type, full, meta) {
// 			$scope.articles[meta.row] = data;

// 			return '\
// 				<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.article.list.edit\', articles[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
// 				<button class="btn btn-danger btn-xs btn-block" ng-click="destroyArticle(articles[' + meta.row + '].id)"><i class="fa fa-trash-o"></i> Delete</button>\
// 			';
// 		}
// 	}

// 	$scope.reloadData = function() {
// 		$scope.dtInstance.reloadData();
// 	}

// 	$scope.navigateTo = function(targetPage, objectData) {
// 		$state.go(targetPage, {
// 			article: objectData
// 		});
// 	}

// 	$scope.destroyArticle = function(id) {
// 		$rootScope.confirmHeader  = 'Konfirmasi';
// 		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
// 		$rootScope.confirmButton  = 'Hapus';

// 		$rootScope.modalAction = function() {
// 			var data = { 'id' : id };

// 			$('#indexConfirm').modal('hide');
			
// 			RequestService.delete(data, 'article').then(function(response) {
// 				$scope.reloadData();
// 			});
// 		}

// 		$('#indexConfirm').modal('show');
// 	}

// 	$scope.init();
// });

app.controller('articleListIndexCtrl', function($scope, $rootScope, $state, TimeService, RequestService) {
	$scope.articles = [];
	$scope.pages = [];
	$scope.last;
	$scope.limit = 10;
	$scope.currentPage = 1;
	$scope.baseUrl = RequestService.url.replace('adm/', '');
	var data = {};

	$scope.init = function() {
        // if ($scope.status != 99) {
	       //  data.status = $scope.status;
        // }

		RequestService.getbypaging(data, 'article', $scope.currentPage, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.articles.push(data[ii]);
			};
		});

		RequestService.gettotal(data, 'article').then(function(data) {
			for (var ii = 1; ii <= Math.ceil(data / $scope.limit); ii++) {
				$scope.pages.push(ii);
			};

			$scope.last = $scope.pages.length;
		});
	};

	$scope.timeHtml = function(data) {
		var date = new Date(data);

		return TimeService.days[date.getDay()] + ', ' 
			+ ('0' + date.getDate()).slice(-2) + ' '
			+ TimeService.months[date.getMonth()] + ' '
			+ date.getFullYear() + ' '
			+ ('0' + date.getHours()).slice(-2) + ':'
			+ ('0' + date.getMinutes()).slice(-2) + ':'
			+ ('0' + date.getMinutes()).slice(-2);
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			article: objectData
		});
	}

	$scope.gotoPage = function(page) {
		$scope.articles = [];
		$scope.currentPage = page;

		RequestService.getbypaging(data, 'article', page, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.articles.push(data[ii]);
			};
		});
	}

	$scope.reloadData = function() {
		$scope.articles = [];
		$scope.pages = [];

		if ($scope.status != 99) {
	        data.status = $scope.status;
        } else {
        	data = {};
        }

		RequestService.getbypaging(data, 'article', $scope.currentPage, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.articles.push(data[ii]);
			};
		});

		RequestService.gettotal(data, 'article').then(function(data) {
			for (var ii = 1; ii <= Math.ceil(data / $scope.limit); ii++) {
				$scope.pages.push(ii);
			};

			$scope.last = $scope.pages.length;
		});
	}

	$scope.destroyArticle = function(id, old_img) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id, 'old_img' : old_img };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'article').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('articleListDetailCtrl', function($scope, $stateParams, $state, RequestService) {
	$scope.baseUrl = RequestService.url.replace('adm/', '');
	$scope.init = function() {
		if ($stateParams.article) { 
			$scope.article = $stateParams.article;

			var data = { id: $scope.article.id_article_sub_category };

			RequestService.getby(data, 'articlesubcategory', 'id').then(function(subCategory) {
				$scope.subCategory = subCategory;
			});

			if ($scope.article.status) {
				$scope.article.status_ = 'Aktif';
			} else {
				$scope.article.status_ = 'Non-Aktif';
			}
		} else $state.go('app.article.list.index');
	}

	$scope.init();
});

app.controller('articleListCreateCtrl', function($scope, $state, $rootScope, RequestService) {
	$scope.init = function() {
		$scope.subCategories = [];

		$scope.errorTitle = false;
		$scope.errorSubCategory = false;
		$scope.errorImg = false;
		$scope.errorDescription = false;

		$scope.headline = false;

		RequestService.getall('articlesubcategory').then(function(subCategories) {
			for (var ii = 0; ii < subCategories.length; ii++) {
				$scope.subCategories.push(subCategories[ii]);
			}
		});

		$scope.status = true;
	}

	$scope.insertArticle = function() {
		var check = true;

		$scope.errorTitle = false;
		$scope.errorSubCategory = false;
		$scope.errorImg = false;
		$scope.errorDescription = false;

		if (!$scope.title) {
			$scope.errorTitle = true;
			check = false;
		}

		if (!$scope.subCategory) {
			$scope.errorSubCategory = true;
			check = false;
		}

		if (!$scope.imgFile) {
			$scope.errorImg = true;
			check = false;
		}

		if (!$scope.description) {
			$scope.errorDescription = true;
			check = false;
		}

		if (check) {
			var date = new Date();
			var status;

			if ($scope.status) {
				status = 1;
			} else {
				status = 0;
			}

			var data = {
				'img': $scope.imgFile.base64,
	            'title': $scope.title,
	            'id_article_sub_category': $scope.subCategory,
	            'time': date,
	            'author': 'Fullan',
	            'headline': $scope.headline,
	            'description': $scope.description,
	            'content': $scope.content,
	            'status': status
			};

			RequestService.save(data, 'article').then(function(response) {
				// RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
					$state.go('app.article.list.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// });
			});
		}
	}

	$scope.init();
});

app.controller('articleListEditCtrl', function($scope, $state, $stateParams, $rootScope, RequestService) {
	$scope.init = function() {
		if ($stateParams.article) {
			$scope.article = $stateParams.article;

			$scope.subCategories = [];

			$scope.errorTitle = false;
			$scope.errorSubCategory = false;
			$scope.errorDescription = false;

			RequestService.getall('articlesubcategory').then(function(subCategories) {
				for (var ii = 0; ii < subCategories.length; ii++) {
					$scope.subCategories.push(subCategories[ii]);
				}
			});
			console.log($scope.article);
			console.log($scope.subCategories);

			if ($scope.article.status) {
				$scope.status_ = true;
			} else {
				$scope.status_ = false;
			}
		} else {
			$state.go('app.article.list.index');
		}
	}

	$scope.updateArticle = function() {
		var check = true;

		$scope.errorTitle = false;
		$scope.errorSubCategory = false;
		$scope.errorDescription = false;

		if (!$scope.article.title) {
			$scope.errorTitle = true;
			check = false;
		}

		if (!$scope.article.id_article_sub_category) {
			$scope.errorSubCategory = true;
			check = false;
		}

		if (!$scope.article.description) {
			$scope.errorDescription = true;
			check = false;
		}

		if (check) {
			var img = '';
			var old_img = '';
			var status;

			if ($scope.imgFile) {
				img = $scope.imgFile.base64;
				old_img = $scope.article.img;
			} else {
				img = $scope.article.img;
			}

			if ($scope.status_) {
				status = 1;
			} else {
				status = 0;
			}

			var data = {
				'id': $scope.article.id,
				'img': img,
				'old_img': old_img,
	            'title': $scope.article.title,
	            'id_article_sub_category': $scope.article.id_article_sub_category,
	            'time': $scope.article.time,
	            'author': $scope.article.author,
	            'headline': $scope.article.headline,
	            'description': $scope.article.description,
	            'content': $scope.article.content,
	            'status': status
			};

			RequestService.save(data, 'article').then(function(response) {
				// if ($scope.imgFile) {
				// 	RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
				// 		$state.go('app.article.list.index');

				// 		$rootScope.modalHeader  = 'Pesan';
				// 		$rootScope.modalMessage = response.data.message;

				// 		$('#indexModal').modal('show');
				// 	});
				// } else {
					$state.go('app.article.list.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// }
			});
		}
	}

	$scope.init();
});