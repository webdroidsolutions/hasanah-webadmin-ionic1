app.controller('umrohRegisterCtrl', function($scope, $uibModal, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	var modalInstance;
	$scope.init = function() {
		$scope.users = [];
		$scope.getArray = [];
		$scope.emptyCsv = true;

		$('#daterange').daterangepicker({
			autoUpdateInput: false,
			locale: {
				cancelLabel: 'Clear'
			}
		});

		$('#daterange').on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});

		$('#daterange').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));

			var data = {
				date_start: picker.startDate.format('YYYY-MM-DD'),
				date_end: picker.endDate.format('YYYY-MM-DD')
			}

			RequestService.getreport(data, 'umrohregister').then(function(data) {
			// RequestService.getall('umrohregister').then(function(data) {
				if (data.length > 0) {
					for (var ii = 0; ii < data.length; ii++) {
						$scope.getArray.push({
							a: ii+1, 
							b: data[ii].umrohregister.name,
							c: data[ii].umrohregister.date,
							d: data[ii].umrohregister.gender,
							e: data[ii].umrohregister.address,
							f: data[ii].umrohregister.phone, 
							g: data[ii].umroh.name, 
							h: data[ii].branch.name
						});
					}

					$scope.emptyCsv = false;
				} else {
					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = 'Data tidak tersedia';

					$('#indexModal').modal('show');					

					$scope.emptyCsv = true;
				}
			});
		});

		$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
			var q = $q.defer();
			var data = {};

	        // if ($scope.status != 99) {
		       //  data.status = $scope.status;
	        // }

	        RequestService.getall('umrohregister').then(function(data) {
	        	q.resolve(data);
	        });

	        return q.promise;
	    }).withOption('createdRow', createdRow);

		$scope.dtColumns = [
		DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
		DTColumnBuilder.newColumn('umrohregister.order_id').withTitle('Order'),
		DTColumnBuilder.newColumn('umrohregister.name').withTitle('Nama'),
		DTColumnBuilder.newColumn(null).withTitle('Tanggal').renderWith(timeHtml),
		DTColumnBuilder.newColumn('umrohregister.company_name').withTitle('Company'),
		DTColumnBuilder.newColumn('umrohregister.tracking_id').withTitle('Tracking No.'),
		DTColumnBuilder.newColumn('umrohregister.address').withTitle('Alamat'),
		DTColumnBuilder.newColumn('umrohregister.phone').withTitle('No. Telpon'),
        //DTColumnBuilder.newColumn('umrohregister.email').withTitle('Email'),
        DTColumnBuilder.newColumn('umroh.name').withTitle('Umroh'),
        //DTColumnBuilder.newColumn('branch.name').withTitle('Cabang Terdekat'),
        DTColumnBuilder.newColumn('umrohregister.order_type').withTitle('Order Type'),
        DTColumnBuilder.newColumn(null).withTitle('Payment').renderWith(paymentHtml),
        DTColumnBuilder.newColumn(null).withTitle('Status').renderWith(statusHtml),
        DTColumnBuilder.newColumn(null).withTitle('Action').renderWith(actionHtml),
        // DTColumnBuilder.newColumn(null).withTitle('Status').renderWith(statusHtml),
        // DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
        ];

        $scope.dtInstance = {};

        function createdRow(row, data, dataIndex) {
        	$compile(angular.element(row).contents())($scope);
        }

        function numberHtml(data, type, full, meta) {
        	return meta.row + 1;
        }

        function timeHtml(data, type, full, meta) {
        	var date = new Date(data.umrohregister.date);

        	return TimeService.days[date.getDay()] + ', ' 
        	+ ('0' + date.getDate()).slice(-2) + ' '
        	+ TimeService.months[date.getMonth()] + ' '
        	+ date.getFullYear() + ' '
        	+ ('0' + date.getHours()).slice(-2) + ':'
        	+ ('0' + date.getMinutes()).slice(-2) + ':'
        	+ ('0' + date.getMinutes()).slice(-2);
        }

        function statusHtml(data, type, full, meta) {
        	$scope.users[meta.row] = data;
        	if (data.umrohregister.status) 
        	{
        		data.umrohregister.status=parseInt(data.umrohregister.status);
        		switch(data.umrohregister.status){
        			case 1 :
        			return "Pending";
        			break;
        			case 2 :
        			return "Paid";
        			break;
        			case 3 :
        			return "On Delivery";
        			break;
        			case 4 :
        			return "Waiting";
        			break;
        			case 5 :
        			return "Done";
        			break;
        			case 6 :
        			return "Cancel";
        			break;
        			case 7 :
        			return "Refund";
        			break;
        			default : return "NA";
        		}
        	}
        	else return "NA";
        };

        function paymentHtml(data, type, full, meta) {
        	$scope.users[meta.row] = data;
        	var html;
        	if (data.umrohregister.status==1 || data.umrohregister.status==6) 
        	{
        		html='\
        		<i class="glyphicon glyphicon-remove" style="color:red"></i>\
        		';
        	}
        	else
        	{
        		html='\
        		<i class="glyphicon glyphicon-ok" style="color:green"></i>\
        		'
        	}
        	return html;
        };

        function actionHtml(data, type, full, meta) {
        	$scope.users[meta.row] = data;
        	var html=" ";
			/*var html='\
				<button class="btn btn-primary btn-xs btn-block" ng-click="update(3)">Mark for Delivery</button>\
				<button class="btn btn-success btn-xs btn-block" ng-click="update(4)">Mark for Pickup</button>\
				<button class="btn btn-warning btn-xs btn-block" ng-click="update(7)">Refund</button>\
				<button class="btn btn-danger btn-xs btn-block" ng-click="update(6)">Cancel</button>\
				';*/
			//<button class="btn btn-danger btn-xs btn-block" ng-click="destroyUser(users[' + meta.row + '].id)"><i class="fa fa-trash-o"></i> Delete</button>\

			if (data.umrohregister.status) 
			{
				data.umrohregister.status=parseInt(data.umrohregister.status);
				switch(data.umrohregister.status){
					case 1 :
					html+='\
					<button class="btn btn-danger btn-xs btn-block" ng-click="update(6,users[' + meta.row + '].umrohregister.order_id)">Cancel</button>\
					'
					break;
					case 2 :
					html+='\
					<button class="btn btn-warning btn-xs btn-block" ng-click="update(7,users[' + meta.row + '].umrohregister.order_id)">Refund</button>\
					';
					if (data.umrohregister.order_type=='send') 
					{
						html+='\
						<button class="btn btn-primary btn-xs btn-block" ng-click="update(3,users[' + meta.row + '].umrohregister.order_id)">Mark for Delivery</button>\
						'
					}
					if (data.umrohregister.order_type=='take')
					{
						html+='\
						<button class="btn btn-success btn-xs btn-block" ng-click="update(4,users[' + meta.row + '].umrohregister.order_id)">Mark for Pickup</button>\
						'
					}
					break;
					case 3 :
					html+='\
					<button class="btn btn-warning btn-xs btn-block" ng-click="update(7,users[' + meta.row + '].umrohregister.order_id)">Refund</button>\
					';
					break;
					case 4 :
					html+='\
					<button class="btn btn-warning btn-xs btn-block" ng-click="update(7,users[' + meta.row + '].umrohregister.order_id)">Refund</button>\
					';
					break;
					default : html+= "NA";
				}
			}
			else html+= "NA";
			return html;
		}
	};

	$scope.update=function(status,id)
	{
		if (!id) 
		{
			alert("Order Id is required..");
			return;
		}
		var data = { 'status' : status,'order_id':id };
		if (status==3) 
		{
			modalInstance = $uibModal.open({
				templateUrl: 'templates/umrohRegister/html/add-tracker.html',
				controller: 'UpdateTrackingCtrl',
				size: 'md',
				animation: true,
				resolve: {
					item: function () {
						return data;
					}
				}
			});
			return;
		}
		
		RequestService.update(data, 'umrohregister')
		.then(function(response) {
			console.log(JSON.stringify(data));
			/*$rootScope.modalHeader  = 'Pesan';
			$rootScope.modalMessage = response.data.message;

			$('#indexModal').modal('show');*/
			$scope.reloadData();
		});
	};

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			user: objectData
		});
	}

	$scope.createOrder = function() {
		$state.go('app.productList');
	}

	$scope.destroyUser = function(id) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'user').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.$on('umroh:updated', function(e)
	{
		$scope.reloadData();
	});

	$scope.init();

});


app.controller('UpdateTrackingCtrl', function($scope, $state, RequestService,$uibModalInstance,item,$rootScope) {
	$scope.obj={};
	$scope.menus;
	$scope.error=false;
	$scope.companyName=false;
	$scope.init = function() {
		$scope.$on('$viewContentLoaded', function(){
			$.AdminLTE.layout.activate();
		});
		$scope.obj=item;
	};

	$scope.update=function()
	{
		$scope.error=false;
		$scope.companyName=false;
		if (!$scope.obj.company_name) {
			$scope.companyName=true;
			return
		}

		if (!$scope.obj.tracking_id) {
			$scope.error=true;
			return
		}
		
		else
		{
			$scope.obj.tracking_id=$scope.obj.tracking_id.toUpperCase();
			$scope.obj.company_name=$scope.obj.company_name.toUpperCase();
			RequestService.update($scope.obj, 'umrohregister')
			.then(function(response) {
				$uibModalInstance.close();
				$rootScope.$broadcast('umroh:updated');
			});
		}
	};

	$scope.init();
})

app.controller('umrohProductListCtrl', function($scope, $stateParams, $state, RequestService, $rootScope) {
	$scope.baseUrl = RequestService.url.replace('adm/', '');
	$scope.init = function() {
		$scope.productsList = [];
		$scope.limit = 10;
		$scope.currentPage = 1;
		var data = {};

		RequestService.getbypaging(data, 'umroh', $scope.currentPage, $scope.limit).then(function(data) {
			for (var ii = 0; ii < data.length; ii++) {
				$scope.productsList.push(data[ii]);
			};
		});
		/*if ($stateParams.umroh) { 
			$scope.umroh = $stateParams.umroh;

			var departure = new Date($scope.umroh.departure);
			$scope.departure = departure.getMonth() + 1 + '/' + departure.getDate() + '/' + departure.getFullYear();

			if ($scope.umroh.status) {
				$scope.umroh.status_ = 'Aktif';
			} else {
				$scope.umroh.status_ = 'Non-Aktif';
			}
		} else $state.go('app.umroh.index');*/
	}

	$scope.init();
});