app.controller('calculatorCtrl', function($scope) {
	$scope.init = function() {

	}

	$scope.init();
});

app.controller('calculatorIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.finances = [];

	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
	        var q = $q.defer();

			RequestService.getall('finance').then(function(data) {
				console.log(data);
				q.resolve(data);
			});

			// RequestService.financeTest().then(function(data) {
			// 	q.resolve(data);
			// });

	        return q.promise;
	    }).withOption('createdRow', createdRow);

	    $scope.dtColumns = [
	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
	        DTColumnBuilder.newColumn('name').withTitle('Nama'),
	        DTColumnBuilder.newColumn(null).withTitle('Margin').notSortable().renderWith(marginHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
	    ];

	    $scope.dtInstance = {};

	    function createdRow(row, data, dataIndex) {
	    	$compile(angular.element(row).contents())($scope);
	    }

	    function numberHtml(data, type, full, meta) {
	    	return meta.row + 1;
	    }

		// function timeHtml(data, type, full, meta) {
		// 	var date = new Date(data.time);

		// 	return TimeService.days[date.getDay()] + ', ' 
		// 		+ ('0' + date.getDate()).slice(-2) + ' '
		// 		+ TimeService.months[date.getMonth()] + ' '
		// 		+ date.getFullYear() + ' '
		// 		+ ('0' + date.getHours()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2);
		// }

		function marginHtml(data, type, full, meta) {
			var margin_temp = '';

			for (var ii = 0; ii < data.rate.length; ii++) {
				margin_temp += data.rate[ii].job + '<br> DP: ' + data.rate[ii].dp + '%' + '<br> Margin: <br>';

				for (var jj = 0; jj < data.rate[ii].margin.length; jj++) {
					margin_temp += data.rate[ii].margin[jj].period + ' bulan: ' + data.rate[ii].margin[jj].margin + '% <br>';
				}

				margin_temp += '<br> <br>';
			};

			return margin_temp;
		}

		function actionHtml(data, type, full, meta) {
			$scope.finances[meta.row] = data;

			return '\
				<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.calculator.edit\', finances[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
				<button class="btn btn-danger btn-xs btn-block" ng-click="destroyCalculator(finances[' + meta.row + '].id)"><i class="fa fa-trash-o"></i> Delete</button>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			calculator: objectData
		});
	}

	$scope.destroyCalculator = function(id) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'finance').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('calculatorCreateCtrl', function($scope, $state, $rootScope, $compile, RequestService) {
	$scope.init = function() {
		$scope.errorName = false;
		$scope.errorRate = false;
	}

	$scope.addJob = function() {
		var number = $('#divMargin .divJob').length;

		var html = '\
			<div class="divJob">\
				<div class="form-group" id="groupJob'+number+'">\
					<label id="labelJob'+number+'" for="job'+number+'" class="col-sm-2 control-label">Jenis Pekerjaan / % DP</label>\
					\
					<div class="col-sm-4" id="inputJob'+number+'">\
						<input type="text" id="job'+number+'" class="form-control" placeholder="Input Jenis Pekerjaan">\
					</div>\
					\
					<div class="col-sm-4" id="inputDp'+number+'">\
						<input type="text" id="dp'+number+'" class="form-control" placeholder="Input % DP">\
					</div>\
					\
					<div class="col-sm-2">\
						<button class="btn btn-flat btn-danger" id="btn-remove" type="button">\
							<i class="fa fa-times"></i>\
						</button>\
					</div>\
				</div>\
				\
				<div class="divRate'+number+'">\
					<div class="form-group" id="groupRate'+number+'0">\
						<label id="labelRate'+number+'0" for="period'+number+'0" class="col-sm-offset-2 col-sm-2 control-label">% Margin</label>\
						\
						<div class="col-sm-3" id="inputRate'+number+'0">\
							<input type="text" id="period'+number+'0" class="form-control" placeholder="Input Period">\
						</div>\
						\
						<div class="col-sm-3" id="inputMargin'+number+'0">\
							<input type="text" id="margin'+number+'0" class="form-control" placeholder="Input % Margin">\
						</div>\
						\
						<div class="col-sm-2">\
							<button class="btn btn-flat btn-primary" id="btn-add" type="button" ng-click="addMargin('+number+')">\
								<i class="fa fa-plus"></i>\
							</button>\
						</div>\
					</div>\
				</div>\
			</div>';

		$('#divMargin').append($compile(html)($scope));

		$('.divJob').on('click', '#btn-remove', function(){
			$(this).parent().parent().parent().remove();
		});
	}

	$scope.addMargin = function(parentNumber) {
		var number = $('.divRate'+parentNumber+' .form-group').length;

		$('.divRate'+parentNumber).append('\
			<div class="form-group" id="groupRate'+parentNumber+''+number+'">\
				<label id="labelRate'+parentNumber+''+number+'" for="period'+parentNumber+''+number+'" class="col-sm-offset-2 col-sm-2 control-label">% Margin</label>\
				\
				<div class="col-sm-3" id="inputRate'+parentNumber+''+number+'">\
					<input type="text" id="period'+parentNumber+''+number+'" class="form-control" placeholder="Input Period">\
				</div>\
				\
				<div class="col-sm-3" id="inputMargin'+parentNumber+''+number+'">\
					<input type="text" id="margin'+parentNumber+''+number+'" class="form-control" placeholder="Input % Margin">\
				</div>\
				\
				<div class="col-sm-2">\
					<button class="btn btn-flat btn-danger" id="btn-remove" type="button">\
						<i class="fa fa-times"></i>\
					</button>\
				</div>\
			</div>\
		');

		$('.divRate'+parentNumber).on('click', '#btn-remove', function(){
			$(this).parent().parent().remove();
		});
	}

	$scope.insertCalculator = function() {
		var check = true;

		$scope.errorName = false;
		// $scope.errorDp = false;

		if (!$scope.name) {
			$scope.errorName = true;
			check = false;
		}

		// if (!$scope.dp) {
		// 	$scope.errorDp = true;
		// 	check = false;
		// }

		if (check) {
			var rate = [];

			var number = $('#divMargin .divJob').length;

			for (var ii = 0; ii < number; ii++) {
				if ($('#job'+ii).val() && $('#dp'+ii).val()) {
					rate.push({ 'job' : $('#job'+ii).val(), 'dp' : parseFloat($('#dp'+ii).val()), 'margin' : [] });

					var marginNumber = $('.divRate'+ii+' .form-group').length;

					for (var jj = 0; jj < marginNumber; jj++) {
						if ($('#period'+ii+jj).val() && $('#margin'+ii+jj).val()) {
							rate[ii].margin.push({ 'period' : parseInt($('#period'+ii+jj).val()), 'margin' : parseFloat($('#margin'+ii+jj).val()) })
						}
					}
				}
			}

			var data = {
				'name': $scope.name,
				'rate': rate
			}

			RequestService.save(data, 'finance').then(function(response) {
				// RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
					$state.go('app.calculator.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// });
			});
		}
	}

	$scope.init();
});

app.controller('calculatorEditCtrl', function($scope, $stateParams, $state, $rootScope, $timeout, $compile, RequestService) {
	$scope.init = function() {
		if ($stateParams.calculator) { 
			$scope.calculator = $stateParams.calculator;

			$scope.errorName = false;
			// $scope.errorDp = false;

			$timeout(function() {
				$('.divJobOld').on('click', '#btn-remove', function(){
					$(this).parent().parent().parent().remove();
				});

				for (var ii = 0; ii < $scope.calculator.rate.length; ii++) {
					$('.divRateOld'+ii).on('click', '#btn-remove', function(){
						$(this).parent().parent().remove();
					});
				}
			}, 300);

		} else $state.go('app.calculator.index');
	}

	$scope.addJob = function() {
		var number = $('#divMargin .divJob').length;

		var html = '\
			<div class="divJob">\
				<div class="form-group" id="groupJob'+number+'">\
					<label id="labelJob'+number+'" for="job'+number+'" class="col-sm-2 control-label">Jenis Pekerjaan / % DP</label>\
					\
					<div class="col-sm-4" id="inputJob'+number+'">\
						<input type="text" id="job'+number+'" class="form-control" placeholder="Input Jenis Pekerjaan">\
					</div>\
					\
					<div class="col-sm-4" id="inputDp'+number+'">\
						<input type="text" id="dp'+number+'" class="form-control" placeholder="Input % DP">\
					</div>\
					\
					<div class="col-sm-2">\
						<button class="btn btn-flat btn-danger" id="btn-remove" type="button">\
							<i class="fa fa-times"></i>\
						</button>\
					</div>\
				</div>\
				\
				<div class="divRate'+number+'">\
					<div class="form-group" id="groupRate'+number+'0">\
						<label id="labelRate'+number+'0" for="period'+number+'0" class="col-sm-offset-2 col-sm-2 control-label">% Margin</label>\
						\
						<div class="col-sm-3" id="inputRate'+number+'0">\
							<input type="text" id="period'+number+'0" class="form-control" placeholder="Input Period">\
						</div>\
						\
						<div class="col-sm-3" id="inputMargin'+number+'0">\
							<input type="text" id="margin'+number+'0" class="form-control" placeholder="Input % Margin">\
						</div>\
						\
						<div class="col-sm-2">\
							<button class="btn btn-flat btn-primary" id="btn-add" type="button" ng-click="addMargin('+number+')">\
								<i class="fa fa-plus"></i>\
							</button>\
						</div>\
					</div>\
				</div>\
			</div>';

		$('#divMargin').append($compile(html)($scope));

		$('.divJob').on('click', '#btn-remove', function(){
			$(this).parent().parent().parent().remove();
		});
	}

	$scope.addMargin = function(parentNumber) {
		var number = $('.divRate'+parentNumber+' .form-group').length;

		$('.divRate'+parentNumber).append('\
			<div class="form-group" id="groupRate'+parentNumber+''+number+'">\
				<label id="labelRate'+parentNumber+''+number+'" for="period'+parentNumber+''+number+'" class="col-sm-offset-2 col-sm-2 control-label">% Margin</label>\
				\
				<div class="col-sm-3" id="inputRate'+parentNumber+''+number+'">\
					<input type="text" id="period'+parentNumber+''+number+'" class="form-control" placeholder="Input Period">\
				</div>\
				\
				<div class="col-sm-3" id="inputMargin'+parentNumber+''+number+'">\
					<input type="text" id="margin'+parentNumber+''+number+'" class="form-control" placeholder="Input % Margin">\
				</div>\
				\
				<div class="col-sm-2">\
					<button class="btn btn-flat btn-danger" id="btn-remove" type="button">\
						<i class="fa fa-times"></i>\
					</button>\
				</div>\
			</div>\
		');

		$('.divRate'+parentNumber).on('click', '#btn-remove', function(){
			$(this).parent().parent().remove();
		});
	}

	$scope.addMarginOld = function(parentNumber) {
		var number = $('.divRateOld'+parentNumber+' .form-group').length;

		$('.divRateOld'+parentNumber).append('\
			<div class="form-group" id="groupRateOld'+parentNumber+''+number+'">\
				<label id="labelRateOld'+parentNumber+''+number+'" for="period'+parentNumber+''+number+'" class="col-sm-offset-2 col-sm-2 control-label">% Margin</label>\
				\
				<div class="col-sm-3" id="inputRateOld'+parentNumber+''+number+'">\
					<input type="text" id="periodOld'+parentNumber+''+number+'" class="form-control" placeholder="Input Period">\
				</div>\
				\
				<div class="col-sm-3" id="inputMarginOld'+parentNumber+''+number+'">\
					<input type="text" id="marginOld'+parentNumber+''+number+'" class="form-control" placeholder="Input % Margin">\
				</div>\
				\
				<div class="col-sm-2">\
					<button class="btn btn-flat btn-danger" id="btn-remove" type="button">\
						<i class="fa fa-times"></i>\
					</button>\
				</div>\
			</div>\
		');

		$('.divRateOld'+parentNumber).on('click', '#btn-remove', function(){
			$(this).parent().parent().remove();
		});
	}

	$scope.updateCalculator = function() {
		var check = true;

		$scope.errorName = false;
		// $scope.errorDp = false;

		if (!$scope.calculator.name) {
			$scope.errorName = true;
			check = false;
		}

		// if (!$scope.calculator.dp) {
		// 	$scope.errorDp = true;
		// 	check = false;
		// }

		if (check) {
			var rate = [];

			var numberOld = $('#divMarginOld .divJobOld').length;

			var number = $('#divMargin .divJob').length;

			for (var ii = 0; ii < numberOld; ii++) {
				if ($('#jobOld'+ii).val() && $('#dpOld'+ii).val()) {
					rate.push({ 'job' : $('#jobOld'+ii).val(), 'dp' : parseFloat($('#dpOld'+ii).val()), 'margin' : [] });

					var marginNumberOld = $('.divRateOld'+ii+' .form-group').length;

					for (var jj = 0; jj < marginNumberOld; jj++) {
						if ($('#periodOld'+ii+jj).val() && $('#marginOld'+ii+jj).val()) {
							rate[ii].margin.push({ 'period' : parseInt($('#periodOld'+ii+jj).val()), 'margin' : parseFloat($('#marginOld'+ii+jj).val()) })
						}
					}
				}
			}

			for (var ii = 0; ii < number; ii++) {
				if ($('#job'+ii).val() && $('#dp'+ii).val()) {
					rate.push({ 'job' : $('#job'+ii).val(), 'dp' : parseFloat($('#dp'+ii).val()), 'margin' : [] });

					var marginNumber = $('.divRate'+ii+' .form-group').length;

					for (var jj = 0; jj < marginNumber; jj++) {
						if ($('#period'+ii+jj).val() && $('#margin'+ii+jj).val()) {
							rate[ii+numberOld].margin.push({ 'period' : parseInt($('#period'+ii+jj).val()), 'margin' : parseFloat($('#margin'+ii+jj).val()) })
						}
					}
				}
			}

			var data = {
				'id': $scope.calculator.id,
				'name': $scope.calculator.name,
				'rate': rate
			}

			RequestService.save(data, 'finance').then(function(response) {
				$state.go('app.calculator.index');

				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = response.data.message;

				$('#indexModal').modal('show');
			});
		}
	}

	$scope.init();
});