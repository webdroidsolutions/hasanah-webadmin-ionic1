app.controller('settingEmailCtrl', function($scope, $rootScope, $state, RequestService) {
	$scope.settingUmroh;
	$scope.settingProperty;

	$scope.init = function() {
		RequestService.getall('umrohsetting').then(function(data) {
			$scope.settingUmroh = data;

			$('#divUmrohOld').on('click', '#btn-remove', function(){
				$(this).parent().parent().remove();
			});
		});

		RequestService.getall('propertysetting').then(function(data) {
			$scope.settingProperty = data;

			$('#divPropertyOld').on('click', '#btn-remove', function(){
				$(this).parent().parent().remove();
			});
		});
	};

	$scope.addEmailUmroh = function() {
		var number = $('#divUmroh .form-group').length;

		$('#divUmroh').append('\
			<div class="form-group has-feedback" id="groupUmroh'+number+'" data-number="'+number+'">\
				<div class="col-sm-offset-2 col-sm-4" id="inputValue'+number+'">\
					<input type="text" id="emailUmroh'+number+'" class="form-control" placeholder="Input Email">\
				</div>\
				<div class="col-sm-2">\
					<button class="btn btn-flat btn-danger" id="btn-remove" type="button">\
						<i class="fa fa-times"></i>\
					</button>\
				</div>\
			</div>\
		');

		$('#divUmroh').on('click', '#btn-remove', function(){
			$(this).parent().parent().remove();
		});
	};

	$scope.updateEmailUmroh = function() {
		var check = true;

		$scope.errorUmrohTitle = false;
		$scope.errorUmrohContent = false;

		var number = $('#divUmroh .form-group').length;

		if (!$scope.settingUmroh.title) {
			$scope.errorTitle = true;
			check = false;
		}

		if (!$scope.settingUmroh.content) {
			$scope.errorContent = true;
			check = false;
		}

		if (check) {
			var emailUmrohs = [];

			for (var ii = 0; ii < number; ii++) {
				if ($('#emailUmroh'+ii).val()) {
					emailUmrohs.push($('#emailUmroh'+ii).val());
				}
			}

			for (var ii = 0; ii < $scope.settingUmroh.email.length; ii++) {
				if ($('#emailUmrohOld'+ii).val()) {
					emailUmrohs.push($('#emailUmrohOld'+ii).val());
				}
			}

			var data = {
				'id': $scope.settingUmroh.id,
				'title': $scope.settingUmroh.title,
	            'content': $scope.settingUmroh.content,
	            'email': emailUmrohs
			}

			RequestService.save(data, 'umrohsetting').then(function(response) {
				$state.reload();

				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = response.data.message;

				$('#indexModal').modal('show');
			});
		};	
	};

	$scope.addEmailProperty = function() {
		var number = $('#divProperty .form-group').length;

		$('#divProperty').append('\
			<div class="form-group has-feedback" id="groupProperty'+number+'" data-number="'+number+'">\
				<div class="col-sm-offset-2 col-sm-4" id="inputValue'+number+'">\
					<input type="text" id="emailProperty'+number+'" class="form-control" placeholder="Input Email">\
				</div>\
				<div class="col-sm-2">\
					<button class="btn btn-flat btn-danger" id="btn-remove" type="button">\
						<i class="fa fa-times"></i>\
					</button>\
				</div>\
			</div>\
		');

		$('#divProperty').on('click', '#btn-remove', function(){
			$(this).parent().parent().remove();
		});
	};

	$scope.updateEmailProperty = function() {
		var check = true;

		$scope.errorPropertyTitle = false;
		$scope.errorPropertyContent = false;

		var number = $('#divProperty .form-group').length;

		if (!$scope.settingProperty.title) {
			$scope.errorTitle = true;
			check = false;
		}

		if (!$scope.settingProperty.content) {
			$scope.errorContent = true;
			check = false;
		}

		if (check) {
			var emailProperties = [];

			for (var ii = 0; ii < number; ii++) {
				if ($('#emailProperty'+ii).val()) {
					emailProperties.push($('#emailProperty'+ii).val());
				}
			}

			for (var ii = 0; ii < $scope.settingProperty.email.length; ii++) {
				if ($('#emailPropertyOld'+ii).val()) {
					emailProperties.push($('#emailPropertyOld'+ii).val());
				}
			}

			var data = {
				'id': $scope.settingProperty.id,
				'title': $scope.settingProperty.title,
	            'content': $scope.settingProperty.content,
	            'email': emailProperties
			}

			RequestService.save(data, 'propertysetting').then(function(response) {
				$state.reload();

				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = response.data.message;

				$('#indexModal').modal('show');
			});
		};	
	};

	$scope.init();
});