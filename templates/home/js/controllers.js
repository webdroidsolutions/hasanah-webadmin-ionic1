app.controller('homeSlideIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.slides = [];

	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
	        var q = $q.defer();
	        var data = {};

	        if ($scope.status != 99) {
		        data.status = $scope.status;
	        }

	        RequestService.getbystatus(data, 'home').then(function(data) {
				q.resolve(data);
			});

	        return q.promise;
	    }).withOption('createdRow', createdRow);

	    $scope.dtColumns = [
	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Gambar').notSortable().renderWith(imageLink),
	        DTColumnBuilder.newColumn('description').withTitle('Deskripsi'),
	        DTColumnBuilder.newColumn(null).withTitle('Waktu').renderWith(timeHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Status').renderWith(statusHtml),
	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
	    ];

	    $scope.dtInstance = {};

	    function createdRow(row, data, dataIndex) {
	    	$compile(angular.element(row).contents())($scope);
	    }

	    function numberHtml(data, type, full, meta) {
	    	return meta.row + 1;
	    }

	    function statusHtml(data, type, full, meta) {
	    	var status;

	    	if (data.status) {
	    		status = '<span class="status-active">Aktif</span>';
	    	} else {
	    		status = '<span class="status-nonactive">Non-Aktif</span>';
	    	}

	        return status;
		}

	    function imageHtml(data, type, full, meta) {
	        return '<img src="data:image;base64,' + data.img + '" width="100px">';
		}
	    function imageLink(data, type, full, meta) {
	    	return '<img src="' + RequestService.url.replace('adm/', '') + data.img + '" width="55px">';
		}

		function timeHtml(data, type, full, meta) {
			var date = new Date(data.time);

			return TimeService.days[date.getDay()] + ', ' 
				+ ('0' + date.getDate()).slice(-2) + ' '
				+ TimeService.months[date.getMonth()] + ' '
				+ date.getFullYear() + ' '
				+ ('0' + date.getHours()).slice(-2) + ':'
				+ ('0' + date.getMinutes()).slice(-2) + ':'
				+ ('0' + date.getMinutes()).slice(-2);
		}

		function actionHtml(data, type, full, meta) {
			$scope.slides[meta.row] = data;

			return '\
				<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.home.slide.edit\', slides[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
				<button class="btn btn-danger btn-xs btn-block" ng-click="destroySlide(slides[' + meta.row + '].id, slides[' + meta.row + '].img)"><i class="fa fa-trash-o"></i> Delete</button>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			slide: objectData
		});
	}

	$scope.destroySlide = function(id, old_img) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';
		
		$rootScope.modalAction = function() {
			var data = { 'id' : id, 'old_img' : old_img };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'home').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('homeSlideCreateCtrl', function($scope, $state, $rootScope, RequestService) {
	$scope.init = function() {
		$scope.errorImg = false;
		$scope.errorDescription = false;

		$scope.status = true;
	}

	$scope.insertSlide = function() {
		var check = true;

		$scope.errorImg = false;
		$scope.errorDescription = false;

		if (!$scope.imgFile) {
			$scope.errorImg = true;
			check = false;
		}

		if (!$scope.description) {
			$scope.errorDescription = true;
			check = false;
		}

		if (check) {
			var date = new Date();
			var status;

			if ($scope.status) {
				status = 1;
			} else {
				status = 0;
			}

			var data = {
				'img': $scope.imgFile.base64,
	            'description': $scope.description,
	            'time': date,
	            'href': '',
	            'status': status
			}

			RequestService.save(data, 'home').then(function(response) {
				// RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
					$state.go('app.home.slide.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// });
			});
		}
	}

	$scope.init();
});

app.controller('homeSlideEditCtrl', function($scope, $stateParams, $state, $rootScope, RequestService) {
	$scope.init = function() {
		if ($stateParams.slide) { 
			$scope.slide = $stateParams.slide;

			$scope.errorDescription = false;

			if ($scope.slide.status) {
				$scope.status_ = true;
			} else {
				$scope.status_ = false;
			}
		} else $state.go('app.home.slide.index');
	}

	$scope.updateSlide = function() {
		var check = true;

		$scope.errorDescription = false;

		if (!$scope.slide.description) {
			$scope.errorDescription = true;
			check = false;
		}

		if (check) {
			var img = '';
			var old_img = '';
			var status;

			if ($scope.imgFile) {
				img = $scope.imgFile.base64;
				old_img = $scope.slide.img;
			} else {
				img = $scope.slide.img;
			}

			if ($scope.status_) {
				status = 1;
			} else {
				status = 0;
			}

			var data = {
				'id': $scope.slide.id,
				'img': img,
				'old_img': old_img,
	            'description': $scope.slide.description,
	            'time': $scope.slide.time,
	            'href': '',
	            'status': status
			}

			RequestService.save(data, 'home').then(function(response) {
				// if ($scope.imgFile) {
				// 	RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
				// 		$state.go('app.home.index');

				// 		$rootScope.modalHeader  = 'Pesan';
				// 		$rootScope.modalMessage = response.data.message;

				// 		$('#indexModal').modal('show');
				// 	});
				// } else {
					$state.go('app.home.slide.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// }
			});
		}
	}

	$scope.init();
});

app.controller('homeMenuIndexCtrl', function($scope, $rootScope, $state, $compile, $q, DTOptionsBuilder, DTColumnBuilder, RequestService, TimeService) {
	$scope.init = function() {
		$scope.menus = [];

	    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
	        var q = $q.defer();

	        RequestService.getall('quickmenu').then(function(data) {
				q.resolve(data);
			});

	        return q.promise;
	    }).withOption('createdRow', createdRow);

	    $scope.dtColumns = [
	        DTColumnBuilder.newColumn(null).withTitle('No').withOption('width', '10px').withClass('text-center').renderWith(numberHtml),
	        DTColumnBuilder.newColumn('title').withTitle('Judul'),
	        DTColumnBuilder.newColumn(null).withTitle('Gambar').notSortable().renderWith(imageLink),
	        DTColumnBuilder.newColumn('description').withTitle('Deskripsi'),
	        DTColumnBuilder.newColumn('link').withTitle('Link'),
	        DTColumnBuilder.newColumn(null).withTitle('Aksi').notSortable().renderWith(actionHtml)
	    ];

	    $scope.dtInstance = {};

	    function createdRow(row, data, dataIndex) {
	    	$compile(angular.element(row).contents())($scope);
	    }

	    function numberHtml(data, type, full, meta) {
	    	return meta.row + 1;
	    }

	    function imageHtml(data, type, full, meta) {
	        return '<img src="data:image;base64,' + data.img + '" width="50px">';
		}
	    function imageLink(data, type, full, meta) {
	    	return '<img src="' + RequestService.url.replace('adm/', '') + data.img + '" width="55px">';
		}

		// function timeHtml(data, type, full, meta) {
		// 	var date = new Date(data.time);

		// 	return TimeService.days[date.getDay()] + ', ' 
		// 		+ ('0' + date.getDate()).slice(-2) + ' '
		// 		+ TimeService.months[date.getMonth()] + ' '
		// 		+ date.getFullYear() + ' '
		// 		+ ('0' + date.getHours()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2) + ':'
		// 		+ ('0' + date.getMinutes()).slice(-2);
		// }

		function actionHtml(data, type, full, meta) {
			$scope.menus[meta.row] = data;

				// <button class="btn btn-danger btn-xs btn-block" ng-click="destroyMenu(menus[' + meta.row + '].id, menus[' + meta.row + '].img)"><i class="fa fa-trash-o"></i> Delete</button>\
			return '\
				<button class="btn btn-primary btn-xs btn-block" ng-click="navigateTo(\'app.home.menu.edit\', menus[' + meta.row + '])"><i class="fa fa-pencil-square-o"></i> Edit</button>\
			';
		}
	}

	$scope.reloadData = function() {
		$scope.dtInstance.reloadData();
	}

	$scope.navigateTo = function(targetPage, objectData) {
		$state.go(targetPage, {
			menu: objectData
		});
	}

	$scope.destroyMenu = function(id, old_img) {
		$rootScope.confirmHeader  = 'Konfirmasi';
		$rootScope.confirmMessage = 'Apakah Anda yakin ingin menghapus?';
		$rootScope.confirmButton  = 'Hapus';

		$rootScope.modalAction = function() {
			var data = { 'id' : id, 'old_img' : old_img };

			$('#indexConfirm').modal('hide');
			
			RequestService.delete(data, 'quickmenu').then(function(response) {
				$scope.reloadData();
			});
		}

		$('#indexConfirm').modal('show');
	}

	$scope.init();
});

app.controller('homeMenuCreateCtrl', function($scope, $state, $rootScope, RequestService) {
	$scope.init = function() {
		$scope.errorImg = false;
		$scope.errorDescription = false;

		$scope.links = [
			{'value': 'product', 'text': 'Produk'},
			{'value': 'branch', 'text': 'Cabang'},
			{'value': 'merchant', 'text': 'Merchant'},
			{'value': 'umroh', 'text': 'Umroh'},
			{'value': 'property', 'text': 'Properti'},
			{'value': 'calculator', 'text': 'Kalkulator'},
			{'value': 'filing', 'text': 'Pengajuan'},
			{'value': 'property', 'text': 'Properti'},
			{'value': 'term', 'text': 'Istilah'},
			{'value': 'promo', 'text': 'Promo'},
			{'value': 'survey', 'text': 'Survey'},
			{'value': 'suggestion', 'text': 'Saran'},
		];
	}

	$scope.insertMenu = function() {
		var check = true;

		$scope.errorTitle = false;
		$scope.errorImg = false;
		$scope.errorDescription = false;
		$scope.errorLink = false;

		if (!$scope.title) {
			$scope.errorTitle = true;
			check = false;
		}

		if (!$scope.imgFile) {
			$scope.errorImg = true;
			check = false;
		}

		if (!$scope.description) {
			$scope.errorDescription = true;
			check = false;
		}

		if (!$scope.link) {
			$scope.errorLink = true;
			check = false;
		}

		if (check) {
			var date = new Date();

			var data = {
				'title': $scope.title,
				'img': $scope.imgFile.base64,
	            'description': $scope.description,
	            'link': $scope.link
			}

			RequestService.save(data, 'quickmenu').then(function(response) {
				// RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
					$state.go('app.home.menu.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// });
			});
		}
	}

	$scope.init();
});

app.controller('homeMenuEditCtrl', function($scope, $stateParams, $state, $rootScope, RequestService) {
	$scope.init = function() {
		if ($stateParams.menu) { 
			$scope.menu = $stateParams.menu;

			$scope.links = [
				{'value': 'product', 'text': 'Produk'},
				{'value': 'branch', 'text': 'Cabang'},
				{'value': 'merchant', 'text': 'Merchant'},
				{'value': 'umroh', 'text': 'Umroh'},
				{'value': 'property', 'text': 'Properti'},
				{'value': 'calculator', 'text': 'Kalkulator'},
				{'value': 'filing', 'text': 'Pengajuan'},
				{'value': 'property', 'text': 'Properti'},
				{'value': 'term', 'text': 'Istilah'},
				{'value': 'promo', 'text': 'Promo'},
				{'value': 'survey', 'text': 'Survey'},
				{'value': 'suggestion', 'text': 'Saran'},
			];

			$scope.errorDescription = false;
		} else $state.go('app.home.menu.index');
	}

	$scope.updateMenu = function() {
		var check = true;

		$scope.errorTitle = false;
		$scope.errorDescription = false;
		$scope.errorLink = false;

		if (!$scope.menu.title) {
			$scope.errorTitle = true;
			check = false;
		}

		if (!$scope.menu.description) {
			$scope.errorDescription = true;
			check = false;
		}

		if (!$scope.menu.link) {
			$scope.errorLink = true;
			check = false;
		}

		if (check) {
			var img = '';
			var old_img = '';

			if ($scope.imgFile) {
				img = $scope.imgFile.base64;
				old_img = $scope.menu.img;
			} else {
				img = $scope.menu.img;
			}

			var data = {
				'id': $scope.menu.id,
	            'title': $scope.menu.title,
				'img': img,
				'old_img': old_img,
	            'description': $scope.menu.description,
	            'link': $scope.menu.link
			}

			RequestService.save(data, 'quickmenu').then(function(response) {
				// if ($scope.imgFile) {
				// 	RequestService.uploadImg($scope.imgFile, response.data.id).then(function(response) {
				// 		$state.go('app.home.index');

				// 		$rootScope.modalHeader  = 'Pesan';
				// 		$rootScope.modalMessage = response.data.message;

				// 		$('#indexModal').modal('show');
				// 	});
				// } else {
					$state.go('app.home.menu.index');

					$rootScope.modalHeader  = 'Pesan';
					$rootScope.modalMessage = response.data.message;

					$('#indexModal').modal('show');
				// }
			});
		}
	}

	$scope.init();
});

// app.controller('homeSettingCtrl', function($scope, $stateParams, $state, $rootScope, RequestService) {
// 	$scope.init = function() {
// 		$scope.setting = '';

// 		RequestService.getall('homesetting').then(function(data) {
// 			$scope.setting = data;
// 		});
// 	};

// 	$scope.updateSetting = function() {
// 		var check = true;

// 		$scope.errorInterval = false;

// 		if (!$scope.setting.interval) {
// 			$scope.errorInterval = true;
// 			check = false;
// 		}

// 		if (check) {
// 			var data = {
// 				'id': $scope.setting.id,
// 	            'interval': $scope.setting.interval,
// 	            'repeat': $scope.setting.repeat
// 			}

// 			RequestService.save(data, 'homesetting').then(function(response) {
// 				$state.go('app.home.slide.index');

// 				$rootScope.modalHeader  = 'Pesan';
// 				$rootScope.modalMessage = response.data.message;

// 				$('#indexModal').modal('show');
// 			});
// 		}
// 	}

// 	$scope.init();
// });
