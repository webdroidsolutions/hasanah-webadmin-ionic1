app.controller('appCtrl', function($scope, $location, $rootScope, $window, $sce, $state, RequestService) {
	$scope.init = function() {
		$scope.$on('$viewContentLoaded', function(){
			$.AdminLTE.layout.activate();
		});

        $scope.links=JSON.parse(localStorage.links);
        $scope.user=localStorage.user;
        $scope.displayName = localStorage.name;
        $scope.displayType = localStorage.user_type;
    }

    $scope.isActive = function (viewLocation) { 
        return $location.path().indexOf(viewLocation) !== -1
    };

    $scope.doLogout = function() {
        RequestService.logout().then(function(response) {
            $rootScope.modalHeader  = 'Pesan';
            $rootScope.modalMessage = response.data.message;

            $('#indexModal').modal('show');

            if (response.data.status === 'success') {
                localStorage.setItem('sn', '');
                $state.go('login');
            }
        });
    }

    $rootScope.renderHtml = function(html){
        return $sce.trustAsHtml(html);
    };

    $rootScope.goBack = function() {
        $window.history.back();
    }

    $scope.init();
});