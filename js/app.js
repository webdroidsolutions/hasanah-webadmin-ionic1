var app = angular.module('app', ['ui.router', 'gm.datepickerMultiSelect', 'ui.bootstrap', 'datatables', 'ui.select2', 'ngFileUpload', 'bsLoadingOverlay', 'daterangepicker', 'naif.base64', 'ngSanitize', 'ngCsv']);

app.directive('ckEditor', function() {
	return {
		require: '?ngModel',
		link: function(scope, elm, attr, ngModel) {
			var ck = CKEDITOR.replace(elm[0]);

			if (!ngModel) return;

			ck.on('pasteState', function() {
				scope.$apply(function() {
					ngModel.$setViewValue(ck.getData());
				});
			});

			ngModel.$render = function(value) {
				ck.setData(ngModel.$viewValue);
			};
		}
	};
});

app.directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;

              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }]);


// angular.module('FileManagerApp').config(['fileManagerConfigProvider', function (config) {
//       var defaults = config.$get();
//       config.set({
//         appName: 'angular-filemanager',
//         pickCallback: function(item) {
//           var msg = 'Picked %s "%s" for external use'
//             .replace('%s', item.type)
//             .replace('%s', item.fullPath());
//           window.alert(msg);
//         },

//         allowedActions: angular.extend(defaults.allowedActions, {
//           pickFiles: false,
//           pickFolders: false,
//         }),
//       });
//     }]);

app.directive('format', ['$filter', function ($filter) {
	return {
		require: '?ngModel',
		link: function (scope, elem, attrs, ctrl) {
			if (!ctrl) return;

			var symbol = "";
			
			ctrl.$formatters.unshift(function (a) {
				return $filter(attrs.format)(ctrl.$modelValue) + symbol;
			});

			ctrl.$parsers.unshift(function (viewValue) {
				var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
				elem.val($filter('number')(plainNumber) + symbol);
				return plainNumber;
			});
		}
	}
}]);

app.filter('rupiah', function() {
	return function(jumlah) {
		var titik = ".";
		var nilai = new String(jumlah);
		var pecah = [];
		while(nilai.length > 3)
		{
			var asd = nilai.substr(nilai.length-3);
			pecah.unshift(asd);
			nilai = nilai.substr(0, nilai.length-3);
		}

		if(nilai.length > 0) { pecah.unshift(nilai); }
		nilai = pecah.join(titik);
		return nilai;
	};
});

app.run(['uiSelect2Config', function(uiSelect2Config) {
	uiSelect2Config.placeholder = "Silahkan Dipilih";
}]);

app.run(function(bsLoadingOverlayService) {
	bsLoadingOverlayService.setGlobalConfig({
		templateUrl: 'templates/loading/html/loading.html'
	});
});

app.factory('TimeService', function() {
	return {
		days: ["Ahad", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"],
		months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
	};
});

app.factory('RequestService', function($http, $q, $timeout, $rootScope, $state, bsLoadingOverlayService, Upload) {
	return {
		url : 'http://localhost:8180/adm/',
		//url : 'http://114.6.71.168:8180/adm/',
		// url : 'http://192.168.10.139:8080/adm/',
		deviceType: 'web_admin',
		ceksession: function() {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			var data = { 
				'sn' : localStorage.sn,
				'device_type': this.deviceType
			};

			$http({
				method: 'POST',
				url: this.url+'home/getall',
				data: data,
				contentType: 'application/json',
				headers: {
					'Content-Type':'application/json'
				}
			}).then(function mySuccess(response) {
				q.resolve(response.data);
				bsLoadingOverlayService.stop();

				if (localStorage.user_type == 'ADMIN') {
					$state.go('app.home.slide.index');
				} else {
					$state.go('app.umroh.index');
				}
			}, function() {
				bsLoadingOverlayService.stop();
			});

			return q.promise;
		},
		uploadFileToUrl: function(file,str,index){
		   // bsLoadingOverlayService.start();
		   var q = $q.defer();
           var fd = new FormData();
           fd.append('file', file);
           $http({
           	  method:'POST',
           	  data:fd,
           	  url:this.url+"upload/uploadimg",
              transformRequest: angular.identity,
              headers: {'Content-Type': undefined}
           }).then(function(response){
           		if(str == 'img' || str == 'portfolio'){
           			$rootScope.$broadcast('image:uploaded', response.data,str,index); 
           		}else{
           			$rootScope.$broadcast('pdf:uploaded', response.data); 
           		}
           		
	        	// bsLoadingOverlayService.stop();
           },
           function(){
           	// bsLoadingOverlayService.stop();
           });
        },
		removeFile: function(data) {
			bsLoadingOverlayService.start();
			var q = $q.defer();
			data.device_type = this.deviceType;
			data.sn = localStorage.sn;

			$http({
	            method: 'POST',
	            url: this.url+'upload/remove',
	            data: data,
	            contentType: 'application/json',
	            headers: {
	                'Content-Type':'application/json'
	            }
	        }).then(function(response) {
	        	q.resolve(response);
	        	bsLoadingOverlayService.stop();
	        }, function(data) {
	        bsLoadingOverlayService.stop();	
	        });

        	return q.promise;
		},
		login: function(data) {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			data.device_type = this.deviceType;

			$http({
				method: 'POST',
				url: this.url+'user/login',
				data: data,
				contentType: 'application/json',
				headers: {
					'Content-Type':'application/json'
				}
			}).then(function(response) {
				q.resolve(response);
				bsLoadingOverlayService.stop();
			}, function(data) {
				bsLoadingOverlayService.stop();
				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = data.data.message;

				$('#indexModal').modal('show');

				if (data.status == 498) {
					$state.go('login');
				}
			});

			return q.promise;
		},
		forgotPassword: function(data) {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			data.device_type = this.deviceType;

			$http({
	            method: 'POST',
	            url: this.url+'user/forgot',
	            data: data,
	            contentType: 'application/json',
	            headers: {
	                'Content-Type':'application/json'
	            }
	        }).then(function(response) {
	        	q.resolve(response);
	        	bsLoadingOverlayService.stop();
	        }, function(data) {
	        	bsLoadingOverlayService.stop();
	        	$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = data.data.message;

				$('#indexModal').modal('show');

				/*if (data.status == 498) {
					$state.go('login');
				}*/
	        });

        	return q.promise;
		},
		logout: function() {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			var data = { 
				'sn' : localStorage.sn,
				'device_type': this.deviceType
			};

			$http({
				method: 'POST',
				url: this.url+'user/logout',
				data: data,
				contentType: 'application/json',
				headers: {
					'Content-Type':'application/json'
				}
			}).then(function(response) {
				q.resolve(response);
				bsLoadingOverlayService.stop();
			}, function(data) {
				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = data.data.message;

				$('#indexModal').modal('show');

				if (data.status == 498) {
					$state.go('login');
				}
			});

			return q.promise;
		},
		get: function(url, link) {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			var data = {
				'sn' : localStorage.sn,
				'device_type': this.deviceType
			}

			$http({
				method: 'POST',
				url: this.url+url+'/get'+link,
				data: data,
				contentType: 'application/json',
				headers: {
					'Content-Type':'application/json'
				}
			}).then(function(response) {
				q.resolve(response.data);
				bsLoadingOverlayService.stop();
			}, function(data) {
				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = data.data.message;

				$('#indexModal').modal('show');

				if (data.status == 498) {
					$state.go('login');
				}
			});

			return q.promise;
		},
		
		getMenus: function(url) {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			var data = { 
				'sn' : localStorage.sn,
				'device_type': this.deviceType
			};

			$http({
	            method: 'POST',
	            url: this.url+'role/getMenus',
	            data: data,
	            contentType: 'application/json',
	            headers: {
	                'Content-Type':'application/json'
	            }
	        }).then(function mySuccess(response) {
        		q.resolve(response.data);
	        	bsLoadingOverlayService.stop();
	        }, function(data) {
	        	$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = data.data.message;

				$('#indexModal').modal('show');

				if (data.status == 498) {
					$state.go('login');
				}
	        });

        	return q.promise;
		},
		getall: function(url) {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			var data = { 
				'sn' : localStorage.sn,
				'device_type': this.deviceType
			};
			if (localStorage.user_type.toUpperCase()=='TRAVEL ADMIN') {
				data.travelagent_id=localStorage.travelagent_id;
			}
			$http({
	            method: 'POST',
	            url: this.url+url+'/getall',
	            data: data,
	            contentType: 'application/json',
	            headers: {
	                'Content-Type':'application/json'
	            }
	        }).then(function mySuccess(response) {
        		q.resolve(response.data);
	        	bsLoadingOverlayService.stop();
	        }, function(data) {
	        	$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = data.data.message;

				$('#indexModal').modal('show');

				if (data.status == 498) {
					$state.go('login');
				}
	        });

        	return q.promise;
		},
		getRoles: function(url) {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			var data = { 
				'sn' : localStorage.sn,
				'device_type': this.deviceType
			};

			$http({
	            method: 'POST',
	            url: this.url+url+'/getAll',
	            data: data,
	            contentType: 'application/json',
	            headers: {
	                'Content-Type':'application/json'
	            }
	        }).then(function mySuccess(response) {
        		q.resolve(response.data);
	        	bsLoadingOverlayService.stop();
	        }, function(data) {
	        	$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = data.data.message;

				$('#indexModal').modal('show');

				if (data.status == 498) {
					$state.go('login');
				}
	        });

        	return q.promise;
		},
		getby: function(data, url, by) {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			data.sn = localStorage.sn;
			data.device_type = this.deviceType;

			$http({
				method: 'POST',
				url: this.url+url+'/getby'+by,
				data: data,
				contentType: 'application/json',
				headers: {
					'Content-Type':'application/json'
				}
			}).then(function mySuccess(response) {
				q.resolve(response.data);
				bsLoadingOverlayService.stop();
			});

			return q.promise;
		},
		getAddressByType: function(id,type) {
			bsLoadingOverlayService.start();
			var q = $q.defer();
			var data = {};
			data.sn = localStorage.sn;
			data.device_type = this.deviceType;
			if(type == 'province'){
				data.type = type;
			}else if(type == 'regency'){
				data.type = type;
				data.provinceId = id;
			}else if(type == 'district'){
				data.type = type;
				data.regencyId = id;
			}else{
				data.type = type;
				data.districtId = id;
			}
			$http({
				method: 'POST',
				url: this.url+'travelagency/getProvince',
				data: data,
				contentType: 'application/json',
				headers: {
					'Content-Type':'application/json'
				}
			}).then(function mySuccess(response) {
				q.resolve(response.data);
				bsLoadingOverlayService.stop();
			});

			return q.promise;
		},
		getreport: function(data, url) {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			data.sn = localStorage.sn;
			data.device_type = this.deviceType;

			$http({
				method: 'POST',
				url: this.url+url+'/getreport',
				data: data,
				contentType: 'application/json',
				headers: {
					'Content-Type':'application/json'
				}
			}).then(function mySuccess(response) {
				q.resolve(response.data);
				bsLoadingOverlayService.stop();
			});

			return q.promise;
		},
		getbystatus: function(data, url) {
			bsLoadingOverlayService.start();
			var q = $q.defer();
			// hadi.bnisyariah@gmail.com
			data.sn = localStorage.sn;
			data.device_type = this.deviceType;
			if (localStorage.user_type.toUpperCase()=='TRAVEL ADMIN') {
				data.travelagent_id=localStorage.travelagent_id;
			}
			$http({
	            method: 'POST',
	            url: this.url+url+'/getall',
	            data: data,
	            contentType: 'application/json',
	            headers: {
	                'Content-Type':'application/json'
	            }
	        }).then(function mySuccess(response) {
        		q.resolve(response.data);
	        	bsLoadingOverlayService.stop();
	        }, function(data) {
	        	$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = data.data.message;

				$('#indexModal').modal('show');

				if (data.status == 498) {
					$state.go('login');
				}
	        });

        	return q.promise;
		},
		getbypaging: function(data, url, page, limit) {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			data.sn = localStorage.sn;
			data.device_type = this.deviceType;
			data.page = page;
			data.limit = limit;
			if (localStorage.user_type.toUpperCase()=='TRAVEL ADMIN') {
				data.travelagent_id=localStorage.travelagent_id;
			}
			$http({
	            method: 'POST',
	            url: this.url+url+'/getall',
	            data: data,
	            contentType: 'application/json',
	            headers: {
	                'Content-Type':'application/json'
	            }
	        }).then(function mySuccess(response) {
        		q.resolve(response.data);
	        	bsLoadingOverlayService.stop();
	        }, function(data) {
	        	$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = data.data.message;

				$('#indexModal').modal('show');

				if (data.status == 498) {
					$state.go('login');
				}
	        });

        	return q.promise;
		},
		gettotal: function(data, url) {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			data.sn = localStorage.sn;
			data.device_type = this.deviceType;

			$http({
	            method: 'POST',
	            url: this.url+url+'/getcount',
	            data: data,
	            contentType: 'application/json',
	            headers: {
	                'Content-Type':'application/json'
	            }
	        }).then(function mySuccess(response) {
        		q.resolve(response.data[0]['total']);
	        	bsLoadingOverlayService.stop();
	        }, function(data) {
	        	$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = data.data.message;

				$('#indexModal').modal('show');

				if (data.status == 498) {
					$state.go('login');
				}
	        });

        	return q.promise;
		},
		save: function(data, url) {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			data.device_type = this.deviceType;
			data.sn = localStorage.sn;

			$http({
	            method: 'POST',
	            url: this.url+url+'/save',
	            data: data,
	            contentType: 'application/json',
	            headers: {
	                'Content-Type':'application/json'
	            }
	        }).then(function(response) {
	        	q.resolve(response);
	        	bsLoadingOverlayService.stop();
	        }, function(data) {
	        	$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = data.data.message;
				console.log('error');
				$('#indexModal').modal('show');

				if (data.status == 498) {
					$state.go('login');
				}
	        });

        	return q.promise;
		},
		delete: function(data, url) {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			data.device_type = this.deviceType;
			data.sn = localStorage.sn;

			$http({
	            method: 'POST',
	            url: this.url+url+'/delete',
	            data: data,
	            contentType: 'application/json',
	            headers: {
	                'Content-Type':'application/json'
	            }
	        }).then(function(response) {
	        	q.resolve(response);
	        	bsLoadingOverlayService.stop();
	        }, function(data) {
	        	$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = data.data.message;

				$('#indexModal').modal('show');

				if (data.status == 498) {
					$state.go('login');
				}
	        });

        	return q.promise;
		},
		update: function(data, url) {
			bsLoadingOverlayService.start();
			var q = $q.defer();
			data.sn = localStorage.sn;

			$http({
	            method: 'POST',
	            url: this.url+url+'/edit',
	            data: data,
	            contentType: 'application/json',
	            headers: {
	                'Content-Type':'application/json'
	            }
	        }).then(function(response) {
	        	q.resolve(response);
	        	bsLoadingOverlayService.stop();
	        }, function(data) {
	        	q.resolve(response.data);
				bsLoadingOverlayService.stop();
	        	/*$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = data.data.message;
				$('#indexModal').modal('show');*/
				if (data.status == 498) {
					$state.go('login');
				}
	        });

        	return q.promise;
		},
		uploadImg: function(file, id) {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			file.upload = Upload.upload({
				url: this.url + 'util/upload',
				data: {
					'id': id, 
					'device_type': this.deviceType,
					'sn': localStorage.sn,
					'file': file
				}
			});

			file.upload.then(function (response) {
				$timeout(function () {
					q.resolve(response);
		        	bsLoadingOverlayService.stop();
		        });
			}, function(data) {
				$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = data.data.message;

				$('#indexModal').modal('show');

				if (data.status == 498) {
					$state.go('login');
				}
			});

			return q.promise;
		},
		gettest: function(url) {
			bsLoadingOverlayService.start();
			var q = $q.defer();

			$http({
	            method: 'GET',
	            url: 'app-data/' + url + '.json',
	            contentType: 'application/json',
	            headers: {
	                'Content-Type':'application/json'
	            }
	        }).then(function(response) {
        		q.resolve(response.data);
	        	bsLoadingOverlayService.stop();
	        }, function(data) {
	        	$rootScope.modalHeader  = 'Pesan';
				$rootScope.modalMessage = data.data.message;

				$('#indexModal').modal('show');

				if (data.status == 498) {
					$state.go('login');
				}
	        });

        	return q.promise;
		}
	};
});

app.config(function($stateProvider, $urlRouterProvider) {
	$stateProvider

	.state('login', {
		url: '/login',
		templateUrl: 'templates/login/html/login.html',
		controller: 'loginCtrl'
	})

	.state('app', {
		url: '/app',
		abstract: true,
		templateUrl: 'templates/app/html/app.html',
		controller: 'appCtrl'
	})

	.state('app.home', {
		url: '/home',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/home/html/home.html'
			}
		}
	})

	.state('app.home.slide', {
		url: '/slide',
		abstract: true,
		views: {
			'homeContent': {
				templateUrl: 'templates/home/html/slide.html'
			}
		}
	})

	.state('app.home.slide.index', {
		url: '/index',
		views: {
			'slideContent': {
				templateUrl: 'templates/home/html/slide-index.html',
				controller: 'homeSlideIndexCtrl'
			}
		}
	})

	.state('app.home.slide.create', {
		url: '/create',
		views: {
			'slideContent': {
				templateUrl: 'templates/home/html/slide-create.html',
				controller: 'homeSlideCreateCtrl'
			}
		}
	})

	.state('app.home.slide.edit', {
		url: '/edit',
		params: {
			slide: null
		},
		views: {
			'slideContent': {
				templateUrl: 'templates/home/html/slide-edit.html',
				controller: 'homeSlideEditCtrl'
			}
		}
	})

	.state('app.home.menu', {
		url: '/menu',
		abstract: true,
		views: {
			'homeContent': {
				templateUrl: 'templates/home/html/menu.html'
			}
		}
	})

	.state('app.home.menu.index', {
		url: '/index',
		views: {
			'menuContent': {
				templateUrl: 'templates/home/html/menu-index.html',
				controller: 'homeMenuIndexCtrl'
			}
		}
	})

	.state('app.home.menu.create', {
		url: '/create',
		views: {
			'menuContent': {
				templateUrl: 'templates/home/html/menu-create.html',
				controller: 'homeMenuCreateCtrl'
			}
		}
	})

	.state('app.home.menu.edit', {
		url: '/edit',
		params: {
			menu: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/home/html/menu-edit.html',
				controller: 'homeMenuEditCtrl'
			}
		}
	})

	// .state('app.home.setting', {
	// 	url: '/setting',
	// 	views: {
	// 		'homeContent': {
	// 			templateUrl: 'templates/home/html/setting.html',
	// 			controller: 'homeSettingCtrl'
	// 		}
	// 	}
	// })

	.state('app.product', {
		url: '/product',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/product/html/product.html',
				controller: 'productCtrl'
			}
		}
	})

	.state('app.product.index', {
		url: '/index',
		views: {
			'productContent': {
				templateUrl: 'templates/product/html/index.html',
				controller: 'productIndexCtrl'
			}
		}
	})

	.state('app.product.detail', {
		url: '/detail',
		params: {
			product: null
		},
		views: {
			'productContent': {
				templateUrl: 'templates/product/html/detail.html',
				controller: 'productDetailCtrl'
			}
		}
	})

	.state('app.product.create', {
		url: '/create',
		views: {
			'productContent': {
				templateUrl: 'templates/product/html/create.html',
				controller: 'productCreateCtrl'
			}
		}
	})

	.state('app.product.edit', {
		url: '/edit',
		params: {
			product: null
		},
		views: {
			'productContent': {
				templateUrl: 'templates/product/html/edit.html',
				controller: 'productEditCtrl'
			}
		}
	})

	.state('app.branch', {
		url: '/branch',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/branch/html/branch.html',
				controller: 'branchCtrl'
			}
		}
	})

	.state('app.branch.index', {
		url: '/index',
		views: {
			'branchContent': {
				templateUrl: 'templates/branch/html/index.html',
				controller: 'branchIndexCtrl'
			}
		}
	})

	.state('app.branch.detail', {
		url: '/detail',
		params: {
			branch: null
		},
		views: {
			'branchContent': {
				templateUrl: 'templates/branch/html/detail.html',
				controller: 'branchDetailCtrl'
			}
		}
	})

	.state('app.branch.create', {
		url: '/create',
		views: {
			'branchContent': {
				templateUrl: 'templates/branch/html/create.html',
				controller: 'branchCreateCtrl'
			}
		}
	})

	.state('app.branch.edit', {
		url: '/edit',
		params: {
			branch: null
		},
		views: {
			'branchContent': {
				templateUrl: 'templates/branch/html/edit.html',
				controller: 'branchEditCtrl'
			}
		}
	})

	.state('app.merchant', {
		url: '/merchant',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/merchant/html/merchant.html',
				controller: 'merchantCtrl'
			}
		}
	})

	.state('app.merchant.index', {
		url: '/index',
		views: {
			'merchantContent': {
				templateUrl: 'templates/merchant/html/index.html',
				controller: 'merchantIndexCtrl'
			}
		}
	})

	.state('app.merchant.detail', {
		url: '/detail',
		params: {
			merchant: null
		},
		views: {
			'merchantContent': {
				templateUrl: 'templates/merchant/html/detail.html',
				controller: 'merchantDetailCtrl'
			}
		}
	})

	.state('app.merchant.create', {
		url: '/create',
		views: {
			'merchantContent': {
				templateUrl: 'templates/merchant/html/create.html',
				controller: 'merchantCreateCtrl'
			}
		}
	})

	.state('app.merchant.edit', {
		url: '/edit',
		params: {
			merchant: null
		},
		views: {
			'merchantContent': {
				templateUrl: 'templates/merchant/html/edit.html',
				controller: 'merchantEditCtrl'
			}
		}
	})

	.state('app.umroh', {
		url: '/umroh',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/umroh/html/umroh.html',
				controller: 'umrohCtrl'
			}
		}
	})

	.state('app.umroh.index', {
		url: '/index',
		views: {
			'umrohContent': {
				templateUrl: 'templates/umroh/html/index.html',
				controller: 'umrohIndexCtrl'
			}
		}
	})

	.state('app.umroh.detail', {
		url: '/detail',
		params: {
			umroh: null
		},
		views: {
			'umrohContent': {
				templateUrl: 'templates/umroh/html/detail.html',
				controller: 'umrohDetailCtrl'
			}
		}
	})

	.state('app.umroh.create', {
		url: '/create',
		views: {
			'umrohContent': {
				templateUrl: 'templates/umroh/html/create.html',
				controller: 'umrohCreateCtrl'
			}
		}
	})

	.state('app.umroh.edit', {
		url: '/edit',
		params: {
			umroh: null
		},
		views: {
			'umrohContent': {
				templateUrl: 'templates/umroh/html/edit.html',
				controller: 'umrohEditCtrl'
			}
		}
	})

	.state('app.property', {
		url: '/property',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/property/html/property.html',
				controller: 'propertyCtrl'
			}
		}
	})

	.state('app.property.index', {
		url: '/index',
		views: {
			'propertyContent': {
				templateUrl: 'templates/property/html/index.html',
				controller: 'propertyIndexCtrl'
			}
		}
	})

	.state('app.property.detail', {
		url: '/detail',
		params: {
			property: null
		},
		views: {
			'propertyContent': {
				templateUrl: 'templates/property/html/detail.html',
				controller: 'propertyDetailCtrl'
			}
		}
	})

	.state('app.property.create', {
		url: '/create',
		views: {
			'propertyContent': {
				templateUrl: 'templates/property/html/create.html',
				controller: 'propertyCreateCtrl'
			}
		}
	})

	.state('app.property.edit', {
		url: '/edit',
		params: {
			property: null
		},
		views: {
			'propertyContent': {
				templateUrl: 'templates/property/html/edit.html',
				controller: 'propertyEditCtrl'
			}
		}
	})

	.state('app.calculator', {
		url: '/calculator',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/calculator/html/calculator.html',
				controller: 'calculatorCtrl'
			}
		}
	})

	.state('app.calculator.index', {
		url: '/index',
		views: {
			'calculatorContent': {
				templateUrl: 'templates/calculator/html/index.html',
				controller: 'calculatorIndexCtrl'
			}
		}
	})

	.state('app.calculator.create', {
		url: '/create',
		views: {
			'calculatorContent': {
				templateUrl: 'templates/calculator/html/create.html',
				controller: 'calculatorCreateCtrl'
			}
		}
	})

	.state('app.calculator.edit', {
		url: '/edit',
		params: {
			calculator: null
		},
		views: {
			'calculatorContent': {
				templateUrl: 'templates/calculator/html/edit.html',
				controller: 'calculatorEditCtrl'
			}
		}
	})

	.state('app.filing', {
		url: '/filing',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/filing/html/filing.html'
			}
		}
	})

	.state('app.filing.finance', {
		url: '/finance',
		abstract: true,
		views: {
			'filingContent': {
				templateUrl: 'templates/filing/html/finance.html'
			}
		}
	})

	.state('app.filing.finance.index', {
		url: '/index',
		views: {
			'financeContent': {
				templateUrl: 'templates/filing/html/finance-index.html',
				controller: 'filingFinanceIndexCtrl'
			}
		}
	})

	.state('app.filing.finance.detail', {
		url: '/detail',
		params: {
			financeFiling: null
		},
		views: {
			'financeContent': {
				templateUrl: 'templates/filing/html/finance-detail.html',
				controller: 'filingFinanceDetailCtrl'
			}
		}
	})

	.state('app.filing.umroh', {
		url: '/umroh',
		abstract: true,
		views: {
			'filingContent': {
				templateUrl: 'templates/filing/html/umroh.html'
			}
		}
	})

	.state('app.filing.umroh.index', {
		url: '/index',
		views: {
			'umrohContent': {
				templateUrl: 'templates/filing/html/umroh-index.html',
				controller: 'filingUmrohIndexCtrl'
			}
		}
	})

	.state('app.filing.umroh.detail', {
		url: '/detail',
		params: {
			umrohFiling: null
		},
		views: {
			'umrohContent': {
				templateUrl: 'templates/filing/html/umroh-detail.html',
				controller: 'filingUmrohDetailCtrl'
			}
		}
	})

	.state('app.filing.property', {
		url: '/property',
		abstract: true,
		views: {
			'filingContent': {
				templateUrl: 'templates/filing/html/property.html'
			}
		}
	})

	.state('app.filing.property.index', {
		url: '/index',
		views: {
			'propertyContent': {
				templateUrl: 'templates/filing/html/property-index.html',
				controller: 'filingPropertyIndexCtrl'
			}
		}
	})

	.state('app.filing.property.detail', {
		url: '/detail',
		params: {
			propertyFiling: null
		},
		views: {
			'propertyContent': {
				templateUrl: 'templates/filing/html/property-detail.html',
				controller: 'filingPropertyDetailCtrl'
			}
		}
	})

	// .state('app.financeFiling.create', {
	// 	url: '/create',
	// 	views: {
	// 		'financeFilingContent': {
	// 			templateUrl: 'templates/financeFiling/html/create.html',
	// 			controller: 'financeFilingCreateCtrl'
	// 		}
	// 	}
	// })

	// .state('app.financeFiling.edit', {
	// 	url: '/edit',
	// 	params: {
	// 		financeFiling: null
	// 	},
	// 	views: {
	// 		'financeFilingContent': {
	// 			templateUrl: 'templates/financeFiling/html/edit.html',
	// 			controller: 'financeFilingEditCtrl'
	// 		}
	// 	}
	// })

	.state('app.article', {
		url: '/article',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/article/html/article.html'
			}
		}
	})

	.state('app.article.category', {
		url: '/category',
		abstract: true,
		views: {
			'articleContent': {
				templateUrl: 'templates/article/html/category.html'
			}
		}
	})

	.state('app.article.category.index', {
		url: '/index',
		views: {
			'categoryContent': {
				templateUrl: 'templates/article/html/category-index.html',
				controller: 'articleCategoryIndexCtrl'
			}
		}
	})

	.state('app.article.category.create', {
		url: '/create',
		views: {
			'categoryContent': {
				templateUrl: 'templates/article/html/category-create.html',
				controller: 'articleCategoryCreateCtrl'
			}
		}
	})

	.state('app.article.category.edit', {
		url: '/edit',
		params: {
			category: null
		},
		views: {
			'categoryContent': {
				templateUrl: 'templates/article/html/category-edit.html',
				controller: 'articleCategoryEditCtrl'
			}
		}
	})

	.state('app.article.subCategory', {
		url: '/subCategory',
		abstract: true,
		views: {
			'articleContent': {
				templateUrl: 'templates/article/html/subCategory.html'
			}
		}
	})

	.state('app.article.subCategory.index', {
		url: '/index',
		views: {
			'subCategoryContent': {
				templateUrl: 'templates/article/html/subCategory-index.html',
				controller: 'articleSubCategoryIndexCtrl'
			}
		}
	})

	.state('app.article.subCategory.create', {
		url: '/create',
		views: {
			'subCategoryContent': {
				templateUrl: 'templates/article/html/subCategory-create.html',
				controller: 'articleSubCategoryCreateCtrl'
			}
		}
	})

	.state('app.article.subCategory.edit', {
		url: '/edit',
		params: {
			subCategory: null
		},
		views: {
			'subCategoryContent': {
				templateUrl: 'templates/article/html/subCategory-edit.html',
				controller: 'articleSubCategoryEditCtrl'
			}
		}
	})

	.state('app.article.list', {
		url: '/list',
		abstract: true,
		views: {
			'articleContent': {
				templateUrl: 'templates/article/html/list.html'
			}
		}
	})

	.state('app.article.list.index', {
		url: '/index',
		views: {
			'listContent': {
				templateUrl: 'templates/article/html/list-index.html',
				controller: 'articleListIndexCtrl'
			}
		}
	})

	.state('app.article.list.detail', {
		url: '/detail',
		params: {
			article: null
		},
		views: {
			'listContent': {
				templateUrl: 'templates/article/html/list-detail.html',
				controller: 'articleListDetailCtrl'
			}
		}
	})

	.state('app.article.list.create', {
		url: '/create',
		views: {
			'listContent': {
				templateUrl: 'templates/article/html/list-create.html',
				controller: 'articleListCreateCtrl'
			}
		}
	})

	.state('app.article.list.edit', {
		url: '/edit',
		params: {
			article: null
		},
		views: {
			'listContent': {
				templateUrl: 'templates/article/html/list-edit.html',
				controller: 'articleListEditCtrl'
			}
		}
	})

	.state('app.term', {
		url: '/term',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/term/html/term.html',
				controller: 'termCtrl'
			}
		}
	})

	.state('app.term.index', {
		url: '/index',
		views: {
			'termContent': {
				templateUrl: 'templates/term/html/index.html',
				controller: 'termIndexCtrl'
			}
		}
	})

	.state('app.term.create', {
		url: '/create',
		views: {
			'termContent': {
				templateUrl: 'templates/term/html/create.html',
				controller: 'termCreateCtrl'
			}
		}
	})

	.state('app.term.edit', {
		url: '/edit',
		params: {
			term: null
		},
		views: {
			'termContent': {
				templateUrl: 'templates/term/html/edit.html',
				controller: 'termEditCtrl'
			}
		}
	})

	.state('app.promo', {
		url: '/promo',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/promo/html/promo.html',
				controller: 'promoCtrl'
			}
		}
	})

	.state('app.promo.index', {
		url: '/index',
		views: {
			'promoContent': {
				templateUrl: 'templates/promo/html/index.html',
				controller: 'promoIndexCtrl'
			}
		}
	})

	.state('app.promo.detail', {
		url: '/detail',
		params: {
			promo: null
		},
		views: {
			'promoContent': {
				templateUrl: 'templates/promo/html/detail.html',
				controller: 'promoDetailCtrl'
			}
		}
	})

	.state('app.promo.create', {
		url: '/create',
		views: {
			'promoContent': {
				templateUrl: 'templates/promo/html/create.html',
				controller: 'promoCreateCtrl'
			}
		}
	})

	.state('app.promo.edit', {
		url: '/edit',
		params: {
			promo: null
		},
		views: {
			'promoContent': {
				templateUrl: 'templates/promo/html/edit.html',
				controller: 'promoEditCtrl'
			}
		}
	})

	.state('app.survey', {
		url: '/survey',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/survey/html/survey.html'
			}
		}
	})

	.state('app.survey.question', {
		url: '/question',
		abstract: true,
		views: {
			'surveyContent': {
				templateUrl: 'templates/survey/html/question.html'
			}
		}
	})

	.state('app.survey.question.index', {
		url: '/index',
		views: {
			'questionContent': {
				templateUrl: 'templates/survey/html/question-index.html',
				controller: 'surveyQuestionIndexCtrl',
			}
		}
	})

	.state('app.survey.question.create', {
		url: '/create',
		views: {
			'questionContent': {
				templateUrl: 'templates/survey/html/question-create.html',
				controller: 'surveyQuestionCreateCtrl'
			}
		}
	})

	.state('app.survey.question.edit', {
		url: '/edit',
		params: {
			survey: null
		},
		views: {
			'questionContent': {
				templateUrl: 'templates/survey/html/question-edit.html',
				controller: 'surveyQuestionEditCtrl'
			}
		}
	})

	.state('app.survey.answer', {
		url: '/answer',
		abstract: true,
		views: {
			'surveyContent': {
				templateUrl: 'templates/survey/html/answer.html'
			}
		}
	})

	.state('app.survey.answer.index', {
		url: '/index',
		views: {
			'answerContent': {
				templateUrl: 'templates/survey/html/answer-index.html',
				controller: 'surveyAnswerIndexCtrl',
			}
		}
	})

	.state('app.suggestion', {
		url: '/suggestion',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/suggestion/html/suggestion.html',
				controller: 'suggestionCtrl'
			}
		}
	})

	.state('app.suggestion.index', {
		url: '/index',
		views: {
			'suggestionContent': {
				templateUrl: 'templates/suggestion/html/index.html',
				controller: 'suggestionIndexCtrl'
			}
		}
	})

	.state('app.user', {
		url: '/user',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/user/html/user.html',
				controller: 'userCtrl'
			}
		}
	})

	.state('app.user.index', {
		url: '/index',
		views: {
			'userContent': {
				templateUrl: 'templates/user/html/index.html',
				controller: 'userIndexCtrl'
			}
		}
	})

	.state('app.message', {
		url: '/message',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/message/html/index.html',
				controller: 'MessageCtrl'
			}
		}
	})

	.state('app.message.index', {
		url: '/index',
		views: {
			'messageContent': {
				templateUrl: 'templates/message/html/message.html',
				controller: 'MessageIndexCtrl'
			}
		}
	})

	.state('app.settingUser', {
		url: '/settingUser',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/settingUser/html/settingUser.html',
				controller: 'settingUserCtrl'
			}
		}
	})

	.state('app.settingUser.index', {
		url: '/index',
		views: {
			'settingUserContent': {
				templateUrl: 'templates/settingUser/html/index.html',
				controller: 'settingUserIndexCtrl'
			}
		}
	})

	.state('app.settingUser.create', {
		url: '/create',
		views: {
			'settingUserContent': {
				templateUrl: 'templates/settingUser/html/create.html',
				controller: 'settingUserCreateCtrl'
			}
		}
	})

	.state('app.settingUser.edit', {
		url: '/edit',
		params: {
			user: null
		},
		views: {
			'settingUserContent': {
				templateUrl: 'templates/settingUser/html/edit.html',
				controller: 'settingUserEditCtrl'
			}
		}
	})

	.state('app.settingCity', {
		url: '/settingCity',
		abstract: true,
		views: {
			'appContent': {
				templateUrl: 'templates/settingCity/html/settingCity.html',
				controller: 'settingCityCtrl'
			}
		}
	})

	.state('app.settingCity.index', {
		url: '/index',
		views: {
			'settingCityContent': {
				templateUrl: 'templates/settingCity/html/index.html',
				controller: 'settingCityIndexCtrl'
			}
		}
	})

	.state('app.settingCity.create', {
		url: '/create',
		views: {
			'settingCityContent': {
				templateUrl: 'templates/settingCity/html/create.html',
				controller: 'settingCityCreateCtrl'
			}
		}
	})

	.state('app.settingCity.edit', {
		url: '/edit',
		params: {
			city: null
		},
		views: {
			'settingCityContent': {
				templateUrl: 'templates/settingCity/html/edit.html',
				controller: 'settingCityEditCtrl'
			}
		}
	})

	.state('app.settingEmail', {
		url: '/settingEmail',
		views: {
			'appContent': {
				templateUrl: 'templates/settingEmail/html/settingEmail.html',
				controller: 'settingEmailCtrl'
			}
		}
	})

	.state('app.role', {
		url: '/roles',
		views: {
			'appContent': {
				templateUrl: 'templates/role/html/role.html',
				controller: 'RolesCtrl'
			}
		}
	})

	.state('app.umrohRegister', {
		url: '/registerUmroh',
		views: {
			'appContent': {
				templateUrl: 'templates/umrohRegister/html/umrohRegister.html',
				controller: 'umrohRegisterCtrl'
			}
		}
	})

	.state('app.productList', {
		url: '/productList',
		views: {
			'appContent': {
				templateUrl: 'templates/umrohRegister/html/umrohProduct-List.html',
				controller: 'umrohProductListCtrl'
			}
		}
	})

	.state('app.propertyRegister', {
		url: '/propertyRegister',
		views: {
			'appContent': {
				templateUrl: 'templates/propertyRegister/html/propertyRegister.html',
				controller: 'propertyRegisterCtrl'
			}
		}
	})

	.state('app.travelAgency', {
		url: '/travelAgency',
		views: {
			'appContent': {
				templateUrl: 'templates/travelAgency/html/agencyList.html',
				controller: 'agencyListCtrl'
			}
		}
	})

	.state('app.manageAgency', {
		url: '/manageAgency/:name',
		views: {
			'appContent': {
				templateUrl: 'templates/travelAgency/html/manageAgency.html',
				controller: 'manageAgencyCtrl'
			}
		}
	})

	// .state('app.profile', {
	// 	url: '/profile',
	// 	abstract: true,
	// 	views: {
	// 		'appContent': {
	// 			templateUrl: 'templates/profile/html/profile.html'
	// 		}
	// 	}
	// })

	// .state('app.profile.slide', {
	// 	url: '/slide',
	// 	abstract: true,
	// 	views: {
	// 		'profileContent': {
	// 			templateUrl: 'templates/profile/html/slide.html'
	// 		}
	// 	}
	// })

	// .state('app.profile.slide.index', {
	// 	url: '/index',
	// 	views: {
	// 		'slideContent': {
	// 			templateUrl: 'templates/profile/html/slide-index.html',
	// 			controller: 'profileSlideIndexCtrl'
	// 		}
	// 	}
	// })

	// .state('app.profile.slide.create', {
	// 	url: '/create',
	// 	views: {
	// 		'slideContent': {
	// 			templateUrl: 'templates/profile/html/slide-create.html',
	// 			controller: 'profileSlideCreateCtrl'
	// 		}
	// 	}
	// })

	// .state('app.profile.slide.edit', {
	// 	url: '/edit',
	// 	params: {
	// 		slide: null
	// 	},
	// 	views: {
	// 		'slideContent': {
	// 			templateUrl: 'templates/profile/html/slide-edit.html',
	// 			controller: 'profileSlideEditCtrl'
	// 		}
	// 	}
	// })

	// .state('app.profile.slide.setting', {
	// 	url: '/setting',
	// 	views: {
	// 		'slideContent': {
	// 			templateUrl: 'templates/profile/html/setting.html',
	// 			controller: 'profileSlideSettingCtrl'
	// 		}
	// 	}
	// })

	// .state('app.profile.history', {
	// 	url: '/history',
	// 	abstract: true,
	// 	views: {
	// 		'profileContent': {
	// 			templateUrl: 'templates/profile/html/history.html'
	// 		}
	// 	}
	// })

	// .state('app.profile.history.index', {
	// 	url: '/index',
	// 	views: {
	// 		'historyContent' : {
	// 			templateUrl: 'templates/profile/html/history-index.html',
	// 			controller: 'profileHistoryIndexCtrl'
	// 		}
	// 	}
	// })

	// .state('app.profile.history.create', {
	// 	url: '/create',
	// 	views: {
	// 		'historyContent' : {
	// 			templateUrl: 'templates/profile/html/history-create.html',
	// 			controller: 'profileHistoryCreateCtrl'
	// 		}
	// 	}
	// })

	// .state('app.profile.history.edit', {
	// 	url: '/edit',
	// 	params: {
	// 		history: null
	// 	},
	// 	views: {
	// 		'historyContent': {
	// 			templateUrl: 'templates/profile/html/history-edit.html',
	// 			controller: 'profileHistoryEditCtrl'
	// 		}
	// 	}
	// })

	// .state('app.profile.about', {
	// 	url: '/about',
	// 	abstract: true,
	// 	views: {
	// 		'profileContent': {
	// 			templateUrl: 'templates/profile/html/about.html'
	// 		}
	// 	}
	// })

	// .state('app.profile.about.index', {
	// 	url: '/index',
	// 	views: {
	// 		'aboutContent': {
	// 			templateUrl: 'templates/profile/html/about-index.html',
	// 			controller: 'profileAboutIndexCtrl'
	// 		}
	// 	}
	// })

	// .state('app.profile.about.create', {
	// 	url: '/create',
	// 	views: {
	// 		'aboutContent': {
	// 			templateUrl: 'templates/profile/html/about-create.html',
	// 			controller: 'profileAboutCreateCtrl'
	// 		}
	// 	}
	// })

	// .state('app.profile.about.edit', {
	// 	url: '/edit',
	// 	params: {
	// 		about: null
	// 	},
	// 	views: {
	// 		'aboutContent': {
	// 			templateUrl: 'templates/profile/html/about-edit.html',
	// 			controller: 'profileAboutEditCtrl'
	// 		}
	// 	}
	// })

	// .state('app.profile.team', {
	// 	url: '/team',
	// 	abstract: true,
	// 	views: {
	// 		'profileContent': {
	// 			templateUrl: 'templates/profile/html/team.html'
	// 		}
	// 	}
	// })

	// .state('app.profile.team.index', {
	// 	url: '/index',
	// 	views: {
	// 		'teamContent': {
	// 			templateUrl: 'templates/profile/html/team-index.html',
	// 			controller: 'profileTeamIndexCtrl'
	// 		}
	// 	}
	// })

	// .state('app.profile.team.create', {
	// 	url: '/create',
	// 	views: {
	// 		'teamContent': {
	// 			templateUrl: 'templates/profile/html/team-create.html',
	// 			controller: 'profileTeamCreateCtrl'
	// 		}
	// 	}
	// })

	// .state('app.profile.team.edit', {
	// 	url: '/edit',
	// 	params: {
	// 		team: null
	// 	},
	// 	views: {
	// 		'teamContent': {
	// 			templateUrl: 'templates/profile/html/team-edit.html',
	// 			controller: 'profileTeamEditCtrl'
	// 		}
	// 	}
	// })

	// .state('app.calendar', {
	// 	url: '/calendar',
	// 	abstract: true,
	// 	views: {
	// 		'appContent': {
	// 			templateUrl: 'templates/calendar/html/calendar.html',
	// 			controller: 'calendarCtrl'
	// 		}
	// 	}
	// })

	// .state('app.calendar.index', {
	// 	url: '/index',
	// 	views: {
	// 		'calendarContent': {
	// 			templateUrl: 'templates/calendar/html/index.html',
	// 			controller: 'calendarIndexCtrl'
	// 		}
	// 	}
	// })

	// .state('app.calendar.create', {
	// 	url: '/create',
	// 	views: {
	// 		'calendarContent': {
	// 			templateUrl: 'templates/calendar/html/create.html',
	// 			controller: 'calendarCreateCtrl'
	// 		}
	// 	}
	// })

	// .state('app.calendar.edit', {
	// 	url: '/edit',
	// 	params: {
	// 		calendar: null
	// 	},
	// 	views: {
	// 		'calendarContent': {
	// 			templateUrl: 'templates/calendar/html/edit.html',
	// 			controller: 'calendarEditCtrl'
	// 		}
	// 	}
	// })

	// .state('app.schedule', {
	// 	url: '/schedule',
	// 	abstract: true,
	// 	views: {
	// 		'appContent': {
	// 			templateUrl: 'templates/schedule/html/schedule.html',
	// 			controller: 'scheduleCtrl'
	// 		}
	// 	}
	// })

	// .state('app.schedule.index', {
	// 	url: '/index',
	// 	views: {
	// 		'scheduleContent': {
	// 			templateUrl: 'templates/schedule/html/index.html',
	// 			controller: 'scheduleIndexCtrl'
	// 		}
	// 	}
	// })

	// .state('app.schedule.create', {
	// 	url: '/create',
	// 	views: {
	// 		'scheduleContent': {
	// 			templateUrl: 'templates/schedule/html/create.html',
	// 			controller: 'scheduleCreateCtrl'
	// 		}
	// 	}
	// })

	// .state('app.schedule.edit', {
	// 	url: '/edit',
	// 	params: {
	// 		schedule: null
	// 	},
	// 	views: {
	// 		'scheduleContent': {
	// 			templateUrl: 'templates/schedule/html/edit.html',
	// 			controller: 'scheduleEditCtrl'
	// 		}
	// 	}
	// })

	// .state('app.stream', {
	// 	url: '/stream',
	// 	abstract: true,
	// 	views: {
	// 		'appContent': {
	// 			templateUrl: 'templates/stream/html/stream.html',
	// 			controller: 'streamCtrl'
	// 		}
	// 	}
	// })

	// .state('app.stream.index', {
	// 	url: '/index',
	// 	views: {
	// 		'streamContent': {
	// 			templateUrl: 'templates/stream/html/index.html',
	// 			controller: 'streamIndexCtrl'
	// 		}
	// 	}
	// })

	// .state('app.stream.create', {
	// 	url: '/create',
	// 	views: {
	// 		'streamContent': {
	// 			templateUrl: 'templates/stream/html/create.html',
	// 			controller: 'streamCreateCtrl'
	// 		}
	// 	}
	// })

	// .state('app.stream.edit', {
	// 	url: '/edit',
	// 	params: {
	// 		stream: null
	// 	},
	// 	views: {
	// 		'streamContent': {
	// 			templateUrl: 'templates/stream/html/edit.html',
	// 			controller: 'streamEditCtrl'
	// 		}
	// 	}
	// })

	// .state('app.gallery', {
	// 	url: '/gallery',
	// 	abstract: true,
	// 	views: {
	// 		'appContent': {
	// 			templateUrl: 'templates/gallery/html/gallery.html'
	// 		}
	// 	}
	// })

	// .state('app.gallery.album', {
	// 	url: '/album',
	// 	abstract: true,
	// 	views: {
	// 		'galleryContent': {
	// 			templateUrl: 'templates/gallery/html/album.html'
	// 		}
	// 	}
	// })

	// .state('app.gallery.album.index', {
	// 	url: '/index',
	// 	views: {
	// 		'albumContent': {
	// 			templateUrl: 'templates/gallery/html/album-index.html',
	// 			controller: 'galleryAlbumIndexCtrl'
	// 		}
	// 	}
	// })

	// .state('app.gallery.album.create', {
	// 	url: '/create',
	// 	views: {
	// 		'albumContent': {
	// 			templateUrl: 'templates/gallery/html/album-create.html',
	// 			controller: 'galleryAlbumCreateCtrl'
	// 		}
	// 	}
	// })

	// .state('app.gallery.album.edit', {
	// 	url: '/edit',
	// 	params: {
	// 		album: null
	// 	},
	// 	views: {
	// 		'albumContent': {
	// 			templateUrl: 'templates/gallery/html/album-edit.html',
	// 			controller: 'galleryAlbumEditCtrl'
	// 		}
	// 	}
	// })

	// .state('app.gallery.image', {
	// 	url: '/image',
	// 	abstract: true,
	// 	views: {
	// 		'galleryContent': {
	// 			templateUrl: 'templates/gallery/html/image.html'
	// 		}
	// 	}
	// })

	// .state('app.gallery.image.index', {
	// 	url: '/index',
	// 	views: {
	// 		'imageContent': {
	// 			templateUrl: 'templates/gallery/html/image-index.html',
	// 			controller: 'galleryImageIndexCtrl'
	// 		}
	// 	}
	// })

	// .state('app.gallery.image.create', {
	// 	url: '/create',
	// 	views: {
	// 		'imageContent': {
	// 			templateUrl: 'templates/gallery/html/image-create.html',
	// 			controller: 'galleryImageCreateCtrl'
	// 		}
	// 	}
	// })

	// .state('app.gallery.image.edit', {
	// 	url: '/edit',
	// 	params: {
	// 		image: null
	// 	},
	// 	views: {
	// 		'imageContent': {
	// 			templateUrl: 'templates/gallery/html/image-edit.html',
	// 			controller: 'galleryImageEditCtrl'
	// 		}
	// 	}
	// })

	// .state('app.donation', {
	// 	url: '/donation',
	// 	abstract: true,
	// 	views: {
	// 		'appContent': {
	// 			templateUrl: 'templates/donation/html/donation.html',
	// 			controller: 'donationCtrl'
	// 		}
	// 	}
	// })

	// .state('app.donation.index', {
	// 	url: '/index',
	// 	views: {
	// 		'donationContent': {
	// 			templateUrl: 'templates/donation/html/index.html',
	// 			controller: 'donationIndexCtrl'
	// 		}
	// 	}
	// })

	// .state('app.donation.create', {
	// 	url: '/create',
	// 	views: {
	// 		'donationContent': {
	// 			templateUrl: 'templates/donation/html/create.html',
	// 			controller: 'donationCreateCtrl'
	// 		}
	// 	}
	// })

	// .state('app.donation.edit', {
	// 	url: '/edit',
	// 	params: {
	// 		donation: null
	// 	},
	// 	views: {
	// 		'donationContent': {
	// 			templateUrl: 'templates/donation/html/edit.html',
	// 			controller: 'donationEditCtrl'
	// 		}
	// 	}
	// })

	// .state('app.report', {
	// 	url: '/report',
	// 	abstract: true,
	// 	views: {
	// 		'appContent': {
	// 			templateUrl: 'templates/report/html/report.html',
	// 			controller: 'reportCtrl'
	// 		}
	// 	}
	// })

	// .state('app.report.index', {
	// 	url: '/index',
	// 	views: {
	// 		'reportContent': {
	// 			templateUrl: 'templates/report/html/index.html',
	// 			controller: 'reportIndexCtrl'
	// 		}
	// 	}
	// })

	// .state('app.filemanager', {
	// 	url: '/filemanager',
	// 	views: {
	// 		'appContent': {
	// 			templateUrl: 'templates/filemanager/html/filemanager.html'
	// 		}
	// 	}
	// })

	$urlRouterProvider.otherwise('login');
})