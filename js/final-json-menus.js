allMenus={
  "type": "menu",
  "menus": [
    {
      "id": 1,
      "state": "app.home.slide.index",
      "name": "Home",
      "icon": "fa fa-home",
      "url": "/app/home"
    },
    {
      "id": 2,
      "state": "app.product.index",
      "name": "Produk",
      "icon": "fa fa-star",
      "url": "/app/product"
    },
    {
      "id": 3,
      "state": "app.branch.index",
      "name": "Cabang",
      "icon": "fa fa-sitemap",
      "url": "/app/branch"
    },
    {
      "id": 4,
      "state": "app.merchant.index",
      "name": "Merchants",
      "icon": "fa fa-shopping-basket",
      "url": "/app/merchant"
    },
    {
      "id": 5,
      "state": "app.umroh.index",
      "name": "Umroh",
      "icon": "fa fa-suitcase",
      "url": "/app/umroh"
    },
    {
      "id": 6,
      "state": "app.property.index",
      "name": "Properti",
      "icon": "fa fa-building",
      "url": "/app/property"
    },
    {
      "id": 7,
      "state": "app.calculator.index",
      "name": "Kalkulator Pembiayaan",
      "icon": "fa fa-calculator",
      "url": "/app/calculator"
    },
    {
      "id": 8,
      "state": "app.filing.finance.index",
      "name": "Pengajuan Pembiayaan",
      "icon": "fa fa-money",
      "url": "/app/filing"
    },
    {
      "id": 9,
      "state": "app.article.category.index",
      "name": "Artikel",
      "icon": "fa fa-file-text",
      "url": "/app/article"
    },
    {
      "id": 10,
      "state": "app.term.index",
      "name": "Istilah",
      "icon": "fa fa-list",
      "url": "/app/term"
    },
    {
      "id": 11,
      "state": "app.promo.index",
      "name": "Promo",
      "icon": "fa fa-bullhorn",
      "url": "/app/promo"
    },
    {
      "id": 12,
      "state": "app.survey.question.index",
      "name": "Survey",
      "icon": "fa fa-check-square",
      "url": "/app/survey"
    },
    {
      "id": 13,
      "state": "app.suggestion.index",
      "name": "Saran",
      "icon": "fa fa-envelope",
      "url": "/app/suggestion"
    },
    {
      "id": 14,
      "state": "app.user.index",
      "name": "User",
      "icon": "fa fa-users",
      "url": "/app/user"
    },
    {
      "id": 15,
      "state": "app.umrohRegister",
      "name": "Pendaftaran Umroh",
      "icon": "fa fa-cube",
      "url": "/app/registerUmroh"
    },
    {
      "id": 16,
      "state": "app.propertyRegister",
      "name": "Pendaftaran Properti",
      "icon": "fa fa-building",
      "url": "/app/propertyRegister"
    },
    {
      "id": 17,
      "state": "app.travelAgency",
      "name": "Travel Agency",
      "icon": "fa fa-car",
      "url": "/app/travelAgency"
    },
    {
      "id": 18,
      "state": "app.message.index",
      "name": "Message",
      "icon": "fa fa-inbox",
      "url": "/app/message"
    },
    {
      
      "icon": "fa fa-gear",
      "id": 19,
      "name": "Setting User",
      "state": "app.settingUser.index",
      "url": "/app/settingUser"
    },
    {
      
      "icon": "fa fa-gear",
      "id": 20,
      "name": "Setting Kota",
      "state": "app.settingCity.index",
      "url": "/app/settingCity"
    },
    {
     
      "icon": "fa fa-gear",
      "id": 21,
      "name": "Setting Email",
      "state": "app.settingEmail",
      "url": "/app/settingEmail"
    },
    {
      
      "icon": "fa fa-gear",
      "id": 22,
      "name": "Setting Role",
      "state": "app.role",
      "url": "/app/role"
    }
  ]
}

AdminROle={
  "menus": [
    {
      "checked": true,
      "icon": "fa fa-home",
      "id": 1,
      "name": "Home",
      "state": "app.home.slide.index",
      "url": "/app/home"
    },
    {
      "checked": true,
      "icon": "fa fa-star",
      "id": 2,
      "name": "Produk",
      "state": "app.product.index",
      "url": "/app/product"
    },
    {
      "checked": true,
      "icon": "fa fa-sitemap",
      "id": 3,
      "name": "Cabang",
      "state": "app.branch.index",
      "url": "/app/branch"
    },
    {
      "checked": true,
      "icon": "fa fa-shopping-basket",
      "id": 4,
      "name": "Merchants",
      "state": "app.merchant.index",
      "url": "/app/merchant"
    },
    {
      "checked": true,
      "icon": "fa fa-suitcase",
      "id": 5,
      "name": "Umroh",
      "state": "app.umroh.index",
      "url": "/app/umroh"
    },
    {
      "checked": true,
      "icon": "fa fa-building",
      "id": 6,
      "name": "Properti",
      "state": "app.property.index",
      "url": "/app/property"
    },
    {
      "checked": true,
      "icon": "fa fa-calculator",
      "id": 7,
      "name": "Kalkulator Pembiayaan",
      "state": "app.calculator.index",
      "url": "/app/calculator"
    },
    {
      "checked": true,
      "icon": "fa fa-money",
      "id": 8,
      "name": "Pengajuan Pembiayaan",
      "state": "app.filing.finance.index",
      "url": "/app/filing"
    },
    {
      "checked": true,
      "icon": "fa fa-file-text",
      "id": 9,
      "name": "Artikel",
      "state": "app.article.category.index",
      "url": "/app/article"
    },
    {
      "checked": true,
      "icon": "fa fa-list",
      "id": 10,
      "name": "Istilah",
      "state": "app.term.index",
      "url": "/app/term"
    },
    {
      "checked": true,
      "icon": "fa fa-bullhorn",
      "id": 11,
      "name": "Promo",
      "state": "app.promo.index",
      "url": "/app/promo"
    },
    {
      "checked": true,
      "icon": "fa fa-check-square",
      "id": 12,
      "name": "Survey",
      "state": "app.survey.question.index",
      "url": "/app/survey"
    },
    {
      "checked": true,
      "icon": "fa fa-envelope",
      "id": 13,
      "name": "Saran",
      "state": "app.suggestion.index",
      "url": "/app/suggestion"
    },
    {
      "checked": true,
      "icon": "fa fa-users",
      "id": 14,
      "name": "User",
      "state": "app.user.index",
      "url": "/app/user"
    },
    {
      "checked": true,
      "icon": "fa fa-cube",
      "id": 15,
      "name": "Pendaftaran Umroh",
      "state": "app.umrohRegister",
      "url": "/app/registerUmroh"
    },
    {
      "checked": true,
      "icon": "fa fa-building",
      "id": 16,
      "name": "Pendaftaran Properti",
      "state": "app.propertyRegister",
      "url": "/app/propertyRegister"
    },
    {
      "checked": true,
      "icon": "fa fa-car",
      "id": 17,
      "name": "Travel Agency",
      "state": "app.travelAgency",
      "url": "/app/travelAgency"
    },
    {
      "checked": true,
      "icon": "fa fa-inbox",
      "id": 18,
      "name": "Message",
      "state": "app.message.index",
      "url": "/app/message"
    },
    {
      "checked": true,
      "icon": "fa fa-gear",
      "id": 19,
      "name": "Setting User",
      "state": "app.settingUser.index",
      "url": "/app/settingUser"
    },
    {
      "checked": true,
      "icon": "fa fa-gear",
      "id": 20,
      "name": "Setting Kota",
      "state": "app.settingCity.index",
      "url": "/app/settingCity"
    },
    {
      "checked": true,
      "icon": "fa fa-gear",
      "id": 21,
      "name": "Setting Email",
      "state": "app.settingEmail",
      "url": "/app/settingEmail"
    },
    {
      "checked": true,
      "icon": "fa fa-gear",
      "id": 22,
      "name": "Setting Role",
      "state": "app.role",
      "url": "/app/role"
    }
  ],
  "name": "ADMIN",
  "type": "role"
}